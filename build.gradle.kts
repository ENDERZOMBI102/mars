@file:Suppress("HasPlatformType")
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

plugins {
	id( "com.github.johnrengelman.shadow" ) version "8.1.1"
	kotlin( "jvm" ) version "2.0.21"
	application
	java
}

val shade by configurations.creating

repositories {
	mavenLocal()
	mavenCentral()
	maven( url = "https://api.repsy.io/mvn/enderzombi102/mc" )
}

application {
	mainClass.set("mars.MarsLaunch")
//	applicationDefaultJvmArgs = listOf( "-Dlog4j2.configurationFile=${project.file( "log4j2.xml" ).absolutePath}" )
}

dependencies {
	implementation( libs.bundles.impl )
	runtimeOnly( libs.bundles.runtime )
	shade( libs.bundles.shade )
	implementation( shade( kotlin( "stdlib-jdk8" ) )!! )
}

java.toolchain.languageVersion.set( JavaLanguageVersion.of(17) )

tasks.named<Jar>("jar") {
	from( "LICENSE" ) {
		rename { "${it}_$archiveBaseName" }
	}
	tasks["startShadowScripts"].dependsOn( this )
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
	configurations = listOf( shade )
	archiveClassifier.set("")

	// expand and replace the ${} formatted strings inside `assets/data/info.json5`
	val date = LocalDateTime.now().format( DateTimeFormatter.ofPattern("dd-MM-yyyy'T'HH:mm:ss") )
	inputs.property( "version", archiveVersion )
	inputs.property( "build_date", date )

	filesMatching( "info.json5" ) {
		expand( "version" to archiveVersion, "build_date" to date )
	}

	tasks["distTar"].dependsOn( this )
	tasks["startScripts"].dependsOn( this )

	manifest.attributes(
		"Specification-Title"      to "MIPS Assembler and Runtime Simulator",
		"Specification-Vendor"     to "KenVollmar",
		"Specification-Version"    to "4.5.1",
		"Implementation-Title"     to "MARS",
		"Implementation-Version"   to archiveVersion,
		"Implementation-Vendor"    to "ENDERZOMBI102",
		"Implementation-Timestamp" to date,
	)
}

tasks.withType<JavaCompile> {
	sourceCompatibility = "17"
	options.encoding = "UTF-8"
	options.release.set( 17 )
}

tasks.withType<KotlinCompile> {
	compilerOptions.jvmTarget.set( JvmTarget.JVM_17 )
}


artifacts {
	archives( tasks["shadowJar"] )
}
