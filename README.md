MIPS Assembler and Runtime Simulator (MARS)
-
![Build Status](https://gitlab.com/ENDERZOMBI102/mars/badges/feat/small-qol-fixes/pipeline.svg)
![Java Version](https://img.shields.io/badge/Java-17-brightgreen)
![License](https://img.shields.io/badge/license-MIT-brightgreen)  
IDE, Assembler and Emulator for the MIPS assembly.

This version of MARS uses [MARS Theme Engine](https://github.com/aeris170/MARS-Theme-Engine) as base for theme support.

Most code has been rewritten and/or updated to Java 17

You can download the latest release [here](https://gitlab.com/ENDERZOMBI102/mars/-/releases/permalink/latest)

Features
-
NOTE: Not all listed features are completed
- [x] Default to dark theme
- [x] Default "Start execution from main" to true
- [ ] Improved logging
- [x] Store configs in a file instead of the Windows registry
- [x] Compile-on-execute
- [ ] Compile all open files by default
- [ ] Edit multiple files at once
- [ ] Translations
- [x] Remember the last directory used to open files
- [x] Relaunch with java 17 if it was launched with a wrong version 
- Improved IDE capabilities
    - [ ] Docstring viewer
    - [ ] Parameter checking

Compiling MARS from source
-
1. Clone this repo with `git clone https://gitlab.com/ENDERZOMBI102/mars.git` or click [here](https://gitlab.com/ENDERZOMBI102/mars/-/archive/main/mars-main.zip) for a zip
   - If using the zip, extract it somewhere
2. `cd` into the downloaded repo or open a terminal window inside it
3. Run `gradlew build` if on Windows or `./gradlew build` if on Linux/macOS
4. The MARS jar will be in `$REPO_PATH/build/libs`
