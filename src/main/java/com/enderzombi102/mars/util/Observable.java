package com.enderzombi102.mars.util;

import org.jetbrains.annotations.NotNull;

import java.util.*;

public abstract class Observable< T extends Observable<T>> {
	private final Set<Observer<T>> observers = Collections.synchronizedSet( new HashSet<>() );

	public void addObservable( @NotNull Observer<T> observer ) {
		this.observers.add( observer );
	}

	public void removeObserver( Observer<T> observer ) {
		this.observers.remove( observer );
	}

	public void notifyObservers( Object arg ) {
		for ( var observer : this.observers ) {
			observer.onObservableChange( this.getThis(), arg );
		}
	}

	public int getObserverCount() {
		return this.observers.size();
	}

	public void clear() {
		this.observers.clear();
	}

	@FunctionalInterface
	public interface Observer<T extends Observable<T>> {
		void onObservableChange( T object, Object arg );
	}

	@SuppressWarnings( "unchecked" )
	protected T getThis() {
		return (T) this;
	}
}
