package com.enderzombi102.mars.res;

import blue.endless.jankson.JsonObject;
import blue.endless.jankson.JsonPrimitive;
import blue.endless.jankson.api.SyntaxError;
import mars.Const;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;


public final class Localization {
	private static final Map<String, Map<String, String>> translations = new HashMap<>();
	private static final Logger LOGGER = LoggerFactory.getLogger( "MARS | I18n" );
	private static final Pattern KEY = Pattern.compile( "\\$\\{(.)+}" );
	private static final Map<String, String> CONSTANTS = Map.ofEntries(
		Map.entry( "const.og.authors", Const.OG_COPYRIGHT_HOLDERS ),
		Map.entry( "const.og.years", Const.OG_COPYRIGHT_YEARS ),
		Map.entry( "const.author", "ENDERZOMBI102" ),
		Map.entry( "const.fullname", "Mips Assembler and Runtime Simulator" ),
		Map.entry( "const.version", Const.VERSION ),
		Map.entry( "const.extensions", String.join( ", ", Const.FILE_EXTENSIONS ) )
	);
	private static @NotNull String currentLang = "en_us";
	private static @Nullable Map<String, String> current = null;

	private Localization() { }

	public static @NotNull String loc( @NotNull String key ) {
		if ( current == null )
			current = translations.computeIfAbsent( currentLang, Localization::loadTranslations );

		return KEY.matcher(
			current.getOrDefault(
				key,
				current.getOrDefault( "translation.missing", key )
			)
		).replaceAll( result -> loc( result.group().substring( 2, result.group().length() - 1 ) ) );
	}

	public static @NotNull String loc( @NotNull String key, Object... replacements ) {
		return loc( key ).formatted( replacements );
	}

	private static Map<String, String> loadTranslations( String code ) {
		LOGGER.info( "Loading localization for language '{}'...", code );
		try {
			// load new translations
			var locs = flattenEntries( "", Resources.loadJson( "/lang/" + code + ".json5" ) );

			// check if any constant was overwritten
			locs.keySet()
				.stream()
				.filter( CONSTANTS::containsKey )
				.forEach( key -> LOGGER.warn(  "Language `{}` tried to overwrite constant `{}`, it won't be overwitten!", code, key ) );

			locs.putAll( CONSTANTS );

			return locs;
		} catch ( SyntaxError | IOException e ) {
			throw new RuntimeException( e );
		}
	}

	private static @NotNull Map<String, String> flattenEntries( @NotNull String currentKey, JsonObject obj ) {
		var map = new HashMap<String, String>();

		for ( var entry : obj.entrySet() ) {
			if ( entry.getValue() instanceof JsonObject newObj )
				map.putAll( flattenEntries( ( currentKey.isEmpty() ? "" : currentKey + "." ) + entry.getKey(), newObj ) );
			else {
				var key = currentKey;
				if (! key.isEmpty() ) {
					if ( !entry.getKey().equals( "value" ) )
						key = key + "." + entry.getKey();
				} else
					key = entry.getKey();

				map.put( key, ( (JsonPrimitive) entry.getValue() ).asString() );
			}
		}

		return map;
	}

	public static void setLanguage( String language ) {
		current = translations.computeIfAbsent( currentLang = language, Localization::loadTranslations );
	}

	public static void main( String[] argv ) {
		setLanguage( "en_us" );
		System.out.println( loc( "mars.splash.row1" ) );
		System.out.println( loc( "mars.cli.copyright" ) );
	}
}
