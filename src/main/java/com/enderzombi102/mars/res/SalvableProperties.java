package com.enderzombi102.mars.res;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;


public class SalvableProperties extends Properties implements AutoCloseable {
	private static final Logger LOGGER = LoggerFactory.getLogger( SalvableProperties.class );
	private final Path backingFile;

	public SalvableProperties( String path ) {
		this( Path.of( path ) );
	}

	public SalvableProperties( Path backingFile ) {
		this.backingFile = backingFile;
		if ( Files.exists( backingFile ) ) {
			try ( var stream = Files.newInputStream( backingFile ) ) {
				this.load( stream );
			} catch ( IOException e ) {
				LOGGER.error( "Could not load backing file {}", backingFile, e );
			}
		}
	}

	public void save() throws IOException {
		try ( var stream = Files.newBufferedWriter( backingFile ) ) {
			this.store( stream, "" );
		}
	}

	@Override
	public void close() throws Exception {
		this.save();
	}

	public void putBoolean( String key, boolean value ) {
		this.put( key, Boolean.toString( value ) );
	}

	public boolean getBoolean( String key, boolean def ) {
		var value = this.getOrDefault( key, def );
		if ( value instanceof String string )
			return Boolean.parseBoolean( string );
		else
			return (boolean) value;
	}

	public String get( String key, String def ) {
		return (String) this.getOrDefault( key, def );
	}

	public void saveNoException() {
		try {
			this.save();
		} catch ( IOException ignored ) { }
	}

	public void putInt( String key, int value ) {
		this.put( key, Integer.toString( value ) );
	}

	public int getInt( String key, int def ) {
		var value = this.getOrDefault( key, def );
		if ( value instanceof String string )
			return Integer.parseInt( string );
		else
			return (int) value;
	}
}
