package com.enderzombi102.mars.res;

import blue.endless.jankson.JsonObject;
import blue.endless.jankson.api.SyntaxError;
import com.enderzombi102.mars.util.Util;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import static com.enderzombi102.enderlib.SafeUtils.doSafely;
import static com.enderzombi102.mars.util.Util.JANKSON;

public class Resources {
	/**
	 * Reads a file from this manager's root path as a {@link String} object.
	 * @param path path of the file to read
	 * @return a {@link String} constructed with the contents of requested file
	 * @throws FileNotFoundException if the file doesn't exist
	 *                               inherited from {@link #readBytes(String)}
	 * @throws OutOfMemoryError      if an array of the required size cannot be allocated.<br>
	 *                               inherited from {@link #readBytes(String)}
	 * @throws IOException           if an I/O error occurs.<br>
	 *                               inherited from {@link #readBytes(String)}
	 */
	public static @NotNull String readString( @NotNull String path ) throws IOException {
		return new String( readBytes( path ) );
	}

	/**
	 * Reads a file from this manager's root path as byte array.
	 * @param path path of the file to read
	 * @return a byte array with the contents of the requested file
	 * @throws FileNotFoundException if the file doesn't exist
	 * @throws OutOfMemoryError      if an array of the required size cannot be allocated.<br>
	 *                               inherited from {@link InputStream#readAllBytes()}
	 * @throws IOException           if an I/O error occurs.<br>
	 *                               inherited from {@link InputStream#readAllBytes()}
	 */
	public static byte @NotNull [] readBytes( @NotNull String path ) throws IOException {
		try ( var stream = Util.class.getResourceAsStream( path ) ) {
			if ( stream == null )
				throw new FileNotFoundException( "File \"" + path + "\" does not exists!" );
			return stream.readAllBytes();
		}
	}

	/**
	 * Reads an image from this manager's root path.
	 * @param path path of the image to read
	 * @return a {@link BufferedImage} instance of the requested image
	 * @throws FileNotFoundException if the image doesn't exist
	 * @throws IOException 			 thrown if the image failed to read.
	 * 								 also inherited from {@link ImageIO#read(InputStream)}
	 */
	public static @NotNull BufferedImage readImage( @NotNull String path ) throws IOException {
		try ( var stream = Util.class.getResourceAsStream( path ) ) {
			if ( stream == null )
				throw new FileNotFoundException( "File \"" + path + "\" does not exists!" );

			@Nullable var buf = ImageIO.read( stream );
			if ( buf == null )
				throw new IOException( "Failed to read image at '" + path + "'" );
			return buf;
		}
	}

	/**
	 * Used to get the url of a file for manual processing
	 * @param path path of the file to create the url for
	 * @return an {@link URL} object that represents the requested resource's location
	 */
	public static @Nullable URL getUrl( @NotNull String path ) {
		return Util.class.getResource( path );
	}

	/**
	 * Used to get the url of a file for manual processing
	 * @param path path of the file to create the url for
	 * @return an {@link InputStream} for the requested resource
	 * @throws IOException if a IO error occurs
	 * @throws FileNotFoundException if the requested resource doesn't exist
	 */
	public static @NotNull InputStream getStream( @NotNull String path ) throws IOException {
		var stream = Util.class.getResourceAsStream( path );
		if ( stream == null )
			throw new FileNotFoundException( "File \"" + path + "\" does not exists!" );
		return stream;
	}

	public static @NotNull JsonObject loadJson( @NotNull String path ) throws IOException, SyntaxError {
		return JANKSON.load( readString( path ) );
	}

	public static @NotNull Icon getIcon( @NotNull String path ) {
		return new ImageIcon( doSafely( () -> readImage( path ) ) );
	}
}
