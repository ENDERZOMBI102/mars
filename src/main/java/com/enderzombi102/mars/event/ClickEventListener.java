package com.enderzombi102.mars.event;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.function.Consumer;

public class ClickEventListener extends MouseAdapter {
	private final Consumer<MouseEvent> lambda;

	public ClickEventListener(Consumer<MouseEvent> lambda ) {
		this.lambda = lambda;
	}

	@Override
	public void mouseClicked( MouseEvent evt ) {
		this.lambda.accept( evt );
	}
}
