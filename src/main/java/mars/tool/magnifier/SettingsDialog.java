package mars.tool.magnifier;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Specialized class for the magnifier's settings dialog.
 */

class SettingsDialog extends JDialog {
	JButton applyButton, cancelButton;
	JCheckBox captureResizeCheckBox, captureMoveCheckBox, captureRescaleCheckBox;
	JRadioButton captureDisplayCenteredButton, captureDisplayUpperleftButton;
	Integer[] scribblerLineWidthSettings = {1, 2, 3, 4, 5, 6, 7, 8};
	JComboBox<Integer> lineWidthSetting;
	JButton lineColorSetting;
	JCheckBox dialogCentered; // Whether or not dialog appears centered over the magnfier frame.
	JDialog dialog;
	// temporary storage until committed with "Apply".  Needed because it is returned
	// by same call that shows the color selection dialog, so cannot be retrieved
	// later from the model (as you can with buttons, checkboxes, etc).
	Color scribblerLineColorSetting;
	// Text for tool tips.
	static final String SETTINGS_APPLY_TOOLTIP_TEXT = "Apply current settings and close the dialog.";
	static final String SETTINGS_CANCEL_TOOLTIP_TEXT = "Close the dialog without applying any setting changes.";
	static final String SETTINGS_SCRIBBLER_WIDTH_TOOLTIP_TEXT = "Scribbler line thickness in pixels.";
	static final String SETTINGS_SCRIBBLER_COLOR_TOOLTIP_TEXT = "Click here to change Scribbler line color.";
	static final String SETTINGS_DIALOG_CENTERED_TOOLTIP_TEXT = "Whether to center this dialog over the Magnifier.";

	SettingsDialog(final JFrame frame) {
		super(frame, "Magnifier Tool Settings");
		dialog = this;
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		final Container contentPane = getContentPane();
		contentPane.setLayout(new BorderLayout());
		new JPanel();
		final JPanel selectionsPanel = new JPanel(new GridLayout(2, 1));
		selectionsPanel.add(getCaptureDisplayPanel());
		final JPanel secondRow = new JPanel(new GridLayout(1, 2));
		secondRow.add(getAutomaticCaptureSettingsPanel());
		secondRow.add(getScribblerPanel(this));
		selectionsPanel.add(secondRow);
		contentPane.add(selectionsPanel);
		contentPane.add(getButtonRowPanel(), BorderLayout.SOUTH);
		pack();
		if (dialogCentered.isSelected()) {
			setLocationRelativeTo(frame);
		}
		setVisible(true);
	}

	// This panel contains the control buttons for the Settings Dialog.
	private JPanel getButtonRowPanel() {
		final JPanel buttonRow = new JPanel();
		applyButton = new JButton("Apply and Close");
		applyButton.setToolTipText(SETTINGS_APPLY_TOOLTIP_TEXT);
		// Action to perform when APply button pressed: commit GUI settings to the models
		applyButton.addActionListener(e -> {
			// commit settings
			((Magnifier) getOwner()).captureResize.setEnabled(captureResizeCheckBox.isSelected());
			((Magnifier) getOwner()).captureMove.setEnabled(captureMoveCheckBox.isSelected());
			((Magnifier) getOwner()).captureRescale.setEnabled(captureRescaleCheckBox.isSelected());
			((Magnifier) getOwner()).captureDisplayCenter.setEnabled(captureDisplayCenteredButton.isSelected());
			((Magnifier) getOwner()).captureDisplayUpperleft.setEnabled(captureDisplayUpperleftButton.isSelected());
			((Magnifier) getOwner()).dialogDisplayCenter.setEnabled(dialogCentered.isSelected());
			if (captureDisplayCenteredButton.isSelected())
				((Magnifier) getOwner()).alignment = new CaptureDisplayCentered();
			else if (captureDisplayUpperleftButton.isSelected())
				((Magnifier) getOwner()).alignment = new CaptureDisplayUpperleft();
			((Magnifier) getOwner()).scribblerSettings.setLineWidth(scribblerLineWidthSettings[lineWidthSetting.getSelectedIndex()]);
			((Magnifier) getOwner()).scribblerSettings.setLineColor(lineColorSetting.getBackground());
			dialog.dispose();
		});
		cancelButton = new JButton("Cancel");
		cancelButton.setToolTipText(SETTINGS_CANCEL_TOOLTIP_TEXT);
		cancelButton.addActionListener(e -> dialog.dispose());
		// By default, display dialog centered over the Magnifier's frame.  This
		// can be changed however to display at upper left corner of screen.  Why
		// would you want to change this?  So you can use the settings dialog to
		// to change scribbler color and/or width without wiping out current
		// scribbler marks.  If the dialog is centered, its mere display may
		// wipe out existing scribbler marks (because they are not repainted
		// when the underlying image is repainted).
		dialogCentered = new JCheckBox("Dialog centered", ((Magnifier) getOwner()).dialogDisplayCenter.isEnabled());
		dialogCentered.setToolTipText(SETTINGS_DIALOG_CENTERED_TOOLTIP_TEXT);
		buttonRow.add(applyButton);
		buttonRow.add(cancelButton);
		buttonRow.add(dialogCentered);
		return buttonRow;
	}

	// Panel that contains settings for automatically performing an image
	// capture.  These are a convenience, as the image can always be
	// manually captured by clicking the "Capture" button.
	private JPanel getAutomaticCaptureSettingsPanel() {
		final JPanel automaticCaptureSettings = new JPanel();
		automaticCaptureSettings.setBorder(new TitledBorder("Automatic Capture"));
		final Box automaticCaptureSettingsBox = Box.createHorizontalBox();
		automaticCaptureSettings.add(automaticCaptureSettingsBox);
		captureResizeCheckBox = new JCheckBox("Capture upon resize", ((Magnifier) getOwner()).captureResize.isEnabled());
		captureMoveCheckBox = new JCheckBox("Capture upon move", ((Magnifier) getOwner()).captureMove.isEnabled());
		captureRescaleCheckBox = new JCheckBox("Capture upon rescale", ((Magnifier) getOwner()).captureRescale.isEnabled());
		final JPanel checkboxColumn = new JPanel(new GridLayout(3, 1));
		checkboxColumn.add(captureResizeCheckBox);
		checkboxColumn.add(captureMoveCheckBox);
		checkboxColumn.add(captureRescaleCheckBox);
		automaticCaptureSettingsBox.add(checkboxColumn);
		return automaticCaptureSettings;
	}

	// Panel that contains settings for extent and display of image upon
	// capture.  In version 1.0, the extent is fixed; it is same as that
	// of the tool's frame itself.  The term "extent" refers to the location
	// and dimension of the rectangle.
	private JPanel getCaptureDisplayPanel() {
		final JPanel captureDisplaySetting = new JPanel();
		captureDisplaySetting.setBorder(new TitledBorder("Capture and Display"));
		final Box captureDisplaySettingsBox = Box.createHorizontalBox();
		captureDisplaySetting.add(captureDisplaySettingsBox);
		captureDisplayCenteredButton = new JRadioButton("Capture area behind magnifier and display centered",
				((Magnifier) getOwner()).captureDisplayCenter.isEnabled());
		captureDisplayUpperleftButton = new JRadioButton("Capture area behind magnifier and display upper-left",
				((Magnifier) getOwner()).captureDisplayUpperleft.isEnabled());
		final ButtonGroup displayButtonGroup = new ButtonGroup();
		displayButtonGroup.add(captureDisplayCenteredButton);
		displayButtonGroup.add(captureDisplayUpperleftButton);
		final JPanel radioColumn = new JPanel(new GridLayout(2, 1));
		radioColumn.add(captureDisplayCenteredButton);
		radioColumn.add(captureDisplayUpperleftButton);
		new JPanel(new GridLayout(1, 1));
		captureDisplaySettingsBox.add(radioColumn);
		return captureDisplaySetting;
	}

	// Panel that contains settings for the Scribbler part of the tool.
	// The only settings here are choice of line width (thickness) in pixels
	// and line color.
	private JPanel getScribblerPanel(final JDialog dialog) {
		final JPanel scribblerSettings = new JPanel();
		scribblerSettings.setBorder(new TitledBorder("Scribbler"));
		final Box scribblerSettingsBox = Box.createHorizontalBox();
		scribblerSettings.add(scribblerSettingsBox);
		lineWidthSetting = new JComboBox<>(scribblerLineWidthSettings);
		lineWidthSetting.setToolTipText(SETTINGS_SCRIBBLER_WIDTH_TOOLTIP_TEXT);
		lineWidthSetting.setSelectedIndex( ( (Magnifier) getOwner() ).scribblerSettings.getLineWidth() - 1 );
		lineColorSetting = new JButton("   ");
		lineColorSetting.setToolTipText(SETTINGS_SCRIBBLER_COLOR_TOOLTIP_TEXT);
		lineColorSetting.setBackground( ( (Magnifier) getOwner() ).scribblerSettings.getLineColor() );
		lineColorSetting.addActionListener( e -> lineColorSetting.setBackground(
			JColorChooser.showDialog(
				dialog,
				"Scribbler line color",
				lineColorSetting.getBackground()
			)
		));
		scribblerLineColorSetting = lineColorSetting.getBackground();
		final JPanel settingsColumn = new JPanel(new GridLayout(2, 1, 5, 5));
		settingsColumn.add(lineWidthSetting);
		settingsColumn.add(lineColorSetting);
		final JPanel labelColumn = new JPanel(new GridLayout(2, 1, 5, 5));
		labelColumn.add(new JLabel("Line width ", SwingConstants.LEFT));
		labelColumn.add(new JLabel("Line color ", SwingConstants.LEFT));
		scribblerSettingsBox.add(labelColumn);
		scribblerSettingsBox.add(settingsColumn);
		return scribblerSettings;
	}

}
