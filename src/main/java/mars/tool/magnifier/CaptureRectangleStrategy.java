package mars.tool.magnifier;

import java.awt.*;

/**
 * Interface to specify strategy for determining the size and location of the
 * screen rectangle to capture.
 */
interface CaptureRectangleStrategy {
	Rectangle getCaptureRectangle(Rectangle magnifierRectangle);
}
