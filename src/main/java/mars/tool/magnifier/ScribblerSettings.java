package mars.tool.magnifier;

import java.awt.*;

/**
 * Class to represent current settings for the scribbler tool.
 */
class ScribblerSettings {

	private int width;
	private Color color;

	/**
	 * Build a new ScribblerSettings object.
	 */
	public ScribblerSettings(final int width, final Color color) {
		this.width = width;
		this.color = color;
	}

	/**
	 * Fetch the current line width for the scribbler tool.
	 */
	public int getLineWidth() {
		return width;
	}

	/**
	 * Fetch the current line color for the scribbler tool.
	 */
	public Color getLineColor() {
		return color;
	}

	/**
	 * Set the current line width for the scribbler tool.
	 */
	public void setLineWidth(final int newWidth) {
		width = newWidth;
	}

	/**
	 * Set the current line color for the scribbler tool.
	 */
	public void setLineColor(final Color newColor) {
		color = newColor;
	}
}
