package mars.tool.magnifier;

/**
 * Represents an automatic capture "event" which is enabled if the capture
 * operation is to be performed automatically upon occurance.
 */
class CaptureModel {

	private boolean enabled;

	/**
	 * Create a new CaptureModel.
	 *
	 * @param set True if capture is to be automatically performed when associated
	 *            event occurs, false otherwise.
	 */
	public CaptureModel(final boolean set) {
		enabled = set;
	}

	/**
	 * Determine whether or not capture will automatically occur.
	 *
	 * @return True if automatic capture will occur, false otherwise.
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Specify whether or not capture will automatically occur.
	 *
	 * @param set True if capture is to be automatically performed when associated
	 *            event occurs, false otherwise.
	 */
	public void setEnabled(final boolean set) {
		enabled = set;
	}

}
