package mars.tool.magnifier;

import javax.swing.*;

/**
 * Captured and scaled image should be displayed so that its upper left corner
 * is initially displayed in the upper left corner of the panel.
 */
class CaptureDisplayUpperleft implements CaptureDisplayAlignmentStrategy {

	/**
	 * Set the scrollbar value to inticate the captured and scaled image is
	 * displayed with the upper left corner initially visible in the panel.
	 *
	 * @param scrollBar The scrollbar to be adjusted.
	 */
	@Override
	public void setScrollBarValue(final JScrollBar scrollBar) {
		scrollBar.setValue(0);
	}
}
