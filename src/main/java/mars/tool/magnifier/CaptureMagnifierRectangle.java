package mars.tool.magnifier;

import java.awt.*;

/**
 * Upon screen capture, capture the same rectangle as the magnifier itself.
 * Pixels that lie directly beneath it.
 */
class CaptureMagnifierRectangle implements CaptureRectangleStrategy {

	@Override
	public Rectangle getCaptureRectangle(final Rectangle magnifierRectangle) {
		return magnifierRectangle;
	}
}
