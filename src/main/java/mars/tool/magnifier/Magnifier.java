package mars.tool.magnifier;

import mars.Globals;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;

class Magnifier extends JFrame implements ComponentListener {
	static Robot robot;
	JButton close, capture, settings;
	JSpinner scaleAdjuster;
	JScrollPane view;
	Dimension viewSize;
	MagnifierImage magnifierImage;
	ActionListener captureActionListener;
	CaptureModel captureResize, captureMove, captureRescale;
	CaptureModel captureDisplayCenter, captureDisplayUpperleft;
	CaptureModel dialogDisplayCenter;
	ScribblerSettings scribblerSettings;
	static final double SCALE_MINIMUM = 1.0;
	static final double SCALE_MAXIMUM = 4.0;
	static final double SCALE_INCREMENT = 0.5;
	static final double SCALE_DEFAULT = 2.0;
	double scale = SCALE_DEFAULT;
	CaptureDisplayAlignmentStrategy alignment;
	CaptureRectangleStrategy captureLocationSize = new CaptureMagnifierRectangle();
	JFrame frame;
	static final String CAPTURE_TOOLTIP_TEXT = "Capture, scale, and display pixels that lay beneath the Magnifier.";
	static final String SETTINGS_TOOLTIP_TEXT = "Show dialog for changing tool settings.";
	static final String SCALE_TOOLTIP_TEXT = "Magnification scale for captured image.";
	static final String CLOSE_TOOLTIP_TEXT = "Exit the Screen Magnifier.  Changed settings are NOT retained.";

	Magnifier() {
		super("Screen Magnifier 1.0");
		frame = this;
		createSettings();
		// If running withint MARS, set to its icon image; if not fuggetit.
		try {
			setIconImage(mars.Globals.getGui().getIconImage());
		} catch (final Exception ignored) { }
		getContentPane().setLayout(new BorderLayout());
		// Will capture an image each time frame is moved/resized.
		addComponentListener(this);
		try {
			robot = new Robot();
		} catch (final AWTException | SecurityException ignored) { }

		close = new JButton("Close");
		close.setToolTipText(CLOSE_TOOLTIP_TEXT);
		close.addActionListener(e -> setVisible(false));
		settings = new JButton("Settings...");
		settings.setToolTipText(SETTINGS_TOOLTIP_TEXT);
		settings.addActionListener(e -> new SettingsDialog(frame));
		magnifierImage = new MagnifierImage(this);
		view = new JScrollPane(magnifierImage);
		viewSize = new Dimension(200, 150);
		view.setSize(viewSize);

		capture = new JButton("Capture");
		capture.setToolTipText(CAPTURE_TOOLTIP_TEXT);
		captureActionListener = e -> {
			magnifierImage.setImage(
				MagnifierImage.getScaledImage(
					captureScreenSection( captureLocationSize.getCaptureRectangle( getFrameRectangle() ) ),
					scale
				)
			);
			alignment.setScrollBarValue(view.getHorizontalScrollBar());
			alignment.setScrollBarValue(view.getVerticalScrollBar());
		};
		final JLabel scaleLabel = new JLabel("Scale: ");
		final var scaleModel = new SpinnerNumberModel( SCALE_DEFAULT, SCALE_MINIMUM, SCALE_MAXIMUM, SCALE_INCREMENT );
		scaleAdjuster = new JSpinner(scaleModel);
		scaleAdjuster.setToolTipText(SCALE_TOOLTIP_TEXT);
		final JSpinner.NumberEditor scaleEditor = new JSpinner.NumberEditor(scaleAdjuster, "0.0");
		scaleEditor.getTextField().setEditable(false);
		scaleAdjuster.setEditor(scaleEditor);
		scaleAdjuster.addChangeListener(e -> {
			scale = ((Double) scaleAdjuster.getValue());
			if ( captureRescale.isEnabled() )
				captureActionListener.actionPerformed( new ActionEvent(frame, 0, "capture") );
		});
		final JPanel scalePanel = new JPanel();
		scalePanel.add(scaleLabel);
		scalePanel.add(scaleAdjuster);
		capture.addActionListener(captureActionListener);
		final Box buttonRow = Box.createHorizontalBox();
		buttonRow.add(Box.createHorizontalStrut(4));
		buttonRow.add(capture);
		buttonRow.add(Box.createHorizontalGlue());
		buttonRow.add(settings);
		buttonRow.add(scalePanel);
		buttonRow.add(Box.createHorizontalGlue());
		buttonRow.add(getHelpButton());
		buttonRow.add(Box.createHorizontalGlue());
		buttonRow.add(close);
		buttonRow.add(Box.createHorizontalStrut(4));
		getContentPane().add(view, BorderLayout.CENTER);
		getContentPane().add(buttonRow, BorderLayout.SOUTH);
		pack();
		setSize(500, 400);
		setLocationRelativeTo(null); // center on screen
		setVisible(true);
		// For some strange reason, the image has to be captured
		// and displayed an extra time for the display justification
		// to be recognized for the scrollbars.  The first capture
		// will justify left-center no matter what (scrollbar
		// positions 0).
		captureActionListener.actionPerformed(new ActionEvent(frame, 0, "capture"));
		captureActionListener.actionPerformed(new ActionEvent(frame, 0, "capture"));
	}

	/*
	 *  Create the default Screen Magnifier tool settings.  These can
	 *  all be changed through the Settings dialog but are not persistent
	 *  across activations of the tool.
	 */
	private void createSettings() {
		// Which events will cause automatic re-capture?  Pick any
		// or all of these three: resize the frame, move the frame,
		// change the magnification scale (using spinner).
		captureResize = new CaptureModel(false);
		captureMove = new CaptureModel(false);
		captureRescale = new CaptureModel(true);
		// When capture is taken, how shall it be displayed in the view
		// panel?  Scrollbars will be present since the displayed image
		// has to be larger than the viewing panel.  Display it either
		// with scrollbars centered, or scrollbars at initial position
		// (upper-left corner of image at upper-left corner of viewer).
		alignment = new CaptureDisplayCentered();// CaptureDisplayUpperleft();
		// Once the alignment is set, these will correctly self-set.
		captureDisplayCenter = new CaptureModel(alignment instanceof CaptureDisplayCentered);
		captureDisplayUpperleft = new CaptureModel(alignment instanceof CaptureDisplayUpperleft);
		// Scribbler has two settings: line width in pixels and line color.
		scribblerSettings = new ScribblerSettings(2, Color.RED);
		// Whether or not to center the Settings dialog over the Magnifier frame.
		dialogDisplayCenter = new CaptureModel(true);
	}

	// A simple explanation of what the tool does.
	private JButton getHelpButton() {
		final String helpContent = """
				Use this utility tool to display a magnified image of a
				screen section and highlight things on the image.  This
				will be of interest mainly to instructors.
								
				To capture an image, size and position the Screen Magnifier
				over the screen segment to be magnified and click "Capture".
				The pixels beneath the magnifier will be captured, scaled,
				and displayed in a scrollable window.
								
				To highlight things in the image, just drag the mouse over
				the image to make a scribble line.  This line is ephemeral
				(is not repainted if covered then uncovered).
								
				The magnification scale can be adjusted using the spinner.
				Other settings can be adjusted through the Settings dialog.
				Settings include: justification of displayed image, automatic
				capture upon certain tool events, and the thickness and color
				of the scribble line.
								
				LIMITS: The image is static; it is not updated when the
				underlying pixels change.  Scale changes do not take effect
				until the next capture (but you can set auto-capture).  The
				Magnifier does not capture frame contents of other tools.
				Setting changes are not saved when the tool is closed.
								
				Contact Pete Sanderson at psanderson@otterbein.edu with
				questions or comments
				""";
		final JButton help = new JButton("Help");
		help.addActionListener(e -> JOptionPane.showMessageDialog(frame, helpContent));
		return help;
	}

	/**
	 * Capture the pixels of the specified screen rectangle into an ImageBuffer. The
	 * trick is the ScreenMagnifier's frame has to be made invisible first so it
	 * does not show up in the image.
	 *
	 * @param section A rectangle specifying the range of pixels to capture.
	 * @return A BufferedImage containing the captured pixel range.
	 */
	BufferedImage captureScreenSection(final Rectangle section) {
		// Hide Frame so that it does not appear in the screen capture.
		setVisible(false);
		// For some reason, the graphic extent vacated by the above call
		// is not redrawn before the screen capture unless I explicitly
		// force it to be redrawn by telling the mars.Mars GUI to update.
		// If this doesn't work, e.g. getGui() returns null, then there
		// are no alternatives so just let what would happen, happen.
		try {
			Globals.getGui().update(Globals.getGui().getGraphics());
		} catch (final Exception ignored) { }
		// Perform the screen capture.
		BufferedImage imageOfSection;
		imageOfSection = robot.createScreenCapture(section);
		setVisible(true);
		return imageOfSection;
	}

	/**
	 * Place the current frame size and location into a Rectangle object.
	 *
	 * @return A Rectangle containing the ScreenMagnifier's location, plus width and
	 * height in pixels.
	 */
	Rectangle getFrameRectangle() {
		return new Rectangle(getLocation().x, getLocation().y, getSize().width, getSize().height);
	}

	//////////////////////////////////////////////////////////////////////
	//////
	////// These four methods are required for ComponentListener interface.
	////// Note: due to single inheritance, I can't extend ComponentAdaptor.
	//////
	//////////////////////////////////////////////////////////////////////

	/**
	 * Respond to frame movement event by showing new image based on the screen
	 * section of interest for this new position. This event may occur once at the
	 * end of the move or it may occur at multiple points during the move. The
	 * latter causes a flashing image, but I have not been able to determine how to
	 * limit it to one event occurance.
	 */

	// Regarding the above comment, I want to have only one capture
	// occur, at the end of the move.  But the number and timing of
	// the generated events seems to vary depending on what machine
	// I am running the program on!  I have seen a technique in which
	// componentMoved would start up a timer to go off every 100 ms or
	// so to determine if the frame location had changed since the
	// previous timer check.  If so, the frame is moving so do nothing.
	// If not, the move is assumed to be over and the image is captured
	// and the timer stopped.  The latter would also handle the case
	// where componentMoved is triggered only at the end of the move.
	// componentMoved would also have to determine whether or not
	// such a timer was already running (e.g. is this the
	// first componentMoved event?) in case it is called multiple
	// times throughout the move, not just at the end.
	@Override
	public void componentMoved(final ComponentEvent e) {
		if (captureMove.isEnabled()) {
			captureActionListener.actionPerformed(new ActionEvent(e.getComponent(), e.getID(), "capture"));
		}
	}

	/**
	 * Respond to frame resize event by showing new image based on the screen
	 * section of interest for this new size. This event may occur once at the end
	 * of the resize or it may occur at multiple points during the resize. The
	 * latter causes a flashing image, but I have not been able to determine how to
	 * limit it to one event occurance.
	 */
	// See comments above for possible technique for assuring only one
	// capture at the end of the resize.
	@Override
	public void componentResized(final ComponentEvent e) {
		if (captureResize.isEnabled()) {
			captureActionListener.actionPerformed(new ActionEvent(e.getComponent(), e.getID(), "capture"));
		}
	}

	/**
	 * Invoked when setVisible(true) is called, do nothing.
	 */
	@Override
	public void componentShown(final ComponentEvent e) {
	}

	/**
	 * Invoked when setVisible(false) is called, do nothing.
	 */
	@Override
	public void componentHidden(final ComponentEvent e) {
	}

}
