package mars.tool.magnifier;

import javax.swing.*;

/**
 * Captured and scaled image should be displayed centerd in the panel.
 */
class CaptureDisplayCentered implements CaptureDisplayAlignmentStrategy {

	/**
	 * Set the scrollbar value to inticate the captured and scaled image is
	 * displayed centered in the panel.
	 *
	 * @param scrollBar The scrollbar to be adjusted.
	 */
	@Override
	public void setScrollBarValue(final JScrollBar scrollBar) {
		scrollBar.setValue(
			(
				scrollBar.getModel().getMaximum() - scrollBar.getModel().getMinimum() - scrollBar.getModel().getExtent()
			) / 2
		);
	}
}
