package mars.tool.magnifier;

import javax.swing.*;

/**
 * Interface to specify strategy for determining initial scrollbar settings when
 * displaying captured and scaled image.
 */
interface CaptureDisplayAlignmentStrategy {
	void setScrollBarValue(JScrollBar scrollBar);
}
