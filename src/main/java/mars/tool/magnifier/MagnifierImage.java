package mars.tool.magnifier;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.geom.Line2D;

/**
 * This class defines a specialized panel for displaying a captured image.
 */

class MagnifierImage extends JPanel {
	// Enclosing JFrame for this panel -- the Screen Magnifier itself.
	private final Magnifier frame;
	// Displayed image's Image object, which is actually a BufferedImage.
	private Image image;
	// Scribbler for highlighting image using mouse.
	private final Scribbler scribbler;

	/**
	 * Construct an MagnifierImage component.
	 */
	public MagnifierImage(final Magnifier frame) {
		this.frame = frame;
		scribbler = new Scribbler(frame.scribblerSettings);

		addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(final MouseEvent e) {
				scribbler.moveto(e.getX(), e.getY()); // Move to click position
			}
		});

		// Install a mouse motion listener to draw the scribble.
		addMouseMotionListener(new MouseMotionAdapter() {

			@Override
			public void mouseDragged(final MouseEvent e) {
				scribbler.lineto(e.getX(), e.getY(), (Graphics2D) getGraphics());
			}
		});
	}

	/**
	 * Return the current image.
	 *
	 * @return Image reference to current image
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * Repaint the MagnifierImage with the current image's pixels.
	 *
	 * @param g graphics context
	 */
	@Override
	public void paintComponent(final Graphics g) {
		// Repaint the component's background.
		super.paintComponent(g);
		// If an image has been defined, draw that image using the Component
		// layer of this MagnifierImage object as the ImageObserver.
		if (image != null) {
			g.drawImage(image, 0, 0, this);
		}
	}

	/**
	 * Establish a new image and update the display.
	 *
	 * @param image new image's Image reference
	 */
	public void setImage(final Image image) {
		// Save the image for later repaint.
		this.image = image;
		// Set this panel's preferred size to the image's size, to influence the
		// display of scrollbars.
		setPreferredSize(new Dimension(image.getWidth(this), image.getHeight(this)));
		// Present scrollbars as necessary.
		revalidate();
		// Update the image displayed on the panel.
		repaint();
	}

	/**
	 * Get a scaled version of the image. Ignores scaling values between .99 and
	 * 1.01.
	 *
	 * @param image          the original image
	 * @param scale          the magnification scale as a double
	 * @param scaleAlgorithm Scaling algorithm to use: Image.SCALE_DEFAULT,
	 *                       Image.SCALE_FAST, Image.SCALE_SMOOTH.
	 */
	static Image getScaledImage(final Image image, final double scale, final int scaleAlgorithm) {
		// Don't bother if it is close to 1.  I anticipate this will be used mainly to
		// enlarge the image, so short circuit evalution will apply most of the time.
		return scale < 1.01 && scale > 0.99 ? image
				: image.getScaledInstance((int) (image.getWidth(null) * scale), (int) (image.getHeight(null) * scale),
				scaleAlgorithm);
	}

	/**
	 * Get a scaled version of the image using default scaling algorithm. Ignores
	 * scaling values between .99 and 1.01.
	 *
	 * @param image the original image
	 * @param scale the magnification scale as a double
	 */
	static Image getScaledImage(final Image image, final double scale) {
		return getScaledImage(image, scale, Image.SCALE_DEFAULT);
	}

	/*
	 *  Little class to allow user to scribble over the image using the
	 *  mouse.  The scribble is ephemeral; it is drawn but specifications
	 *  are not saved.
	 */
	private static class Scribbler {

		private final ScribblerSettings scribblerSettings;
		private BasicStroke drawingStroke;
		// coordinates of previous mouse position
		protected int last_x, last_y;

		Scribbler(final ScribblerSettings scribblerSettings) {
			this.scribblerSettings = scribblerSettings;
			drawingStroke = new BasicStroke(scribblerSettings.getLineWidth());
		}

		/**
		 * Get the scribbler's drawing color.
		 */
		public Color getColor() {
			return scribblerSettings.getLineColor();//color;
		}

		/**
		 * Get the scribbler's line (stroke) width.
		 */
		public int getLineWidth() {
			drawingStroke = new BasicStroke(scribblerSettings.getLineWidth());
			return scribblerSettings.getLineWidth();//width;
		}

		/**
		 * Set the scribbler's drawing color.
		 */
		public void setColor(final Color newColor) {
			scribblerSettings.setLineColor(newColor);// this.color = newColor;
		}

		/**
		 * Set the scribbler's line (stroke) width.
		 */
		public void setLineWidth(final int newWidth) {
			scribblerSettings.setLineWidth(newWidth);// this.width = newWidth;
			drawingStroke = new BasicStroke(newWidth);
		}

		/**
		 * Get the scribbler's drawing (stroke) object.
		 */
		private BasicStroke getStroke() {
			return drawingStroke;
		}

		/**
		 * Set the scribbler's drawing (stroke) object.
		 */
		private void setStroke(final BasicStroke newStroke) {
			drawingStroke = newStroke;
		}

		/**
		 * Remember the specified point
		 */
		public void moveto(final int x, final int y) {
			last_x = x;
			last_y = y;
		}

		/**
		 * Draw from the last point to this point, then remember new point
		 */
		public void lineto(final int x, final int y, final Graphics2D g2d) {
			// System.out.println(drawingStroke.getLineWidth());
			g2d.setStroke(new BasicStroke(scribblerSettings.getLineWidth()));
			g2d.setColor(scribblerSettings.getLineColor()); // Tell it what color to use
			g2d.draw(new Line2D.Float(last_x, last_y, x, y));
			moveto(x, y); // Save the current point
		}
	}

}
