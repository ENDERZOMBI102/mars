package mars.tool.scavenger;

/**
 * Used to define (X,Y) coordinate of a location to which ScavengerHunt players
 * will travel.
 */
class Location {
	public int X;
	public int Y;
}
