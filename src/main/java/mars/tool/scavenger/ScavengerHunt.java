package mars.tool.scavenger;

import mars.Globals;
import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.AddressErrorException;
import mars.mips.hardware.Memory;
import mars.mips.hardware.MemoryAccessNotice;
import mars.tool.MarsTool;
import mars.util.Binary;

import javax.swing.*;
import java.util.Random;

import static mars.tool.scavenger.Constants.*;


/**
 * Demo of mars.Mars tool capability. Ken Vollmar, 27 Oct 2006
 * KenVollmar@missouristate.edu This tool displays movements by a series of
 * players in a game of ScavengerHunt. Players will read and write MIPS
 * memory-mapped locations to move and regain energy. See accompanying
 * documentation for memory-mapped addresses, rules of the game, etc.
 */
public class ScavengerHunt implements MarsTool {
	ScavengerHuntDisplay graphicArea;
	private int authenticationValue = 0;
	boolean GameOn = false;  // MIPS programs readiness
	private static int SetWordCounter = 0;
	private static int playerID = ADMINISTRATOR_ID;   // Range 0...(NUM_PLAYERS-1), plus ADMINISTRATOR_ID
	private final boolean KENVDEBUG = false;

	static PlayerData[] pd = new PlayerData[NUM_PLAYERS];
	static Location[] loc = new Location[NUM_LOCATIONS];
	private Random randomStream;
	long startTime;

	@Override
	public String getName() {
		return "ScavengerHunt";
	}

	/*
	 * This will set up the ScavengerHunt's GUI.  Invoked when ScavengerHunt menu item selected.
	 */
	@Override
	public void action() {
		final ScavengerHuntRunnable shr = new ScavengerHuntRunnable(this);
		final Thread t1 = new Thread(shr);
		t1.start();

		// Register as observer for a particular MIPS data range. Other ranges
		// are not used by this Tool.
		try {
			Globals.memory.addObserver(this::onMemoryChange, 0xffff8000, 0xfffffff0);  // must be on word boundaries
		} catch (final AddressErrorException e) {
			System.out.println("\n\nScavengerHunt.action: Globals.memory.addObserver caused AddressErrorException.\n\n");
			System.exit(0);
		}

	}

	/*
	 * This method observes MIPS memory for directives to modify ScavengerHunt activity (that is,
	 * MIPS program write to MMIO) and updates instance variables to reflect that directive.
	 * This method takes action when it "observes" MIPS memory changes -- but this method
	 * must not write to those memory locations in order to prevent an infinite cycle of events.
	 * This method observes certain locations and then may (and does) write to OTHER locations.
	 */
	public void onMemoryChange( final Memory o, final Object arg) {
		int address;
		int data;
		boolean isWrite;
		int energyLevel;

		// Here we are only interested in MemoryAccessNotice. For anything else, just return.
		if (! ( arg instanceof MemoryAccessNotice notice) )
			return;

		// Get pertinent information about this MemoryAccessNotice.
		address = notice.getAddress();
		data = notice.getValue();
		isWrite = notice.getAccessType() == AccessNotice.WRITE;

		// If we are only interested in MIPS memory WRITES, then just return on READS.
		// That's a matter of policy: perhaps players should be prohibited from
		// reading each other's memory spaces.
		if (!isWrite)
			return;

		// Take the appropriate action, depending on data written and priority of user.
		if ( playerID == ADMINISTRATOR_ID && address == ADDR_GAME_ON ) {
			// No need to authenticate since administrator runs first, then this location has no effect thereafter.
			GameOn = true;
			initializeScavengerData();
		} else if ( address == ADDR_AUTHENTICATION ) {
			// Anyone is allowed to write to the authentication location -- but if that value is not
			// correct (authentic) then action can be taken.
			// NO ACTION HERE
		} else if ( address == ADDR_NUM_TURNS ) {
			// Anyone is allowed to write to the "number of turns" location
			// NO ACTION HERE
		} else if ( address == ADDR_PLAYER_ID ) {
			// if the data written will change the PlayerID, authenticate the write
			// 2006 Oct 31  dummy validation scheme, suitable for distribution
			// to students for development: Initial authentication value is zero.
			// Each successive authentication value is one greater
			// than the preceding value, modulo 0xffffffff.
			authenticationValue += 1;  // "server's" updated version of the authenticationValue
			if (toolGetWord(ADDR_AUTHENTICATION) == authenticationValue) { // Compare to "client's" version of the authenticationValue
				playerID = toolGetWord(ADDR_PLAYER_ID); // Use the new player ID
			} else {
				System.out.println(
					"ScavengerHunt.update(): Invalid write of player ID! \nPlayer "
					+ playerID
					+ " tried to write.  Expected:   "
					+ Binary.intToHexString(authenticationValue)
					+ ", got:  "
					+ Binary.intToHexString(toolGetWord(ADDR_AUTHENTICATION))
					+ "\n"
				);
			}
		} else if ( address == ADDR_BASE + playerID * MEM_PER_PLAYER + OFFSET_MOVE_READY && data != 0 ) {
			//  Player wrote data to their assigned MoveReady location{
			energyLevel = toolReadPlayerData(playerID, OFFSET_ENERGY); // find if player has energy
			if (energyLevel <= 0)  // No energy. Player not allowed to move
				return;

			if (
				toolReadPlayerData(playerID, OFFSET_MOVE_TO_X) < 0 ||
				toolReadPlayerData(playerID, OFFSET_MOVE_TO_X) > GRAPHIC_WIDTH ||
				toolReadPlayerData(playerID, OFFSET_MOVE_TO_Y) < 0 ||
				toolReadPlayerData(playerID, OFFSET_MOVE_TO_Y) > GRAPHIC_HEIGHT
			) { // Out of bounds. Player not allowed to move
				System.out.println("Player " + playerID + " can't move -- out of bounds.");
				return;
			}

			// Verify movement is allowed (does not exceed maximum movement)
			if (
				Math.sqrt(
					Math.pow(
						toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_X) - toolReadPlayerData(playerID, OFFSET_MOVE_TO_X),
						2.0
					) + Math.pow(
						toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_Y) - toolReadPlayerData(playerID, OFFSET_MOVE_TO_Y),
						2.0
					)
				) <= MAX_MOVE_DISTANCE
			) {
				// Write the new position of the player
				toolWritePlayerData(playerID, OFFSET_WHERE_AM_I_X, toolReadPlayerData(playerID, OFFSET_MOVE_TO_X));
				toolWritePlayerData(playerID, OFFSET_WHERE_AM_I_Y, toolReadPlayerData(playerID, OFFSET_MOVE_TO_Y));
				pd[playerID].setWhereAmI(
					toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_X),
					toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_Y)
				);

				// Write the new (reduced) energy of the player
				// Policy: Constant ENERGY_PER_MOVE for any move regardless of length.
				toolWritePlayerData(
					playerID,
					OFFSET_ENERGY,
					toolReadPlayerData(playerID, OFFSET_ENERGY) - ENERGY_PER_MOVE
				);
				pd[playerID].setEnergy(toolReadPlayerData(playerID, OFFSET_ENERGY));

				// TBD FUTURE --- need to keep track of locations that the player has actually got to
				// -- be able to tell that the player has reached a certain location
				// -- be able to tell that the player has reached every location
				for (int i = 0; i < NUM_LOCATIONS; i++) {
					if (
						toolReadPlayerData( playerID, OFFSET_WHERE_AM_I_X ) == loc[i].X &&
						toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_Y) == loc[i].Y
					) pd[playerID].setVisited(i);  // Player has visited this location
				}

				// Write 0 to "move ready" location, signifying that the move request was processed
				// Here we must write to the same location that we're now reading from, and we
				// can't cause an infinite loop. Temporarily switch player ID to that of the administrator,
				// and restore ID after the write. With the playerID set to administrator, the event
				// caused by the write will not go through this same logic.
				final int tempPlayerID = playerID;
				playerID = ADMINISTRATOR_ID;
				toolWritePlayerData(tempPlayerID, OFFSET_MOVE_READY, 0);
				playerID = tempPlayerID;
			} else {
				System.out.println("Player " + playerID + " can't move -- exceeded max. movement.");
				System.out.println(
					"    Player is at (" +
					toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_X) +
					", " +
					toolReadPlayerData(playerID, OFFSET_WHERE_AM_I_Y) +
					"), wants to go to (" +
					toolReadPlayerData(playerID, OFFSET_MOVE_TO_X) +
					"," +
					toolReadPlayerData(playerID, OFFSET_MOVE_TO_Y) +
					")"
				);
			}
		} else if ( address == ADDR_BASE + playerID * MEM_PER_PLAYER + OFFSET_TASK_COMPLETE && data != 0) {
			//  Player wrote data to their assigned TaskComplete location
			// Player indicates he/she has completed a task. Check to see if task is completed correctly.
			// Task for this assignment: Numbers are sorted in ascending order.
			int prevData, currentData;
			prevData = toolReadPlayerData(playerID, OFFSET_TASK_ARRAY);
			for (int i = 1; i < SIZE_OF_TASK; i++) {
				currentData = toolReadPlayerData(playerID, OFFSET_TASK_ARRAY + i * 4);
				if (prevData > currentData) {
					// Task failure! Task not completed correctly!
					System.out.println("Whoops! Player has NOT completed task correctly");
					return;
				}
				prevData = currentData; // update for next iteration

			}

			// If program flow has reached this point, the task is completed correctly.
			// Award energy, reset TaskComplete, and set new task values for future use.
			toolWritePlayerData(playerID, OFFSET_ENERGY, ENERGY_AWARD);
			toolWritePlayerData(playerID, OFFSET_TASK_COMPLETE, 0);
			for (int j = 0; j < SIZE_OF_TASK; j++) {  // Initialize the task data for this player
				toolWritePlayerData(playerID, OFFSET_TASK_ARRAY + j * 4, (int) (randomStream.nextDouble() * Integer.MAX_VALUE));   // Set a random number for the task (sort them)
			}
			pd[playerID].setEnergy(ENERGY_AWARD);
			// end if Player wrote nonzero data to their assigned TaskComplete location
		} else if ( address == ADDR_BASE + playerID * MEM_PER_PLAYER + OFFSET_PLAYER_COLOR) {
			//  Player wrote data to their assigned PlayerColor location
			// PPlayer indicates he/she has changed the color of display
			pd[playerID].setColor(toolReadPlayerData(playerID, OFFSET_PLAYER_COLOR));
		} else if ( address >= ADDR_BASE + playerID * MEM_PER_PLAYER && address < ADDR_BASE + (playerID + 1) * MEM_PER_PLAYER ) {
			// Player wrote data elsewhere within their assigned locatio
			// Player can write to any location within their assigned location
		} else if ( playerID != ADMINISTRATOR_ID ) {
			// only ADMINISTRATOR_ID can write to any location, because it's trusted software
			JOptionPane.showMessageDialog(
				null,
				"ScavengerHunt.update(): Player " +
				playerID +
				" writing outside assigned mem. loc. at address " +
				Binary.intToHexString(address) +
				" -- not implemented!"
			);
		}
		// Yet to be implemented: Enforce only one write of MoveRequest per player per turn
	}

	// Write one word to MIPS memory. This is a wrapper to isolate the try..catch blocks.
	private void toolSetWord(final int address, final int data) {
		if (KENVDEBUG)
			System.out.println("   ScavengerHunt.toolSetWord: Setting MIPS Memory[" + Binary.intToHexString(address) + "] to " + Binary.intToHexString(data) + " = " + data);
		SetWordCounter++;

		try {
			Globals.memory.setWord(address, data); // Write
		} catch (final AddressErrorException e) {
			System.out.println("ScavengerHunt.toolSetWord: deliberate exit on AEE exception.");
			System.out.println("     SetWordCounter = " + SetWordCounter);
			System.out.println("     address = " + Binary.intToHexString(address));
			System.out.println("     data = " + data);
			System.exit(0);
		} catch (final Exception e) {
			System.out.println("ScavengerHunt.toolSetWord: deliberate exit on " + e.getMessage() + " exception.");
			System.out.println("     SetWordCounter = " + SetWordCounter);
			System.out.println("     address = " + Binary.intToHexString(address));
			System.out.println("     data = " + data);
			System.exit(0);
		}

		if (KENVDEBUG) {
			// Verify data written correctly
			final int verifyData = toolGetWord(address);
			if (verifyData != data) {
				System.out.println("\n\nScavengerHunt.toolSetWord: Can't verify data! Special exit.");
				System.out.println("     address = " + Binary.intToHexString(address));
				System.out.println("     data = " + data);
				System.out.println("     verifyData = " + verifyData);
				System.exit(0);
			} else {
				System.out.println("  ScavengerHunt.toolSetWord: Mem[" + Binary.intToHexString(address)
						+ " verified as " + Binary.intToHexString(data));
			}
		}

	}

	// Read one word from MIPS memory. This is a wrapper to isolate the try..catch blocks.
	private int toolGetWord(final int address) {
		try {
			return Globals.memory.getWord(address);
		} catch (final AddressErrorException e) {
			System.out.println("ScavengerHunt.toolGetWord: deliberate exit on AEE exception.");
			System.out.println("     SetWordCounter = " + SetWordCounter);
			System.out.println("     address = " + Binary.intToHexString(address));
			System.exit(0);
		} catch (final Exception e) {
			System.out.println("ScavengerHunt.toolGetWord: deliberate exit on " + e.getMessage() + " exception.");
			System.out.println("     SetWordCounter = " + SetWordCounter);
			System.out.println("     address = " + Binary.intToHexString(address));
			System.exit(0);
		}

		return 0;
	}

	// Read player's data field.
	private int toolReadPlayerData(final int p, final int offset) {
		if (KENVDEBUG) {
			System.out.println(
				"ScavengerHunt.toolReadPlayerData: called with player "
				+ p + ", offset = " + Binary.intToHexString(offset) +
				" ---> address " + Binary.intToHexString(ADDR_BASE + p * MEM_PER_PLAYER + offset)
			);
		}

		final int returnValue = toolGetWord(ADDR_BASE + p * MEM_PER_PLAYER + offset);

		if (KENVDEBUG)
			System.out.println("ScavengerHunt.toolReadPlayerData: Mem[" + Binary.intToHexString(ADDR_BASE + p * MEM_PER_PLAYER + offset) + "] = " + Binary.intToHexString(returnValue) + " --- returning normally");

		return returnValue;
	}

	// Write player's data field.
	private void toolWritePlayerData(final int p, final int offset, final int data) {

		final int address = ADDR_BASE + p * MEM_PER_PLAYER + offset;

		if (KENVDEBUG)
			System.out.println("ScavengerHunt.toolWritePlayerData: called with player " + p + ", offset = " + Binary.intToHexString(offset) + ", data = " + Binary.intToHexString(data));

		toolSetWord(address, data);

		if (KENVDEBUG) {
			final int verifyData = toolGetWord(address);
			if (data != verifyData) {
				System.out.println("\n\nScavengerHunt.toolWritePlayerData: MAYDAY data not verified !");
				System.out.println("      requested data to be written was " + Binary.intToHexString(data));
				System.out.println("      actual data at that loc is " + Binary.intToHexString(toolGetWord(address)));
				System.exit(0);
			} else
				System.out.println("  ScavengerHunt.toolWritePlayerData: Mem[" + Binary.intToHexString(address) + " verified as " + Binary.intToHexString(data));
		}

	}

	void initializeScavengerData() {
		//GameOn = false;  // MIPS programs readiness
		authenticationValue = 0;
		playerID = ADMINISTRATOR_ID;
		startTime = System.currentTimeMillis();  // Clock time for program run

		// This is a dubious use of the tool (this Java program) to initialize data values for the game.
		// The administrator portion of the MIPS program should initialize all data for each
		// player -- but it's easier to work with random numbers in the Java program
		// Initialize the locations and task data for each player
		randomStream = new Random(42);  // TBD Use a seed for development. Remove seed for randomizing.

		for (int j = 0; j < NUM_LOCATIONS - 1; j++) { // The first (n-1) locations are "random"
			// Two coordinates (x and y) for each location
			loc[j] = new Location(); // Initialize each location element
			loc[j].X = (int) (randomStream.nextDouble() * GRAPHIC_WIDTH); // X coord.
			loc[j].Y = (int) (randomStream.nextDouble() * (GRAPHIC_HEIGHT - 50)); // Y coord. (leave room for buttons in window)
		}
		loc[NUM_LOCATIONS - 1] = new Location();  // The last location is a return to the starting position
		loc[NUM_LOCATIONS - 1].X = START_AND_END_LOCATION;
		loc[NUM_LOCATIONS - 1].Y = START_AND_END_LOCATION;

		for (int i = 0; i < NUM_PLAYERS; i++) { // Initialize data for each player
			pd[i] = new PlayerData();  // Initialize each player data structure.

			for (int j = 0; j < NUM_LOCATIONS; j++) { // Initialize the locations this player goes to
				toolWritePlayerData(i, OFFSET_LOC_ARRAY + j * 8, loc[j].X);  // Set the same locations for each player
				toolWritePlayerData(i, OFFSET_LOC_ARRAY + j * 8 + 4, loc[j].Y);  // Set the same locations for each player
			}

			for (int j = 0; j < SIZE_OF_TASK; j++)// Initialize the task data for this player
				toolWritePlayerData(i, OFFSET_TASK_ARRAY + j * 4, (int) (randomStream.nextDouble() * Integer.MAX_VALUE));   // Set a random number for the task (sort them)
		}

	}

}
