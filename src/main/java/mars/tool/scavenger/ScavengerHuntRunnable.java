package mars.tool.scavenger;

import mars.Globals;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import static mars.tool.scavenger.Constants.*;


class ScavengerHuntRunnable implements Runnable {
	final ScavengerHunt hunt;
	final JPanel panel;

	public ScavengerHuntRunnable(ScavengerHunt scavengerHunt) {
		// Recommended by Pete Sanderson, 2 Nov. 2006, so that the Tool window and
		// MARS window can be on the screen at the same time.
		final JDialog frame = new JDialog(Globals.getGui(), "ScavengerHunt");
		hunt = scavengerHunt;

		panel = new JPanel(new BorderLayout());
		hunt.graphicArea = new ScavengerHuntDisplay(GRAPHIC_WIDTH, GRAPHIC_HEIGHT, hunt);
		final JPanel buttonPanel = new JPanel();
		final JButton resetButton = new JButton("Reset");
		resetButton.addActionListener(e -> {
			hunt.graphicArea.clear();

			// Reset actions here
			hunt.initializeScavengerData();

		});
		buttonPanel.add(resetButton);

		panel.add(hunt.graphicArea, BorderLayout.CENTER);
		panel.add(buttonPanel, BorderLayout.SOUTH);

		// Snippet by Pete Sanderson, 2 Nov. 2006, to be a window-closing sequence
		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(final WindowEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});

		frame.getContentPane().add(panel);
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
		frame.setTitle(" This is the ScavengerHunt");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // TBD --- This should close only the Tool, not the entire MARS
		frame.setPreferredSize(new Dimension(GRAPHIC_WIDTH, GRAPHIC_HEIGHT)); // TBD  SIZE
		frame.setVisible(true); // show();
	}

	@Override
	public void run() {
		// infinite loop: play the Scavenger Hunt game
		do {
			// Pause to slow down the redisplay of the game. This is separate from
			// the execution speed of the MIPS program, so the display may lag behind
			// the state of the MIPS program.
			try {
				Thread.sleep(100);   // millisec
			} catch (final InterruptedException ignored) { break; }

			panel.repaint(); // show new ScavengerHunt position
		} while (true);
	}
}
