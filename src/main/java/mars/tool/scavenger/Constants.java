package mars.tool.scavenger;

class Constants {
	public static final int GRAPHIC_WIDTH = 712;
	public static final int GRAPHIC_HEIGHT = 652;

	public static final int NUM_PLAYERS = 22;    // Number of players in the game, including "baseline" player
	public static final int MAX_X_MOVEMENT = 2;  // Max. movement in X direction
	public static final int MAX_Y_MOVEMENT = 2;  // Max. movement in X direction
	public static final double MAX_MOVE_DISTANCE = 2.5;  // Max. distance (Euclidean measure)
	public static final int ENERGY_AWARD = 20;    // Energy awarded for each task completion
	public static final int ENERGY_PER_MOVE = 1;  // Energy used in making each move (regardless of distance)
	public static final int SIZE_OF_TASK = 20;   // Number of elements in the task

	public static final int NUM_LOCATIONS = 7;  // Number of locations to which ScavengerHunt players travel.
	// The first (n-1) locations are "random" locations, the last is START_AND_END_LOCATION

	public static final int START_AND_END_LOCATION = 255; // Start and end location of the ScavengerHunt
	public static final int ADMINISTRATOR_ID = 999;  // Special ID for administrator

	// MIPS addresses of administrative memory space.
	// The administrator MIPS program writes a value to the Authentication field prior to writing a new
	// value to the PlayerID field. The value of the Authentication field is not itself verified until
	// the PlayerID field changes. Each new value of the Authentication field is one of some sequence
	// known to the Tool program (one-time pad system).
	// Each change of data in the PlayerID field is a signal for the Tool to check the value of the Authentication
	// field. If the value of the Authentication field is correct, the value of the PlayerID field is used
	// for memory bounds checking. A new value of the Authentication field is expected at each change of the
	// PlayerID field (one-time pad system).
	public static final int ADDR_AUTHENTICATION = 0xffffe000;  // MIPS byte address of Authentication field
	public static final int ADDR_PLAYER_ID = 0xffffe004;  // MIPS byte address of PlayerID field
	public static final int ADDR_GAME_ON = 0xffffe008;  // MIPS byte address of signal that administration has initialized data
	public static final int ADDR_NUM_TURNS = 0xffffe00c;  // MIPS byte address of number of turns remaining in the game

	// MIPS addresses of various data in each player's memory space.
	// Each player's assigned memory is the MEM_PER_PLAYER bytes which begin at
	// location ADDR_BASE + (ID *  MEM_PER_PLAYER)
	public static final int ADDR_BASE = 0xffff8000;  // MIPS byte address of memory space for first player
	public static final int MEM_PER_PLAYER = 0x400;  // MIPS bytes of memory space given to each player
	public static final int OFFSET_WHERE_AM_I_X = 0x0;  // MIPS byte offset to this field
	public static final int OFFSET_WHERE_AM_I_Y = 0x4;  // MIPS byte offset to this field
	public static final int OFFSET_MOVE_TO_X = 0x8;  // MIPS byte offset to this field
	public static final int OFFSET_MOVE_TO_Y = 0xc;  // MIPS byte offset to this field
	public static final int OFFSET_MOVE_READY = 0x10;  // MIPS byte offset to this field
	public static final int OFFSET_ENERGY = 0x14;  // MIPS byte offset to this field
	public static final int OFFSET_NUMBER_LOCATIONS = 0x18;  // MIPS byte offset to this field
	public static final int OFFSET_PLAYER_COLOR = 0x1c;  // MIPS byte offset to this field
	public static final int OFFSET_SIZE_OF_TASK = 0x20;  // MIPS byte offset to this field
	public static final int OFFSET_LOC_ARRAY = 0x24;  // MIPS byte offset to this field
	public static final int OFFSET_TASK_COMPLETE = 0x124;  // MIPS byte offset to this field
	public static final int OFFSET_TASK_ARRAY = 0x128;  // MIPS byte offset to this field
	// Other MIPS memory locations are available to the player's use.
}
