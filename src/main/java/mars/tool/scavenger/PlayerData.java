package mars.tool.scavenger;

import static mars.tool.scavenger.Constants.*;


/**
 * 	private inner class to provide the data on each player needed for display
 */
class PlayerData {
	int whereAmIX = START_AND_END_LOCATION;   // Read only. Memory Address:  Base
	int whereAmIY = START_AND_END_LOCATION;   // Read only. Memory Address:  Base + 0x4
	//int moveToX;    //  Memory Address:  Base + 0x8
	//int moveToY;    //  Memory Address:  Base + 0xc
	//int goalX;     // Read only. Memory Address:  Base + 0x10
	//int goalY;     // Read only. Memory Address:  Base + 0x14
	int energy = 20;    // Read only. Memory Address:  Base + 0x18
	int color = 0;    // Memory Address:  Base + 0x1c
	long finishTime;
	//int locID;  // ID of the location to which ScavengerHunt players are headed. Not used by player.
	boolean[] hasVisitedLoc = new boolean[NUM_LOCATIONS];  // boolean: player has visited each location
	boolean finis = false;

	// Class PlayerData has no constructor

	public void setWhereAmI(final int gX, final int gY) {
		whereAmIX = gX;
		whereAmIY = gY;
	}

	//public void setGoal(int gX, int gY) {  goalX = gX; goalY = gY; }
	public void setEnergy(final int e) { energy = e; }

	public void setColor(final int c) { color = c; }

	public int getWhereAmIX() { return whereAmIX; }

	public int getWhereAmIY() { return whereAmIY; }

	public int getColor() { return color; }

	public boolean hasVisited(final int i) {
		return hasVisitedLoc[i];
	}

	public void setVisited(final int i) { hasVisitedLoc[i] = true; }

	public void setFinished() {
		finis = true;
	}

	public boolean isFinished() { return finis; }

	public long getFinishTime() { return finishTime; }

	public long getFinishMin() { return finishTime / 60000; }   // Minutes portion of finishTime

	public long getFinishSec() {
		return finishTime % 60000 / 1000;  // Seconds portion of finishTime
	}  // Seconds portion of finishTime

	public long getFinishMillisec() { return finishTime % 1000; }  // Millisec portion of finishTime

	public void setFinishTime(final long t) { finishTime = t; }

	//public int getGoalX() {  return goalX; }
	//public int getGoalY() {  return goalY; }
	//public int getMoveToX() {  return moveToX; }
	//public int getMoveToY() {  return moveToY; }
	public int getEnergy() { return energy; }
	//public int getLocationID() {  return locID; }
}
