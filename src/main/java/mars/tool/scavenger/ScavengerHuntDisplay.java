package mars.tool.scavenger;

import javax.swing.*;
import java.awt.*;

import static mars.tool.scavenger.Constants.*;
import static mars.tool.scavenger.ScavengerHunt.loc;
import static mars.tool.scavenger.ScavengerHunt.pd;

/**
 * ScavengerHuntDisplay does not have access to the same MIPS Memory class
 * object used by the ScavengerHunt class object. Need read-only access to a
 * similar data structure maintained within ScavengerHunt.
 */
class ScavengerHuntDisplay extends JPanel {
	private final int width;
	private final int height;
	private final ScavengerHunt hunt;

	public ScavengerHuntDisplay(final int tw, final int th, ScavengerHunt hunt) {
		this.hunt = hunt;
		width = tw;
		height = th;
	}

	public void redraw() {
		repaint();
	}

	public void clear() {
		repaint();
	}

	/**
	 * paintComponent does not have access to the same MIPS Memory class object used
	 * by the ScavengerHunt class object. Need read-only access to a similar data
	 * structure maintained within ScavengerHunt.
	 */
	@Override
	public void paintComponent(final Graphics g) {
		int xCoord;
		int yCoord;

		// Recover Graphics2D
		final Graphics2D g2 = (Graphics2D) g;

		if (!hunt.GameOn) {  // Make sure game is ready before continuing
			g2.setColor(Color.lightGray);
			g2.fillRect(0, 0, width - 1, height - 1); // Clear all previous drawn information
			g2.setColor(Color.black);
			g2.drawString(" ScavengerHunt not yet initialized by MIPS administrator program.", 100, 200);
			return;
		}

		// Clear all previous drawn information
		g2.setColor(Color.lightGray);
		g2.fillRect(0, 0, width - 1, height - 1);

		// Draw the locations to which the players will be moving
		// All players have the same location data.
		for (int i = 0; i < NUM_LOCATIONS; i++) {
			xCoord = loc[i].X;
			yCoord = loc[i].Y;
			g2.setColor(Color.blue);
			g2.fillRect(xCoord, yCoord, 20, 20);  // coord is upper left corner of oval
			g2.setColor(Color.white);
			g2.drawString(" " + i, xCoord + 4, yCoord + 15);  // coord is lower left corner of string text box
		}

		// Draw scoreboard
		g2.setColor(Color.black);
		g2.drawString("Player", width - 160, 30);
		g2.drawString("Locations", width - 110, 30);
		g2.drawString("Energy", width - 50, 30);
		g2.drawLine(width - 160, 35, width - 10, 35); // line under column headings
		g2.drawLine(width - 120, 35, width - 120, 35 + NUM_PLAYERS * 15); // vertical line for location-visited marks
		g2.drawLine(width - 50, 35, width - 50, 35 + NUM_PLAYERS * 15); // vertical line for location-visited marks
		for ( int i = 0; i < NUM_PLAYERS; i++ ) {
			// Draw player's symbol
			g2.setColor( new Color( pd[i].getColor() ) );

			xCoord = pd[i].getWhereAmIX();
			yCoord = pd[i].getWhereAmIY();

			// Draw player symbol and label it with player ID number.
			// Hardcoded size of location graphic and label.
			g2.drawOval(xCoord, yCoord, 20, 20);  // coord is upper left corner of oval
			g2.drawString(" " + i, xCoord + 4, yCoord + 15);  // coord is lower left corner of string text box

			// Draw player's info on scoreboard
			g2.setColor(Color.black);
			g2.drawString(" " + i, width - 150, 50 + i * 15);  // Player's ID on scoreboard
			g2.drawString(" " + pd[i].getEnergy(), width - 40, 50 + i * 15);  // Player's energy on scoreboard

			// Display player's progress or finishing time, whichever is applicable
			if ( pd[i].isFinished() ) {
				// Display finishing time
				// This doesn't display leading zeroes to align time components (e.g. 27 millisec ought to print as 027)
				g2.drawString(
					pd[i].getFinishMin() + ":" +  // Minutes
					pd[i].getFinishSec() + ":" +   // Seconds
					pd[i].getFinishMillisec(),   // Milliseconds
					width - 115, 50 + i * 15
				);
			} else { // player either has not finished or is just now finishing
				int visCount = 0;
				for (int j = 0; j < NUM_LOCATIONS; j++) {
					if ( pd[i].hasVisited(j) )  // count number of locations that player has visited
						visCount++;
				}
				if (visCount == NUM_LOCATIONS) { // player has visited every location -- finished!
					pd[i].setFinished();
					pd[i].setFinishTime(System.currentTimeMillis() - hunt.startTime);

				} else { // player has not yet visited every location
					// Display locations that the player has actually visited
					for ( int j = 0; j < NUM_LOCATIONS; j++ )
						if ( pd[i].hasVisited(j) )  // Player has visited this location
							g2.fillRect(width - 120 + j * 10, 42 + i * 15, 10, 8);
				}
			} // end player had not previously finished
		}
	}
}
