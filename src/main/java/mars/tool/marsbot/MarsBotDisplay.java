package mars.tool.marsbot;

import javax.swing.*;
import java.awt.*;

class MarsBotDisplay extends JPanel {
	private final MarsBot bot;

	public MarsBotDisplay(MarsBot bot, int width, int height) {
		setSize( width, height );
		this.bot = bot;
	}

	public void redraw() {
		repaint();
	}

	@Override
	public void paintComponent(final Graphics g) {
		// Recover Graphics2D
		final Graphics2D g2 = (Graphics2D) g;

		// Draw the track left behind, for each segment of the path
		g2.setColor(Color.blue);
		for (int i = 1; i <= bot.trackIndex; i += 2) { // Index grows by two (begin-end pair)
			try {
				g2.drawLine(
					(int) bot.tracks[i - 1].getX(),
					(int) bot.tracks[i - 1].getY(),
					(int) bot.tracks[i].getX(),
					(int) bot.tracks[i].getY()
				);
			} catch ( final ArrayIndexOutOfBoundsException | NullPointerException ignored ) { }
		}

		g2.setColor(Color.black);
		g2.fillRect( (int) bot.x, (int) bot.y, 20, 20 ); // Draw bot at its current position
	}

}