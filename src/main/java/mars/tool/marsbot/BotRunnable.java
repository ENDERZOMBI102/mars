package mars.tool.marsbot;

import mars.Globals;
import mars.mips.hardware.AddressErrorException;

import javax.swing.*;
import java.awt.*;

import static mars.tool.marsbot.Constants.*;

class BotRunnable implements Runnable {
	private final MarsBotDisplay graphicArea;
	private final MarsBot bot;
	private final JPanel panel;

	public BotRunnable(MarsBot marsBot) {
		bot = marsBot;
		final JFrame frame = new JFrame("Bot");
		panel = new JPanel(new BorderLayout());
		graphicArea = new MarsBotDisplay(bot, GRAPHIC_WIDTH, GRAPHIC_HEIGHT);
		final JPanel buttonPanel = new JPanel();
		final JButton clearButton = new JButton("Clear");
		clearButton.addActionListener(e -> {
			graphicArea.redraw();
			bot.leaveTrack = false; // true --> leave track when moving, false --> do not ...
			bot.x = 0; // X pixel position of MarsBot
			bot.y = 0; // Y pixel position of MarsBot
			bot.moving = false; // true --> MarsBot is moving, false --> MarsBot not moving
			bot.trackIndex = 0;
		});
		buttonPanel.add(clearButton);
		final JButton closeButton = new JButton("Close");
		closeButton.addActionListener(e -> frame.setVisible(false));
		buttonPanel.add(closeButton);
		panel.add(graphicArea, BorderLayout.CENTER);
		panel.add(buttonPanel, BorderLayout.SOUTH);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setTitle(" This is the MarsBot");
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE); // changed 12/12/09 DPS (was EXIT)
		frame.setSize(GRAPHIC_WIDTH + 200, GRAPHIC_HEIGHT + 100); // TBD  SIZE
		frame.setVisible(true);
	}

	@Override
	public void run() {

		double tempAngle;
		// infinite loop: move the bot according to the current directives
		// (which may be to NOT move)
		do {
			if ( bot.moving ) {
				//System.out.println("BotRunnable.run: bot IS moving.");
				// TBD This is an arbitrary distance for bot movement. This could just
				// as easily be a random distance to simulate terrain, etc.
				// adjust bot position.
				// The "mathematical angle" is zero at east, 90 at north, etc.
				// The "heading" is 0 at north, 90 at east, etc.
				// Conversion: MathAngle = [(360 - heading) + 90] mod 360
				tempAngle = (360 - bot.heading + 90) % 360;
				bot.x += Math.cos(Math.toRadians(tempAngle)); // Math.cos parameter unit is radians
				bot.y += -Math.sin(Math.toRadians(tempAngle)); // Negate value because Y coord grows down

				// Write this new information to MARS memory area
				try {
					Globals.memory.setWord(ADDR_WHEREAREWEX, (int) bot.x);
					Globals.memory.setWord(ADDR_WHEREAREWEY, (int) bot.y);
				} catch (final AddressErrorException ignored) { }

				// Whether or not we're leaving a track, write the current point to the
				// current position in the array.
				//   -- If we are not leaving a track now, we will need the current point to
				//      start a future track, and that goes into the array.
				//   -- If we are leaving a track now, the current point may end the track,
				//      and that goes into the array.
				bot.tracks[bot.trackIndex] = new Point( (int) bot.x, (int) bot.y );
			}

			// TBD Pause whether the bot is or is not moving. This gives the MIPS program
			// opportunity to consider results of movement, or to make the bot move.
			// ??? What is relationship of robot speed to MARS's
			// execution time for a single instruction? Does the robot speed have to
			// be slow enough to allow a MARS busy loop to detect the bot position
			// at a specific pixel?
			try {
				//noinspection BusyWait
				Thread.sleep(40);
			} catch (final InterruptedException ignored) { }

			panel.repaint(); // show new bot position
		} while (true);
	}
}
