package mars.tool.marsbot;

class Constants {
	public static final int GRAPHIC_WIDTH = 512;
	public static final int GRAPHIC_HEIGHT = 512;
	public static final int ADDR_HEADING = 0xffff8010;
	public static final int ADDR_LEAVETRACK = 0xffff8020;
	public static final int ADDR_WHEREAREWEX = 0xffff8030;
	public static final int ADDR_WHEREAREWEY = 0xffff8040;
	public static final int ADDR_MOVE = 0xffff8050;
	public static final int TRACK_PTS = 256;
}
