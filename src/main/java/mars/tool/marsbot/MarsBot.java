package mars.tool.marsbot;

import mars.Globals;
import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.AddressErrorException;
import mars.mips.hardware.Memory;
import mars.mips.hardware.MemoryAccessNotice;
import mars.tool.MarsTool;

import java.awt.*;

import static mars.tool.marsbot.Constants.*;

/**
 * Simple Demo of Mars tool capability
 */
public class MarsBot implements MarsTool {
	boolean leaveTrack = false; // true --> leave track when moving, false --> do not ...
	int heading = 0; // 0 --> North (up), 90 --> East (right), etc.
	double y = 0; // Y pixel position of MarsBot
	double x = 0; // X pixel position of MarsBot
	boolean moving = false; // true --> MarsBot is moving, false --> MarsBot not moving

	// The begin and end points of a "track" segment are kept in neighboring pairs
	// of elements of the array. arrayOfTrack[i] is the start pt, arrayOfTrack[i+1] is
	// the end point of a path that should leave a track.
	final Point[] tracks = new Point[TRACK_PTS];
	int trackIndex = 0;

	@Override
	public String getName() {
		return "Mars Bot";
	}

	/*
	 * This will set up the Bot's GUI.  Invoked when Bot menu item selected.
	 */
	@Override
	public void action() {
		final BotRunnable br1 = new BotRunnable(this);
		final Thread t1 = new Thread(br1);
		t1.start();
		// New: DPS 27 Feb 2006.  Register observer for memory subrange.
		try {
			Globals.memory.addObserver( this::onMemoryChanged, 0xffff8000, 0xffff8060);
		} catch (final AddressErrorException exc) {
			exc.printStackTrace();
		}
	}

	/*
	 * This method observes MIPS program directives to modify Bot activity (that is,
	 * MIPS program write to MMIO) and updates instance variables to reflect that
	 * directive.
	 */
	public void onMemoryChanged( Memory memory, Object arg) {
		MemoryAccessNotice notice = (MemoryAccessNotice) arg;
		int address = notice.getAddress();
		if (address < 0 && notice.getAccessType() == AccessNotice.WRITE) {
			if (address == ADDR_HEADING) {
				heading = notice.getValue();
			} else if (address == ADDR_LEAVETRACK) {
				// If we HAD NOT been leaving a track, but we should NOW leave
				// a track, put start point into array.
				if ( !leaveTrack && notice.getValue() == 1 ) {
					leaveTrack = true;
					tracks[trackIndex] = new Point((int) x, (int) y);
					trackIndex++;  // the index of the end point
				}
				// If we HAD NOT been leaving a track, and get another directive
				// to NOT leave a track, do nothing (nothing to do).
				else if ( !leaveTrack && notice.getValue() == 0 ) { }
				// If we HAD been leaving a track, and get another directive
				// to LEAVE a track, do nothing (nothing to do).
				else if ( leaveTrack && notice.getValue() == 1 ) { }
				// If we HAD been leaving a track, and get another directive
				// to NOT leave a track, put end point into array.
				else if ( leaveTrack && notice.getValue() == 0 ) {
					leaveTrack = false;
					tracks[trackIndex] = new Point((int) x, (int) y);
					trackIndex++;  // the index of the next start point
				}
			} else if (address == ADDR_MOVE) {
				moving = notice.getValue() != 0;
			}
		}
	}
}
