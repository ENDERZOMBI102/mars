package mars.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Properties;

public final class Utility {

	/**
	 * Read and return property file value (if any) for requested property.
	 *
	 * @param propertiesFile name of properties file (do NOT include filename
	 *                       extension, which is assumed to be ".properties")
	 * @param propertyName   String containing desired property name
	 * @return String containing associated value; null if property not found
	 */
	public static String getPropertyEntry(final String propertiesFile, final String propertyName) {
		return getPropertyEntry( propertiesFile, propertyName, true );
	}

	/**
	 * Read and return property file value (if any) for requested property.
	 *
	 * @param propertiesFile name of properties file (do NOT include filename
	 *                       extension, which is assumed to be ".properties")
	 * @param propertyName   String containing desired property name
	 * @return String containing associated value; null if property not found
	 */
	public static String getPropertyEntry( final String propertiesFile, final String propertyName, boolean fromClasspath ) {
		if ( fromClasspath ) {
			var props = new Properties();
			try {
				props.load( Utility.class.getResourceAsStream( propertiesFile ) );
			} catch (IOException e) {
				e.printStackTrace();
			}
			return props.getProperty( propertyName );
		} else
			return loadPropertiesFromFile(propertiesFile).getProperty(propertyName);
	}

	/**
	 * Produce Properties (a Hashtable) object containing key-value pairs from
	 * specified properties file. This may be used as an alternative to
	 * readPropertiesFile() which uses a different implementation.
	 *
	 * @param file Properties filename. Do NOT include the file extension as it is
	 *             assumed to be ".properties" and is added here.
	 * @return Properties (Hashtable) of key-value pairs read from the file.
	 */
	public static Properties loadPropertiesFromFile(final String file) {
		try ( var stream = Files.newInputStream( Path.of(file) ) ) {
			return loadPropertiesFromFile( stream );
		} catch (IOException e) {
			return new Properties();
		}
	}

	public static Properties loadPropertiesFromFile( final InputStream stream ) {
		final Properties properties = new Properties();
		try {
			properties.load( stream );
		} catch (final IOException | NullPointerException ignored) { } // If it doesn't work, properties will be empty
		return properties;
	}
}
