package mars.util;

import java.awt.*;
import java.util.ArrayList;

public class Vertex {
	private int numIndex;
	private int init;
	private int end;
	private int current;
	private String name;
	public static final int MOVING_UPSIDE = 1;
	public static final int MOVING_DOWNSIDE = 2;
	public static final int MOVING_LEFT = 3;
	public static final int MOVING_RIGHT = 4;
	public int direction;
	public int oppositeAxis;
	private boolean isMovingXaxis;
	private Color color;
	private boolean first_interaction;
	private boolean active;
	private final boolean isText;
	private final ArrayList<Integer> targetVertex;

	public Vertex(final int index, final int init, final int end, final String name, final int oppositeAxis, final boolean isMovingXaxis, final String listOfColors, final String listTargetVertex, final boolean isText) {
		numIndex = index;
		this.init = init;
		current = this.init;
		this.end = end;
		this.name = name;
		this.oppositeAxis = oppositeAxis;
		this.isMovingXaxis = isMovingXaxis;
		first_interaction = true;
		active = false;
		this.isText = isText;
		color = new Color(0, 153, 0);
		if (isMovingXaxis) {
			if (init < end) {
				direction = MOVING_LEFT;
			} else {
				direction = MOVING_RIGHT;
			}

		} else {
			if (init < end) {
				direction = MOVING_UPSIDE;
			} else {
				direction = MOVING_DOWNSIDE;
			}
		}
		final String[] list = listTargetVertex.split("#");
		targetVertex = new ArrayList<>();
		for ( String s : list ) {
			targetVertex.add( Integer.parseInt(s) );
		}
		final String[] listColor = listOfColors.split("#");
		color = new Color(
				Integer.parseInt(listColor[0]),
				Integer.parseInt(listColor[1]),
				Integer.parseInt(listColor[2])
		);
	}

	public int getDirection() {
		return direction;
	}

	public boolean isText() {
		return isText;
	}

	public ArrayList<Integer> getTargetVertex() {
		return targetVertex;
	}

	public int getNumIndex() {
		return numIndex;
	}

	public void setNumIndex(final int numIndex) {
		this.numIndex = numIndex;
	}

	public int getInit() {
		return init;
	}

	public void setInit(final int init) {
		this.init = init;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(final int end) {
		this.end = end;
	}

	public int getCurrent() {
		return current;
	}

	public void setCurrent(final int current) {
		this.current = current;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getOppositeAxis() {
		return oppositeAxis;
	}

	public void setOppositeAxis(final int oppositeAxis) {
		this.oppositeAxis = oppositeAxis;
	}

	public boolean isMovingXaxis() {
		return isMovingXaxis;
	}

	public void setMovingXaxis(final boolean isMovingXaxis) {
		this.isMovingXaxis = isMovingXaxis;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(final Color color) {
		this.color = color;
	}

	public boolean isFirst_interaction() {
		return first_interaction;
	}

	public void setFirst_interaction(final boolean first_interaction) {
		this.first_interaction = first_interaction;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(final boolean active) {
		this.active = active;
	}
}
