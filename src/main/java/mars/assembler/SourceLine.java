package mars.assembler;

import mars.MIPSprogram;

/**
 * Handy class to represent, for a given line of source code, the code itself,
 * the program containing it, and its line number within that program. This is
 * used to separately keep track of the original file/position of a given line
 * of code. When .include is used, it will migrate to a different line and
 * possibly different program but the migration should not be visible to the
 * user.
 */
public record SourceLine(
		String source,
		String filename,
		MIPSprogram mipsProgram,
		int lineNumber
) {
	/**
	 * SourceLine constructor
	 *
	 * @param source      The source code itself
	 * @param mipsProgram The program (object representing source file) containing
	 *                    that line
	 * @param lineNumber  The line number within that program where source appears.
	 */
	public SourceLine(final String source, final MIPSprogram mipsProgram, final int lineNumber) {
		this( source, mipsProgram != null ? mipsProgram.getFilename() : null, mipsProgram, lineNumber );
	}
}
