package mars.mips.instructions;

import mars.mips.instructions.syscalls.Syscall;
import mars.util.FilenameFinder;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/*
 * Copyright (c) 2003-2006, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/****************************************************************************/
/* This class provides functionality to bring external Syscall definitions
 * into MARS.  This permits anyone with knowledge of the mars.Mars public interfaces,
 * in particular of the Memory and Register classes, to write custom MIPS syscall
 * functions. This is adapted from the ToolLoader class, which is in turn adapted
 * from Bret Barker's GameServer class from the book "Developing Games In Java".
 */

class SyscallLoader {

	private static final String CLASS_PREFIX = "mars.mips.instructions.syscalls.";
	private static final String SYSCALLS_DIRECTORY_PATH = "mars/mips/instructions/syscalls";
	private static final String SYSCALL_INTERFACE = "Syscall.class";
	private static final String SYSCALL_ABSTRACT = "AbstractSyscall.class";
	private static final String CLASS_EXTENSION = "class";

	private List<Syscall> syscallList;

	/*
	  *  Dynamically loads Syscalls into an ArrayList.  This method is adapted from
	  *  the loadGameControllers() method in Bret Barker's GameServer class.
	  *  Barker (bret@hypefiend.com) is co-author of the book "Developing Games
	  *  in Java".  Also see the "loadMarsTools()" method from ToolLoader class.
	  */
	void loadSyscalls() {
		syscallList = new ArrayList<>();
		// grab all class files in the same directory as Syscall
		final List<String> candidates = FilenameFinder.getFilenameList(this.getClass().getClassLoader(), SYSCALLS_DIRECTORY_PATH, CLASS_EXTENSION);
		final Map<String, String> syscalls = new HashMap<>();
		for (String candidate : candidates) {
			// Do not add class if already encountered (happens if run in MARS development directory)
			if (syscalls.containsKey(candidate)) {
				continue;
			} else {
				syscalls.put(candidate, candidate);
			}
			if (!candidate.equals(SYSCALL_INTERFACE) && !candidate.equals(SYSCALL_ABSTRACT)) {
				try {
					// grab the class, make sure it implements Syscall, instantiate, add to list
					final String syscallClassName = CLASS_PREFIX + candidate.substring(0, candidate.indexOf(CLASS_EXTENSION) - 1);
					final Class<?> clazz = Class.forName(syscallClassName);
					if (!Syscall.class.isAssignableFrom(clazz)) {
						continue;
					}
					// TODO: Move to static instancing
					final Syscall syscall = (Syscall) clazz.getConstructor().newInstance();
					if (findSyscall(syscall.getNumber()) == null) {
						syscallList.add(syscall);
					} else {
						throw new Exception(
								"Duplicate service number: " +
								syscall.getNumber() +
								" already registered to " +
								Objects.requireNonNull( findSyscall( syscall.getNumber() ) ).getName()
						);
					}
				} catch (final Exception e) {
					Const.LOGGER.error("Error instantiating Syscall from file " + candidate + ": " + e);
					System.exit(0);
				}
			}
		}
	}

	/*
	 * Method to find Syscall object associated with given service number.
	 * Returns null if no associated object found.
	 */
	@Nullable Syscall findSyscall(final int number) {
		// linear search is OK since number of syscalls is small.
		if (syscallList == null)
			loadSyscalls();
		for (Syscall service : syscallList) {
			if (service.getNumber() == number) {
				return service;
			}
		}
		return null;
	}
}
