package mars.mips.instructions;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class Const {
	static final Logger LOGGER = LoggerFactory.getLogger("Instructions");
}
