package mars;

import java.util.List;

/**
 * @author ENDERZOMBI102
 * Constants for the lifetime of the application
 */
public class Const {
	public static final String VERSION = "4.6.2 Unofficial";

	public static final String OG_COPYRIGHT_YEARS = "2003-2014";
	public static final String OG_COPYRIGHT_HOLDERS = "Pete Sanderson and Kenneth Vollmar";


	/**
	 * List of accepted file extensions for MIPS assembly source files.
	 */
	public static final List<String> FILE_EXTENSIONS = List.of("mips", "asm");
}
