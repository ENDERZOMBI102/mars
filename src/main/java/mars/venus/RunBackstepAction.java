package mars.venus;

import mars.Globals;
import mars.mips.hardware.Coprocessor0;
import mars.mips.hardware.Coprocessor1;
import mars.mips.hardware.Memory;
import mars.mips.hardware.RegisterFile;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

/*
 * Copyright (c) 2003-2009, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * Action for the Run -> Backstep menu item
 */
public class RunBackstepAction extends Action {
	String name;
	ExecutePane executePane;

	public RunBackstepAction( final VenusUI gui ) {
		super(
			"backstep",
			"StepBack22.png",
			KeyEvent.VK_B,
			KeyStroke.getKeyStroke(KeyEvent.VK_F8, 0),
			gui
		);
	}

	/**
	 * perform next simulated instruction step.
	 */
	@Override
	public void actionPerformed(final ActionEvent evt) {
		name = getValue( javax.swing.Action.NAME).toString();
		executePane = mainUI.getMainPane().getExecutePane();
		if (!FileStatus.isAssembled()) {
			// note: this should never occur since backstepping is only enabled after successful assembly.
			JOptionPane.showMessageDialog(mainUI, "The program must be assembled before it can be run.");
			return;
		}
		VenusUI.setStarted(true);
		mainUI.messagesPane.setSelectedComponent(mainUI.messagesPane.runTab);
		executePane.getTextSegmentWindow().setCodeHighlighting(true);

		if (Globals.getSettings().getBackSteppingEnabled()) {
			final boolean inDelaySlot = Globals.program.getBackStepper().inDelaySlot(); // Added 25 June 2007
			Memory.getInstance().addObservable(executePane.getDataSegmentWindow()::onMemoryEvent);
			RegisterFile.addRegistersListener(executePane.getRegistersWindow()::onRegisterEvent);
			Coprocessor0.addRegistersListener(executePane.getCoprocessor0Window()::onRegisterEvent);
			Coprocessor1.addRegistersListener(executePane.getCoprocessor1Window()::onRegisterEvent);
			Globals.program.getBackStepper().backStep();
			Memory.getInstance().removeObserver(executePane.getDataSegmentWindow()::onMemoryEvent);
			RegisterFile.deleteRegistersListener(executePane.getRegistersWindow()::onRegisterEvent);
			executePane.getRegistersWindow().updateRegisters();
			executePane.getCoprocessor1Window().updateRegisters();
			executePane.getCoprocessor0Window().updateRegisters();
			executePane.getDataSegmentWindow().updateValues();
			executePane.getTextSegmentWindow().highlightStepAtPC(inDelaySlot); // Argument aded 25 June 2007
			FileStatus.set(FileStatus.RUNNABLE);
			VenusUI.setReset(false);
		}
	}
}
