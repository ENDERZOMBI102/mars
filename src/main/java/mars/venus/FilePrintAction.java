package mars.venus;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;

/*
 * Copyright (c) 2003-2006, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * Action for the File -> Print menu item
 */
public class FilePrintAction extends Action {

	public FilePrintAction( final VenusUI gui ) {
		super( "print", "Print22.gif", KeyEvent.VK_P, null, gui );
	}

	/**
	 * Uses the HardcopyWriter class developed by David Flanagan for the book "Java
	 * Examples in a Nutshell". It will do basic printing of multipage text
	 * documents. It displays a print dialog but does not act on any changes the
	 * user may have specified there, such as number of copies.
	 *
	 * @param evt component triggering this call
	 */
	@Override
	public void actionPerformed(final ActionEvent evt) {
		final EditPane editPane = mainUI.getMainPane().getEditPane();
		// if we have no files open, abort
		if ( editPane == null )
			return;

		try (
			var out = new HardcopyWriter(
				mainUI,
				editPane.getFilename(),
				10,
				.5,
				.5,
				.5,
				.5
			)
		) {
			// source code
			final String[] source = editPane.getSource().split("\n");
			// are we showing the line numbers?
			final boolean showLineNumbers = editPane.showingLineNumbers();
			// max line number length
			final int lineNumberDigits = Integer.toString( source.length ).length();
			for ( int lineN = 0; lineN < source.length; lineN++ ) {
				// if showing line numbers, prepend them
				if ( showLineNumbers ) {
					var lineNumber = lineN + ": ";
					source[lineN] = lineNumber + " ".repeat(lineNumberDigits - lineNumber.length()) + source[lineN];
				}
				// write
				out.write(source[lineN]);
			}
		} catch (final HardcopyWriter.PrintCanceledException | IOException ignored) { }
	}
}
