package mars.venus;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;

import mars.tool.*;
import mars.tool.bht.BHTSimulator;
import mars.tool.magnifier.ScreenMagnifier;
import mars.tool.marsbot.MarsBot;
import mars.tool.scavenger.ScavengerHunt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.enderzombi102.mars.res.Localization.loc;

/*
 * Copyright (c) 2003-2006, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * This class provides functionality to bring external mars.Mars tools into the mars.Mars
 * system by adding them to its Tools menu. This permits anyone with knowledge
 * of the mars.Mars public interfaces, in particular of the Memory and Register
 * classes, to write applications which can interact with a MIPS program
 * executing under mars.Mars. The execution is of course simulated. The private
 * method for loading tool classes is adapted from Bret Barker's GameServer
 * class from the book "Developing Games In Java".
 *
 * @author Pete Sanderson with help from Bret Barker
 * @version August 2005
 */

public class ToolLoader {
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolLoader.class);
	private static final List< Class<? extends MarsTool> > TOOL_CLASSES = List.of(
		BHTSimulator.class,
		BitmapDisplay.class,
		CacheSimulator.class,
		DigitalLabSim.class,
		FloatRepresentation.class,
		InstructionCounter.class,
		InstructionStatistics.class,
		IntroToTools.class,
		KeyboardAndDisplaySimulator.class,
		MarsBot.class,
		MemoryReferenceVisualization.class,
		MipsXray.class,
		ScavengerHunt.class,
		ScreenMagnifier.class
	);

	/**
	 * Called in VenusUI to build its Tools menu.
	 *
	 * @return a Tools JMenu.
	 */
	public JMenu buildToolsMenu() {
		var menu = new JMenu( loc( "mars.menu.tools.name" ) );
		menu.setMnemonic( KeyEvent.VK_T );
		// traverse array list and build menu
		for ( MarsTool tool : loadMarsTools() ) {
			menu.add( new ToolAction( tool.getClass(), tool.getName() ) );
		}
		return menu;
	}

	/*
	*  Dynamically loads MarsTools into an ArrayList.
	*/
	private ArrayList<MarsTool> loadMarsTools() {
		final ArrayList<MarsTool> toolList = new ArrayList<>();
		for ( final Class<? extends MarsTool> clazz : TOOL_CLASSES ) {
			try {
				toolList.add( clazz.getDeclaredConstructor().newInstance() );
			} catch (final Exception exc) {
				LOGGER.error( "Error instantiating MarsTool from class {}: {}", clazz.getName(), exc );
			}
		}
		return toolList;
	}
}
