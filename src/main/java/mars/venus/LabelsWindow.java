package mars.venus;

import com.enderzombi102.mars.event.ClickEventListener;
import mars.Globals;
import mars.assembler.Symbol;
import mars.mips.hardware.AccessNotice;
import mars.mips.hardware.AddressErrorException;
import mars.mips.hardware.Memory;
import mars.mips.hardware.MemoryAccessNotice;
import mars.util.Binary;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.util.Comparator;
import java.util.List;


/**
 * Represents the Labels window, which is a type of JInternalFrame.
 * With it the user can view MIPS program labels.
 *
 * @author ENDERZOMBI102
 **/
public class LabelsWindow extends JInternalFrame {
	private final AbstractTableModel model;
	private final JCheckBox dataLabels;
	private final JTable table;
	private final JCheckBox textLabels;
	private Sorting sorting = Sorting.valueOf( Globals.getSettings().getStringSetting("LabelSortState") );

	public LabelsWindow() {
		super( "Labels", true, false, true, true );
		this.table = new JTable( model = new SymbolTableModel() );
		this.table.addMouseListener( new ClickEventListener( evt -> {
			// do not populate when there's no symbols
			if ( getSymbols().isEmpty() ) {
				return;
			}
			var address = Binary.stringToInt(
				(String) table.getValueAt( table.rowAtPoint( evt.getPoint() ), 1 )
			);
			// if we clicked an address, go to it
			if ( Memory.inTextSegment(address) || Memory.inKernelTextSegment(address) ) {
				Globals.getGui().getMainPane().getExecutePane().getTextSegmentWindow().selectStepAtAddress(address);
			}
			else {
				Globals.getGui().getMainPane().getExecutePane().getDataSegmentWindow().selectCellForAddress(address);
			}
		}));
		// disable reordering
		this.table.getTableHeader().setReorderingAllowed(false);
		// set header listener to change row sorting
		this.table.getTableHeader().addMouseListener( new ClickEventListener( evt -> {
			// do not sort when there's no symbols
			if ( getSymbols().isEmpty() )
				return;
			// sorting
			var column = table.getColumnName( table.columnAtPoint( evt.getPoint() ) );
			if ( column.startsWith("Address") ) {
				// if its already by address reverse it
				if ( this.sorting.isByAddress() )
					this.sorting = this.sorting.reverse();
				else
					this.sorting = Sorting.BY_ADDRESS;
			} else if ( column.startsWith("Label") ) {
				// if its already by label reverse it
				if ( this.sorting.isByLabel() )
					this.sorting = this.sorting.reverse();
				else
					this.sorting = Sorting.BY_NAME;
			}
			// save sorting to config
			Globals.getSettings().setStringSetting( "LabelSortState", this.sorting.name() );
			// update column names
			this.table.setModel( new SymbolTableModel() );
			this.table.getTableHeader().repaint();
		}));
		this.dataLabels = new JCheckBox( "Data", true );
		this.textLabels = new JCheckBox( "Text", false );
		this.dataLabels.addItemListener( evt -> model.fireTableDataChanged() );
		this.textLabels.addItemListener( evt -> model.fireTableDataChanged() );
		this.dataLabels.setToolTipText( "If checked, will display labels defined in data segment" );
		this.textLabels.setToolTipText( "If checked, will display labels defined in text segment" );
		this.getContentPane().add( new JScrollPane( this.table ), BorderLayout.CENTER );
		this.getContentPane().add( new JPanel() {{ add( dataLabels ); add( textLabels ); }}, BorderLayout.SOUTH );

		Memory.getInstance().addObservable( ( memory, arg) -> {
			// update table only on writes
			if ( ( (MemoryAccessNotice) arg ).getAccessType() == AccessNotice.WRITE )
				model.fireTableDataChanged();
		});
	}

	private List<Symbol> getSymbols() {
		if ( Globals.program != null && Globals.program.getLocalSymbolTable() != null )
			return Globals.program.getLocalSymbolTable()
				.getAllSymbols()
				.stream()
				.filter( element ->
					( element.getType().isData() && this.dataLabels.isSelected() ) ||
					( element.getType().isText() && this.textLabels.isSelected() )
				)
				.sorted( this.sorting.comparator )
				.toList();
		else
			return List.of();
	}

	private class SymbolTableModel extends AbstractTableModel {
		private static final List<String> COL_NAMES = List.of("Label", "Address", "Value");

		@Override
		public int getColumnCount() {
			return COL_NAMES.size();
		}

		@Override
		public int getRowCount() {
			return LabelsWindow.this.getSymbols().size();
		}

		@Override
		public String getColumnName(int column) {
			return COL_NAMES.get(column) + LabelsWindow.this.sorting.getSymbol( column );
		}

		@Override
		public Object getValueAt( int rowIndex, int columnIndex ) {
			if (! LabelsWindow.this.getSymbols().isEmpty() ) {
				var symbol = LabelsWindow.this.getSymbols().get(rowIndex);
				try {
					return switch(columnIndex) {
						default -> symbol.getName();
						case 1 -> Binary.intToHexString( symbol.getAddress() );
						case 2 -> Binary.intToHexString( Memory.getInstance().getWord( symbol.getAddress() ) );
					};
				} catch (AddressErrorException ignored) { }
			}
			return null;
		}
	}

	private enum Sorting {
		BY_NAME(
			Comparator.comparing( Symbol::getName ),
			0,
			"BY_NAME_REVERSED"
		),
		BY_NAME_REVERSED(
			BY_NAME.comparator.reversed(),
			0,
			"BY_NAME"
		),
		BY_ADDRESS(
			( Symbol symbol0, Symbol symbol1 ) -> {
				int addr0 = symbol0.getAddress(), addr1 = symbol1.getAddress();
				return addr0 >= 0 && addr1 >= 0 || addr0 < 0 && addr1 < 0 ? addr0 - addr1 : addr1;
			},
			1,
			"BY_ADDRESS_REVERSED"
		),
		BY_ADDRESS_REVERSED(
			BY_ADDRESS.comparator.reversed(),
			1,
			"BY_ADDRESS"
		);

		private final Comparator<Symbol> comparator;
		private final String reversed;
		private final int column;

		Sorting(Comparator<Symbol> comparator, int column, String reversed ) {
			this.comparator = comparator;
			this.reversed = reversed;
			this.column = column;
		}

		public boolean isByLabel() {
			return this == BY_NAME || this == BY_NAME_REVERSED;
		}

		public boolean isByAddress() {
			return this == BY_ADDRESS || this == BY_ADDRESS_REVERSED;
		}

		public Sorting reverse() {
			return valueOf( this.reversed );
		}

		public String getSymbol(int column) {
			if ( this.column == column )
				return this.name().endsWith("ED") ? "▼" : "▲";
			return "";
		}
	}
}