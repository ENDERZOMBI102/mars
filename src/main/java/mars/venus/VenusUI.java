package mars.venus;

import com.enderzombi102.mars.res.Resources;
import com.enderzombi102.mars.ui.SplashScreen;
import mars.Const;
import mars.Globals;
import mars.Settings;
import mars.mips.dump.DumpFormatLoader;
import mars.simulator.Simulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.net.URL;

import static com.enderzombi102.mars.res.Localization.loc;

/*
 * Copyright (c) 2003-2013, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * Top level container for Venus GUI.
 *
 * @author Sanderson and Team JSpim
 **/

/* Heavily modified by Pete Sanderson, July 2004, to incorporate JSPIMMenu and JSPIMToolbar
 * not as subclasses of JMenuBar and JToolBar, but as instances of them.  They are both
 * here primarily so both can share the Action objects.
 */

@SuppressWarnings("FieldCanBeLocal")
public class VenusUI extends JFrame {
	private static final Logger LOGGER = LoggerFactory.getLogger( VenusUI.class );
	VenusUI mainUI;
	public JMenuBar menu;
	JToolBar toolbar;
	MainPane mainPane;
	RegistersPane registersPane;
	RegistersWindow registersTab;
	Coprocessor1Window coprocessor1Tab;
	Coprocessor0Window coprocessor0Tab;
	MessagesPane messagesPane;
	JSplitPane splitter, horizonSplitter;

	private static int menuState = FileStatus.NO_FILE;

	// PLEASE PUT THESE TWO (& THEIR METHODS) SOMEWHERE THEY BELONG, NOT HERE
	private static boolean reset = true; // registers/memory reset for execution
	private static boolean started = false;  // started execution
	Editor editor;

	// components of the menubar
	private JMenu file, run, help, edit, settings;
	private JMenuItem fileNew, fileOpen, fileClose, fileCloseAll, fileSave, fileSaveAs, fileSaveAll, fileDumpMemory,
			filePrint, fileExit;
	private JMenuItem editUndo, editRedo, editCut, editCopy, editPaste, editFindReplace, editSelectAll;
	private JMenuItem runGo, runStep, runBackstep, runReset, runAssemble, runStop, runPause, runClearBreakpoints,
			runToggleBreakpoints;
	private JCheckBoxMenuItem settingsLabel, settingsPopupInput, settingsValueDisplayBase, settingsAddressDisplayBase,
								settingsExtended, settingsAssembleOnOpen, settingsAssembleAll, settingsWarningsAreErrors,
								settingsStartAtMain, settingsDelayedBranching, settingsProgramArguments,
								settingsSelfModifyingCode;
	private JMenuItem settingsExceptionHandler, settingsEditor, settingsHighlighting, settingsThemes, settingsMemoryConfiguration;
	private JMenuItem helpHelp, helpAbout;

	// components of the toolbar
	private JButton Undo, Redo, Cut, Copy, Paste, FindReplace, SelectAll;
	private JButton New, Open, Save, SaveAs, DumpMemory, Print;
	private JButton Run, Assemble, Reset, Step, Backstep, Stop, Pause;
	private JButton Help;

	// The "action" objects, which include action listeners.  One of each will be created then
	// shared between a menu item and its corresponding toolbar button.  This is a very cool
	// technique because it relates the button and menu item so closely

	private javax.swing.Action fileNewAction, fileOpenAction, fileCloseAction, fileCloseAllAction, fileSaveAction;
	private javax.swing.Action fileSaveAsAction, fileSaveAllAction, fileDumpMemoryAction, filePrintAction, fileExitAction;
	EditUndoAction editUndoAction;
	EditRedoAction editRedoAction;
	private javax.swing.Action editCutAction, editCopyAction, editPasteAction, editFindReplaceAction, editSelectAllAction;
	private RunAssembleAction runAssembleAction;
	private javax.swing.Action runGoAction, runStepAction, runBackstepAction, runResetAction, runStopAction,
					runPauseAction, runClearBreakpointsAction, runToggleBreakpointsAction;
	private javax.swing.Action settingsLabelAction, settingsPopupInputAction, settingsValueDisplayBaseAction,
					settingsAddressDisplayBaseAction, settingsExtendedAction, settingsAssembleOnOpenAction,
					settingsAssembleAllAction, settingsWarningsAreErrorsAction, settingsStartAtMainAction,
					settingsProgramArgumentsAction, settingsDelayedBranchingAction, settingsExceptionHandlerAction,
					settingsEditorAction, settingsHighlightingAction, settingsThemesAction,
					settingsMemoryConfigurationAction, settingsSelfModifyingCodeAction;
	private javax.swing.Action helpHelpAction, helpAboutAction;

	public VenusUI() {
		super( "MARS " + Const.VERSION);
		mainUI = this;
		Globals.setGui(this);
		editor = new Editor(this);

		final double screenWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
		final double screenHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
		// basically give up some screen space if running at 800 x 600
		final double messageWidthPct = screenWidth < 1000.0 ? 0.67 : 0.73;
		final double messageHeightPct = screenWidth < 1000.0 ? 0.12 : 0.15;
		final double mainWidthPct = screenWidth < 1000.0 ? 0.67 : 0.73;
		final double mainHeightPct = screenWidth < 1000.0 ? 0.60 : 0.65;
		final double registersWidthPct = screenWidth < 1000.0 ? 0.18 : 0.22;
		final double registersHeightPct = screenWidth < 1000.0 ? 0.72 : 0.80;

		final Dimension messagesPanePreferredSize = new Dimension(
			(int) (screenWidth * messageWidthPct),
			(int) (screenHeight * messageHeightPct)
		);
		final Dimension mainPanePreferredSize = new Dimension(
			(int) (screenWidth * mainWidthPct),
			(int) (screenHeight * mainHeightPct)
		);
		final Dimension registersPanePreferredSize = new Dimension(
			(int) (screenWidth * registersWidthPct),
			(int) (screenHeight * registersHeightPct)
		);

		Globals.initialize();

		final URL im = this.getClass().getResource( Globals.imagesPath + "RedMars16.gif" );
		if ( im == null ) {
			System.out.println("Internal Error: images folder or file not found");
			System.exit(0);
		}
		final Image mars = Toolkit.getDefaultToolkit().getImage(im);
		setIconImage(mars);
		// Everything in frame will be arranged on JPanel "center", which is only frame component.
		// "center" has BorderLayout and 2 major components:
		//   -- panel (jp) on North with 2 components
		//      1. toolbar
		//      2. run speed slider.
		//   -- split pane (horizonSplitter) in center with 2 components side-by-side
		//      1. split pane (splitter) with 2 components stacked
		//         a. main pane, with 2 tabs (edit, execute)
		//         b. messages pane with 2 tabs (mars, run I/O)
		//      2. registers pane with 3 tabs (register file, coproc 0, coproc 1)
		// I should probably run this breakdown out to full detail.  The components are created
		// roughly in bottom-up order; some are created in component constructors and thus are
		// not visible here.

		registersTab = new RegistersWindow();
		coprocessor1Tab = new Coprocessor1Window();
		coprocessor0Tab = new Coprocessor0Window();
		registersPane = new RegistersPane(mainUI, registersTab, coprocessor1Tab, coprocessor0Tab);
		registersPane.setPreferredSize(registersPanePreferredSize);

		mainPane = new MainPane(mainUI, editor, registersTab, coprocessor1Tab, coprocessor0Tab);

		mainPane.setPreferredSize(mainPanePreferredSize);
		messagesPane = new MessagesPane();
		messagesPane.setPreferredSize(messagesPanePreferredSize);
		splitter = new JSplitPane(JSplitPane.VERTICAL_SPLIT, mainPane, messagesPane);
		splitter.setOneTouchExpandable(true);
		splitter.resetToPreferredSizes();
		horizonSplitter = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, splitter, registersPane);
		horizonSplitter.setOneTouchExpandable(true);
		horizonSplitter.resetToPreferredSizes();

		// due to dependencies, do not set up menu/toolbar until now.
		createActionObjects();
		menu = setUpMenuBar();
		setJMenuBar(menu);

		toolbar = setUpToolBar();

		final JPanel jp = new JPanel(new FlowLayout(FlowLayout.LEFT));
		jp.add(toolbar);
		jp.add(RunSpeedPanel.getInstance());
		final JPanel center = new JPanel(new BorderLayout());
		center.add(jp, BorderLayout.NORTH);
		center.add(horizonSplitter);

		getContentPane().add(center);

		FileStatus.reset();
		// The following has side effect of establishing menu state
		FileStatus.set(FileStatus.NO_FILE);

		// This is invoked when opening the app.  It will set the app to
		// appear at full screen size.
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(final WindowEvent e) {
				mainUI.setExtendedState(Frame.MAXIMIZED_BOTH);
			}
		});

		// This is invoked when exiting the app through the X icon.  It will in turn
		// check for unsaved edits before exiting.
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent e) {
				if (mainUI.editor.closeAll()) {
					System.exit(0);
				}
			}
		});

		// The following will handle the windowClosing event properly in the
		// situation where user Cancels out of "save edits?" dialog.  By default,
		// the GUI frame will be hidden but I want it to do nothing.
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

		pack();
		setVisible(true);
	}

	/*
	 * Action objects are used instead of action listeners because one can be easily shared between
	 * a menu item and a toolbar button.
	 * Does nice things like disable both if the action is disabled, etc.
	 */
	private void createActionObjects() {
		try {
			fileNewAction = new Action(
				"new",
				"New22.png",
				KeyEvent.VK_N,
				KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> mainUI.editor.newFile()
			);
			fileOpenAction = new Action(
				"open",
				"Open22.png",
				KeyEvent.VK_O,
				KeyStroke.getKeyStroke( KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() ),
				mainUI,
				evt -> mainUI.editor.open()
			);
			fileCloseAction = new Action(
				"close",
				null,
				KeyEvent.VK_C,
				KeyStroke.getKeyStroke( KeyEvent.VK_W, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() ),
				mainUI,
				evt -> mainUI.editor.close()
			);
			fileCloseAllAction = new Action(
				"close_all",
				null,
				KeyEvent.VK_L,
				null,
				mainUI,
				evt -> mainUI.editor.closeAll()
			);
			fileSaveAction = new Action(
				"save",
				"Save22.png",
				KeyEvent.VK_S,
				KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> mainUI.editor.save()
			);
			fileSaveAsAction = new Action(
				"save_as",
				"SaveAs22.png",
				KeyEvent.VK_A,
				null,
				mainUI,
				evt -> mainUI.editor.saveAs()
			);
			fileSaveAllAction = new Action(
				"save_all",
				null,
				KeyEvent.VK_V,
				null,
				mainUI,
				evt -> mainUI.editor.saveAll()
			);
			fileDumpMemoryAction = new FileDumpMemoryAction( mainUI );
			filePrintAction = new FilePrintAction( mainUI );
			fileExitAction = new Action(
				"exit",
				null,
				KeyEvent.VK_X,
				null,
				mainUI,
				evt -> {
					if ( mainUI.editor.closeAll() )
						System.exit(0);
				}
			);
			editUndoAction = new EditUndoAction( mainUI );
			editRedoAction = new EditRedoAction( mainUI );
			editCutAction = new Action(
				"cut",
				"Cut22.gif",
				KeyEvent.VK_C,
				KeyStroke.getKeyStroke( KeyEvent.VK_X, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx() ),
				mainUI,
				evt -> mainUI.getMainPane().getEditPane().cutText()
			);
			editCopyAction = new Action(
				"copy",
				"Copy22.png",
				KeyEvent.VK_O,
				KeyStroke.getKeyStroke(KeyEvent.VK_C, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> mainUI.getMainPane().getEditPane().copyText()
			);
			editPasteAction = new Action(
				"paste",
				"Paste22.png",
				KeyEvent.VK_P,
				KeyStroke.getKeyStroke(KeyEvent.VK_V, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> mainUI.getMainPane().getEditPane().pasteText()
			);
			editFindReplaceAction = new EditFindReplaceAction( mainUI );
			editSelectAllAction = new Action(
				"select_all",
				null,
				KeyEvent.VK_A,
				KeyStroke.getKeyStroke(KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> mainUI.getMainPane().getEditPane().selectAllText()
			);
			runAssembleAction = new RunAssembleAction( mainUI );
			runGoAction = new RunGoAction( mainUI );
			runStepAction = new RunStepAction( mainUI );
			runBackstepAction = new RunBackstepAction( mainUI );
			runPauseAction = new Action(
				"pause",
				"Pause22.png",
				KeyEvent.VK_P,
				KeyStroke.getKeyStroke(KeyEvent.VK_F9, 0),
				mainUI,
				evt -> Simulator.getInstance().stopExecution( (AbstractAction) runPauseAction )
			);
			runStopAction = new Action(
				"stop",
				"Stop22.png",
				KeyEvent.VK_S,
				KeyStroke.getKeyStroke(KeyEvent.VK_F11, 0),
				mainUI,
				evt -> Simulator.getInstance().stopExecution( (AbstractAction) runStopAction )
			);
			runResetAction = new RunResetAction( mainUI );
			runClearBreakpointsAction = new RunClearBreakpointsAction( mainUI );
			runToggleBreakpointsAction = new Action(
				"toggle_all_breakpoints",
				null,
				KeyEvent.VK_T,
				KeyStroke.getKeyStroke(KeyEvent.VK_T, Toolkit.getDefaultToolkit().getMenuShortcutKeyMaskEx()),
				mainUI,
				evt -> Globals.getGui().getMainPane().getExecutePane().getTextSegmentWindow().toggleBreakpoints()
			);
			settingsLabelAction = new Action(
				"show_symbol_table",
				null,
				null,
				null,
				mainUI,
				evt -> {
					final boolean visibility = ( (JCheckBoxMenuItem) evt.getSource() ).isSelected();
					Globals.getGui().getMainPane().getExecutePane().setLabelWindowVisibility(visibility);
					Globals.getSettings().setBooleanSetting( Settings.LABEL_WINDOW_VISIBILITY, visibility );
				}
			);
			settingsPopupInputAction = new Action(
				"popup_input_syscall_dialog",
				null,
				null,
				null,
				mainUI,
				evt -> {
					final boolean usePopup = ( (JCheckBoxMenuItem) evt.getSource() ).isSelected();
					Globals.getSettings().setBooleanSetting(Settings.POPUP_SYSCALL_INPUT, usePopup);
				}
			);
			settingsValueDisplayBaseAction = new Action(
				"display_values_in_hex",
				null,
				null,
				null,
				mainUI,
				evt -> {
					final boolean isHex = ( (JCheckBoxMenuItem) evt.getSource() ).isSelected();
					Globals.getGui().getMainPane().getExecutePane().getValueDisplayBaseChooser().setSelected(isHex);
					Globals.getSettings().setBooleanSetting(Settings.DISPLAY_VALUES_IN_HEX, isHex);
				}
			);
			settingsAddressDisplayBaseAction = new Action(
				"display_addresses_in_hex",
				null,
				null,
				null,
				mainUI,
				evt -> {
					final boolean isHex = ( (JCheckBoxMenuItem) evt.getSource() ).isSelected();
					Globals.getGui().getMainPane().getExecutePane().getAddressDisplayBaseChooser().setSelected(isHex);
					Globals.getSettings().setBooleanSetting(Settings.DISPLAY_ADDRESSES_IN_HEX, isHex);
				}
			);
			settingsExtendedAction = new Action(
				"allow_extended_instructions",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.EXTENDED_ASSEMBLER_ENABLED,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsAssembleOnOpenAction = new Action(
				"assemble_file_on_opening",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.ASSEMBLE_ON_OPEN_ENABLED,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsAssembleAllAction = new Action(
				"assemble_all_files_in_directory",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.ASSEMBLE_ALL_ENABLED,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsWarningsAreErrorsAction = new Action(
				"assembler_warnings_are_errors",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.WARNINGS_ARE_ERRORS,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsStartAtMainAction = new Action(
				"pc_starts_as_main",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.START_AT_MAIN,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsProgramArgumentsAction = new Action(
				"mips_program_arguments",
				null,
				null,
				null,
				mainUI,
					evt -> {
						final boolean selected = ( (JCheckBoxMenuItem) evt.getSource() ).isSelected();
						Globals.getSettings().setBooleanSetting( Settings.PROGRAM_ARGUMENTS, selected );
						if (selected) {
							Globals.getGui().getMainPane().getExecutePane().getTextSegmentWindow().addProgramArgumentsPanel();
						} else {
							Globals.getGui().getMainPane().getExecutePane().getTextSegmentWindow().removeProgramArgumentsPanel();
						}
					}
			);
			settingsDelayedBranchingAction = new Action(
				"enable_delayed_branching",
				null,
				null,
				null,
				mainUI,
				evt -> {
					Globals.getSettings().setBooleanSetting(
						Settings.DELAYED_BRANCHING_ENABLED,
						( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
					);
					// 25 June 2007 Re-assemble if the situation demands it to maintain consistency.
					if (
						Globals.getGui() != null && (
							FileStatus.get() == FileStatus.RUNNABLE ||
							FileStatus.get() == FileStatus.RUNNING ||
							FileStatus.get() == FileStatus.TERMINATED
						)
					) {
						// Stop execution if executing -- should NEVER happen because this
						// Action's widget is disabled during MIPS execution.
						if (FileStatus.get() == FileStatus.RUNNING)
							Simulator.getInstance().stopExecution( (AbstractAction) settingsDelayedBranchingAction );
						Globals.getGui().getRunAssembleAction().actionPerformed(null);
					}
				}
			);
			settingsSelfModifyingCodeAction = new Action(
				"enable_self_modifying_code",
				null,
				null,
				null,
				mainUI,
				evt -> Globals.getSettings().setBooleanSetting(
					Settings.SELF_MODIFYING_CODE_ENABLED,
					( (JCheckBoxMenuItem) evt.getSource() ).isSelected()
				)
			);
			settingsEditorAction = new SettingsEditorAction( mainUI );
			settingsHighlightingAction = new SettingsHighlightingAction( mainUI );
			settingsThemesAction = new SettingsThemesAction( mainUI );
			settingsExceptionHandlerAction = new SettingsExceptionHandlerAction( mainUI );
			settingsMemoryConfigurationAction = new SettingsMemoryConfigurationAction( mainUI );
			helpHelpAction = new HelpHelpAction( mainUI );
			helpAboutAction = new Action(
				"about",
				null,
				null,
				null,
				mainUI,
				evt -> {
					if ( ( evt.getModifiers() & InputEvent.CTRL_MASK ) != 0 ) {
						new SplashScreen( true );
					} else {
						JOptionPane.showMessageDialog(
							mainUI,
							"""
							MARS %s    Copyright %s
							%s
							
							Modified by ENDERZOMBI102, 2022
							Theme Engine by aeris170, 2020
							
							MARS is the Mips Assembler and Runtime Simulator.
							Mars image courtesy of NASA/JPL.
							Toolbar and menu icons are from:
							  - Tango Desktop Project (tango.freedesktop.org),
							  - glyFX (www.glyfx.com) Common Toolbar Set,
							  - KDE-Look (www.kde-look.org) crystalline-blue-0.1,
							  - Icon-King (www.icon-king.com) Nuvola 1.0.
							Print feature adapted from HardcopyWriter class in David Flanagan's
							Java Examples in a Nutshell 3rd Edition, O'Reilly, ISBN 0-596-00620-9.
							""".formatted( Const.VERSION, Const.OG_COPYRIGHT_YEARS, Const.OG_COPYRIGHT_HOLDERS ),
							"About Mars",
							JOptionPane.INFORMATION_MESSAGE,
							new ImageIcon( "images/RedMars50.gif" )
						);
					}
				}
			);
		} catch (final NullPointerException e) {
			LOGGER.error( "Internal Error: images folder not found, or other null pointer exception while creating Action objects", e );
			System.exit( 1 );
		}
	}

	/*
	 * build the menus and connect them to action objects (which serve as action listeners
	 * shared between menu item and corresponding toolbar icon).
	 */
	private JMenuBar setUpMenuBar() {
		final JMenuBar menuBar = new JMenuBar();
		file = new JMenu( loc( "mars.menu.file.name" ) );
		file.setMnemonic(KeyEvent.VK_F);
		edit = new JMenu( loc( "mars.menu.edit.name" ) );
		edit.setMnemonic(KeyEvent.VK_E);
		run = new JMenu( loc( "mars.menu.run.name" ) );
		run.setMnemonic(KeyEvent.VK_R);
		settings = new JMenu( loc( "mars.menu.settings.name" ) );
		settings.setMnemonic(KeyEvent.VK_S);
		help = new JMenu( loc( "mars.menu.help.name" ) );
		help.setMnemonic(KeyEvent.VK_H);
		// slight bug: user typing alt-H activates help menu item directly, not help menu

		fileNew = new JMenuItem(fileNewAction);
		fileNew.setIcon( Resources.getIcon( Globals.imagesPath + "New16.png") );
		fileOpen = new JMenuItem(fileOpenAction);
		fileOpen.setIcon( Resources.getIcon( Globals.imagesPath + "Open16.png") );
		fileClose = new JMenuItem(fileCloseAction);
		fileClose.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif") );
		fileCloseAll = new JMenuItem(fileCloseAllAction);
		fileCloseAll.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif") );
		fileSave = new JMenuItem(fileSaveAction);
		fileSave.setIcon( Resources.getIcon( Globals.imagesPath + "Save16.png") );
		fileSaveAs = new JMenuItem(fileSaveAsAction);
		fileSaveAs.setIcon( Resources.getIcon( Globals.imagesPath + "SaveAs16.png") );
		fileSaveAll = new JMenuItem(fileSaveAllAction);
		fileSaveAll.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif") );
		fileDumpMemory = new JMenuItem(fileDumpMemoryAction);
		fileDumpMemory.setIcon( Resources.getIcon( Globals.imagesPath + "Dump16.png") );
		filePrint = new JMenuItem(filePrintAction);
		filePrint.setIcon( Resources.getIcon( Globals.imagesPath + "Print16.gif") );
		fileExit = new JMenuItem(fileExitAction);
		fileExit.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif") );
		file.add(fileNew);
		file.add(fileOpen);
		file.add(fileClose);
		file.add(fileCloseAll);
		file.addSeparator();
		file.add(fileSave);
		file.add(fileSaveAs);
		file.add(fileSaveAll);
		if ( new DumpFormatLoader().loadDumpFormats().size() > 0 )
			file.add(fileDumpMemory);
		file.addSeparator();
		file.add(filePrint);
		file.addSeparator();
		file.add(fileExit);

		editUndo = new JMenuItem(editUndoAction);
		editUndo.setIcon( Resources.getIcon( Globals.imagesPath + "Undo16.png" ) );//"Undo16.gif"))));
		editRedo = new JMenuItem(editRedoAction);
		editRedo.setIcon( Resources.getIcon( Globals.imagesPath + "Redo16.png" ) );//"Redo16.gif"))));
		editCut = new JMenuItem(editCutAction);
		editCut.setIcon( Resources.getIcon( Globals.imagesPath + "Cut16.gif" ) );
		editCopy = new JMenuItem(editCopyAction);
		editCopy.setIcon( Resources.getIcon( Globals.imagesPath + "Copy16.png" ) );//"Copy16.gif"))));
		editPaste = new JMenuItem(editPasteAction);
		editPaste.setIcon( Resources.getIcon( Globals.imagesPath + "Paste16.png" ) );//"Paste16.gif"))));
		editFindReplace = new JMenuItem(editFindReplaceAction);
		editFindReplace.setIcon( Resources.getIcon( Globals.imagesPath + "Find16.png" ) );//"Paste16.gif"))));
		editSelectAll = new JMenuItem(editSelectAllAction);
		editSelectAll.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif" ) );
		edit.add(editUndo);
		edit.add(editRedo);
		edit.addSeparator();
		edit.add(editCut);
		edit.add(editCopy);
		edit.add(editPaste);
		edit.addSeparator();
		edit.add(editFindReplace);
		edit.add(editSelectAll);

		runAssemble = new JMenuItem(runAssembleAction);
		runAssemble.setIcon( Resources.getIcon( Globals.imagesPath + "Assemble16.png" ) );//"MyAssemble16.gif"))));
		runGo = new JMenuItem(runGoAction);
		runGo.setIcon( Resources.getIcon( Globals.imagesPath + "Play16.png" ) );//"Play16.gif"))));
		runStep = new JMenuItem(runStepAction);
		runStep.setIcon( Resources.getIcon( Globals.imagesPath + "StepForward16.png" ) );//"MyStepForward16.gif"))));
		runBackstep = new JMenuItem(runBackstepAction);
		runBackstep.setIcon( Resources.getIcon( Globals.imagesPath + "StepBack16.png" ) );//"MyStepBack16.gif"))));
		runReset = new JMenuItem(runResetAction);
		runReset.setIcon( Resources.getIcon( Globals.imagesPath + "Reset16.png" ) );//"MyReset16.gif"))));
		runStop = new JMenuItem(runStopAction);
		runStop.setIcon( Resources.getIcon( Globals.imagesPath + "Stop16.png" ) );//"Stop16.gif"))));
		runPause = new JMenuItem(runPauseAction);
		runPause.setIcon( Resources.getIcon( Globals.imagesPath + "Pause16.png" ) );//"Pause16.gif"))));
		runClearBreakpoints = new JMenuItem(runClearBreakpointsAction);
		runClearBreakpoints.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif" ) );
		runToggleBreakpoints = new JMenuItem(runToggleBreakpointsAction);
		runToggleBreakpoints.setIcon( Resources.getIcon( Globals.imagesPath + "MyBlank16.gif" ) );

		run.add(runAssemble);
		run.add(runGo);
		run.add(runStep);
		run.add(runBackstep);
		run.add(runPause);
		run.add(runStop);
		run.add(runReset);
		run.addSeparator();
		run.add(runClearBreakpoints);
		run.add(runToggleBreakpoints);

		settingsLabel = new JCheckBoxMenuItem(settingsLabelAction);
		settingsLabel.setSelected(Globals.getSettings().getBooleanSetting(Settings.LABEL_WINDOW_VISIBILITY));
		settingsPopupInput = new JCheckBoxMenuItem(settingsPopupInputAction);
		settingsPopupInput.setSelected(Globals.getSettings().getBooleanSetting(Settings.POPUP_SYSCALL_INPUT));
		settingsValueDisplayBase = new JCheckBoxMenuItem(settingsValueDisplayBaseAction);
		settingsValueDisplayBase.setSelected(Globals.getSettings().getBooleanSetting(Settings.DISPLAY_VALUES_IN_HEX));
		// Tell the corresponding JCheckBox in the Execute Pane about me -- it has already been created.
		mainPane.getExecutePane().getValueDisplayBaseChooser().setSettingsMenuItem(settingsValueDisplayBase);
		settingsAddressDisplayBase = new JCheckBoxMenuItem(settingsAddressDisplayBaseAction);
		settingsAddressDisplayBase.setSelected(Globals.getSettings().getBooleanSetting(Settings.DISPLAY_ADDRESSES_IN_HEX));
		// Tell the corresponding JCheckBox in the Execute Pane about me -- it has already been created.
		mainPane.getExecutePane().getAddressDisplayBaseChooser().setSettingsMenuItem(settingsAddressDisplayBase);
		settingsExtended = new JCheckBoxMenuItem(settingsExtendedAction);
		settingsExtended.setSelected(Globals.getSettings().getBooleanSetting(Settings.EXTENDED_ASSEMBLER_ENABLED));
		settingsDelayedBranching = new JCheckBoxMenuItem(settingsDelayedBranchingAction);
		settingsDelayedBranching.setSelected(Globals.getSettings().getBooleanSetting(Settings.DELAYED_BRANCHING_ENABLED));
		settingsSelfModifyingCode = new JCheckBoxMenuItem(settingsSelfModifyingCodeAction);
		settingsSelfModifyingCode.setSelected(Globals.getSettings().getBooleanSetting(Settings.SELF_MODIFYING_CODE_ENABLED));
		settingsAssembleOnOpen = new JCheckBoxMenuItem(settingsAssembleOnOpenAction);
		settingsAssembleOnOpen.setSelected(Globals.getSettings().getBooleanSetting(Settings.ASSEMBLE_ON_OPEN_ENABLED));
		settingsAssembleAll = new JCheckBoxMenuItem(settingsAssembleAllAction);
		settingsAssembleAll.setSelected(Globals.getSettings().getBooleanSetting(Settings.ASSEMBLE_ALL_ENABLED));
		settingsWarningsAreErrors = new JCheckBoxMenuItem(settingsWarningsAreErrorsAction);
		settingsWarningsAreErrors.setSelected(Globals.getSettings().getBooleanSetting(Settings.WARNINGS_ARE_ERRORS));
		settingsStartAtMain = new JCheckBoxMenuItem(settingsStartAtMainAction);
		settingsStartAtMain.setSelected(Globals.getSettings().getBooleanSetting(Settings.START_AT_MAIN));
		settingsProgramArguments = new JCheckBoxMenuItem(settingsProgramArgumentsAction);
		settingsProgramArguments.setSelected(Globals.getSettings().getBooleanSetting(Settings.PROGRAM_ARGUMENTS));
		settingsEditor = new JMenuItem(settingsEditorAction);
		settingsHighlighting = new JMenuItem(settingsHighlightingAction);
		settingsThemes = new JMenuItem(settingsThemesAction);
		settingsExceptionHandler = new JMenuItem(settingsExceptionHandlerAction);
		settingsMemoryConfiguration = new JMenuItem(settingsMemoryConfigurationAction);

		settings.add(settingsLabel);
		settings.add(settingsProgramArguments);
		settings.add(settingsPopupInput);
		settings.add(settingsAddressDisplayBase);
		settings.add(settingsValueDisplayBase);
		settings.addSeparator();
		settings.add(settingsAssembleOnOpen);
		settings.add(settingsAssembleAll);
		settings.add(settingsWarningsAreErrors);
		settings.add(settingsStartAtMain);
		settings.addSeparator();
		settings.add(settingsExtended);
		settings.add(settingsDelayedBranching);
		settings.add(settingsSelfModifyingCode);
		settings.addSeparator();
		settings.add(settingsEditor);
		settings.add(settingsHighlighting);
		settings.add(settingsThemes);
		settings.add(settingsExceptionHandler);
		settings.add(settingsMemoryConfiguration);

		helpHelp = new JMenuItem(helpHelpAction);
		helpHelp.setIcon( Resources.getIcon( Globals.imagesPath + "Help16.png" ) );
		helpAbout = new JMenuItem(helpAboutAction);
		helpAbout.setIcon( Resources.getIcon(Globals.imagesPath + "MyBlank16.gif") );
		help.add(helpHelp);
		help.addSeparator();
		help.add(helpAbout);

		menuBar.add(file);
		menuBar.add(edit);
		menuBar.add(run);
		menuBar.add(settings);
		final JMenu toolMenu = new ToolLoader().buildToolsMenu();
		if (toolMenu != null) {
			menuBar.add( toolMenu );
		}
		menuBar.add(help);

		// experiment with a popup menu for settings. 3 Aug 2006 PS
		//setupPopupMenu();

		return menuBar;
	}

	/*
	 * build the toolbar and connect items to action objects (which serve as action listeners
	 * shared between toolbar icon and corresponding menu item).
	 */
	JToolBar setUpToolBar() {
		final JToolBar toolBar = new JToolBar();

		New = new JButton(fileNewAction);
		New.setText("");
		Open = new JButton(fileOpenAction);
		Open.setText("");
		Save = new JButton(fileSaveAction);
		Save.setText("");
		SaveAs = new JButton(fileSaveAsAction);
		SaveAs.setText("");
		DumpMemory = new JButton(fileDumpMemoryAction);
		DumpMemory.setText("");
		Print = new JButton(filePrintAction);
		Print.setText("");

		Undo = new JButton(editUndoAction);
		Undo.setText("");
		Redo = new JButton(editRedoAction);
		Redo.setText("");
		Cut = new JButton(editCutAction);
		Cut.setText("");
		Copy = new JButton(editCopyAction);
		Copy.setText("");
		Paste = new JButton(editPasteAction);
		Paste.setText("");
		FindReplace = new JButton(editFindReplaceAction);
		FindReplace.setText("");
		SelectAll = new JButton(editSelectAllAction);
		SelectAll.setText("");

		Run = new JButton(runGoAction);
		Run.setText("");
		Assemble = new JButton(runAssembleAction);
		Assemble.setText("");
		Step = new JButton(runStepAction);
		Step.setText("");
		Backstep = new JButton(runBackstepAction);
		Backstep.setText("");
		Reset = new JButton(runResetAction);
		Reset.setText("");
		Stop = new JButton(runStopAction);
		Stop.setText("");
		Pause = new JButton(runPauseAction);
		Pause.setText("");
		Help = new JButton(helpHelpAction);
		Help.setText("");

		toolBar.add(New);
		toolBar.add(Open);
		toolBar.add(Save);
		toolBar.add(SaveAs);
		if ( !new DumpFormatLoader().loadDumpFormats().isEmpty() ) {
			toolBar.add( DumpMemory );
		}
		toolBar.add(Print);
		toolBar.add(new JToolBar.Separator());
		toolBar.add(Undo);
		toolBar.add(Redo);
		toolBar.add(Cut);
		toolBar.add(Copy);
		toolBar.add(Paste);
		toolBar.add(FindReplace);
		toolBar.add(new JToolBar.Separator());
		toolBar.add(Assemble);
		toolBar.add(Run);
		toolBar.add(Step);
		toolBar.add(Backstep);
		toolBar.add(Pause);
		toolBar.add(Stop);
		toolBar.add(Reset);
		toolBar.add(new JToolBar.Separator());
		toolBar.add(Help);
		toolBar.add(new JToolBar.Separator());

		return toolBar;
	}

	/* Determine from FileStatus what the menu state (enabled/disabled)should
	 * be then call the appropriate method to set it.  Current states are:
	 *
	 * setMenuStateInitial: set upon startup and after File->Close
	 * setMenuStateEditingNew: set upon File->New
	 * setMenuStateEditing: set upon File->Open or File->Save or erroneous Run->Assemble
	 * setMenuStateRunnable: set upon successful Run->Assemble
	 * setMenuStateRunning: set upon Run->Go
	 * setMenuStateTerminated: set upon completion of simulated execution
	 */
	void setMenuState(final int status) {
		menuState = status;
		switch (status) {
			case FileStatus.NO_FILE:
				setMenuStateInitial();
				break;
			case FileStatus.NEW_NOT_EDITED:
			case FileStatus.NEW_EDITED:
				setMenuStateEditingNew();
				break;
			case FileStatus.NOT_EDITED:
				setMenuStateNotEdited(); // was MenuStateEditing. DPS 9-Aug-2011
				break;
			case FileStatus.EDITED:
				setMenuStateEditing();
				break;
			case FileStatus.RUNNABLE:
				setMenuStateRunnable();
				break;
			case FileStatus.RUNNING:
				setMenuStateRunning();
				break;
			case FileStatus.TERMINATED:
				setMenuStateTerminated();
				break;
			case FileStatus.OPENING:// This is a temporary state. DPS 9-Aug-2011
				break;
			default:
				System.out.println("Invalid File Status: " + status);
				break;
		}
	}

	void setMenuStateInitial() {
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(false);
		fileCloseAllAction.setEnabled(false);
		fileSaveAction.setEnabled(false);
		fileSaveAsAction.setEnabled(false);
		fileSaveAllAction.setEnabled(false);
		fileDumpMemoryAction.setEnabled(false);
		filePrintAction.setEnabled(false);
		fileExitAction.setEnabled(true);
		editUndoAction.setEnabled(false);
		editRedoAction.setEnabled(false);
		editCutAction.setEnabled(false);
		editCopyAction.setEnabled(false);
		editPasteAction.setEnabled(false);
		editFindReplaceAction.setEnabled(false);
		editSelectAllAction.setEnabled(false);
		settingsDelayedBranchingAction.setEnabled(true); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(true); // added 21 July 2009
		runAssembleAction.setEnabled(false);
		runGoAction.setEnabled(false);
		runStepAction.setEnabled(false);
		runBackstepAction.setEnabled(false);
		runResetAction.setEnabled(false);
		runStopAction.setEnabled(false);
		runPauseAction.setEnabled(false);
		runClearBreakpointsAction.setEnabled(false);
		runToggleBreakpointsAction.setEnabled(false);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	/* Added DPS 9-Aug-2011, for newly-opened files.  Retain
	 existing Run menu state (except Assemble, which is always true).
	Thus if there was a valid assembly it is retained. */
	void setMenuStateNotEdited() {
		/* Note: undo and redo are handled separately by the undo manager*/
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(true);
		fileCloseAllAction.setEnabled(true);
		fileSaveAction.setEnabled(true);
		fileSaveAsAction.setEnabled(true);
		fileSaveAllAction.setEnabled(true);
		fileDumpMemoryAction.setEnabled(false);
		filePrintAction.setEnabled(true);
		fileExitAction.setEnabled(true);
		editCutAction.setEnabled(true);
		editCopyAction.setEnabled(true);
		editPasteAction.setEnabled(true);
		editFindReplaceAction.setEnabled(true);
		editSelectAllAction.setEnabled(true);
		settingsDelayedBranchingAction.setEnabled(true);
		settingsMemoryConfigurationAction.setEnabled(true);
		runAssembleAction.setEnabled(true);
		// If assemble-all, allow previous Run menu settings to remain.
		// Otherwise, clear them out.  DPS 9-Aug-2011
		if (! Globals.getSettings().getBooleanSetting(Settings.ASSEMBLE_ALL_ENABLED) ) {
			runGoAction.setEnabled(true);
			runStepAction.setEnabled(false);
			runBackstepAction.setEnabled(false);
			runResetAction.setEnabled(false);
			runStopAction.setEnabled(false);
			runPauseAction.setEnabled(false);
			runClearBreakpointsAction.setEnabled(false);
			runToggleBreakpointsAction.setEnabled(false);
		}
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	void setMenuStateEditing() {
		/* Note: undo and redo are handled separately by the undo manager*/
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(true);
		fileCloseAllAction.setEnabled(true);
		fileSaveAction.setEnabled(true);
		fileSaveAsAction.setEnabled(true);
		fileSaveAllAction.setEnabled(true);
		fileDumpMemoryAction.setEnabled(false);
		filePrintAction.setEnabled(true);
		fileExitAction.setEnabled(true);
		editCutAction.setEnabled(true);
		editCopyAction.setEnabled(true);
		editPasteAction.setEnabled(true);
		editFindReplaceAction.setEnabled(true);
		editSelectAllAction.setEnabled(true);
		settingsDelayedBranchingAction.setEnabled(true); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(true); // added 21 July 2009
		runAssembleAction.setEnabled(true);
		runGoAction.setEnabled(true);
		runStepAction.setEnabled(false);
		runBackstepAction.setEnabled(false);
		runResetAction.setEnabled(false);
		runStopAction.setEnabled(false);
		runPauseAction.setEnabled(false);
		runClearBreakpointsAction.setEnabled(false);
		runToggleBreakpointsAction.setEnabled(false);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	/**
	 * Use this when "File -> New" is used
	 */
	void setMenuStateEditingNew() {
		/* Note: undo and redo are handled separately by the undo manager*/
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(true);
		fileCloseAllAction.setEnabled(true);
		fileSaveAction.setEnabled(true);
		fileSaveAsAction.setEnabled(true);
		fileSaveAllAction.setEnabled(true);
		fileDumpMemoryAction.setEnabled(false);
		filePrintAction.setEnabled(true);
		fileExitAction.setEnabled(true);
		editCutAction.setEnabled(true);
		editCopyAction.setEnabled(true);
		editPasteAction.setEnabled(true);
		editFindReplaceAction.setEnabled(true);
		editSelectAllAction.setEnabled(true);
		settingsDelayedBranchingAction.setEnabled(true); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(true); // added 21 July 2009
		runAssembleAction.setEnabled(false);
		runGoAction.setEnabled(false);
		runStepAction.setEnabled(false);
		runBackstepAction.setEnabled(false);
		runResetAction.setEnabled(false);
		runStopAction.setEnabled(false);
		runPauseAction.setEnabled(false);
		runClearBreakpointsAction.setEnabled(false);
		runToggleBreakpointsAction.setEnabled(false);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	/* Use this upon successful assemble or reset */
	void setMenuStateRunnable() {
		/* Note: undo and redo are handled separately by the undo manager */
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(true);
		fileCloseAllAction.setEnabled(true);
		fileSaveAction.setEnabled(true);
		fileSaveAsAction.setEnabled(true);
		fileSaveAllAction.setEnabled(true);
		fileDumpMemoryAction.setEnabled(true);
		filePrintAction.setEnabled(true);
		fileExitAction.setEnabled(true);
		editCutAction.setEnabled(true);
		editCopyAction.setEnabled(true);
		editPasteAction.setEnabled(true);
		editFindReplaceAction.setEnabled(true);
		editSelectAllAction.setEnabled(true);
		settingsDelayedBranchingAction.setEnabled(true); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(true); // added 21 July 2009
		runAssembleAction.setEnabled(true);
		runGoAction.setEnabled(true);
		runStepAction.setEnabled(true);
		runBackstepAction.setEnabled( Globals.getSettings().getBackSteppingEnabled() && !Globals.program.getBackStepper().empty() );
		runResetAction.setEnabled(true);
		runStopAction.setEnabled(false);
		runPauseAction.setEnabled(false);
		runToggleBreakpointsAction.setEnabled(true);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	/* Use this while program is running */
	void setMenuStateRunning() {
		/* Note: undo and redo are handled separately by the undo manager */
		fileNewAction.setEnabled(false);
		fileOpenAction.setEnabled(false);
		fileCloseAction.setEnabled(false);
		fileCloseAllAction.setEnabled(false);
		fileSaveAction.setEnabled(false);
		fileSaveAsAction.setEnabled(false);
		fileSaveAllAction.setEnabled(false);
		fileDumpMemoryAction.setEnabled(false);
		filePrintAction.setEnabled(false);
		fileExitAction.setEnabled(false);
		editCutAction.setEnabled(false);
		editCopyAction.setEnabled(false);
		editPasteAction.setEnabled(false);
		editFindReplaceAction.setEnabled(false);
		editSelectAllAction.setEnabled(false);
		settingsDelayedBranchingAction.setEnabled(false); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(false); // added 21 July 2009
		runAssembleAction.setEnabled(false);
		runGoAction.setEnabled(false);
		runStepAction.setEnabled(false);
		runBackstepAction.setEnabled(false);
		runResetAction.setEnabled(false);
		runStopAction.setEnabled(true);
		runPauseAction.setEnabled(true);
		runToggleBreakpointsAction.setEnabled(false);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.setEnabled(false);//updateUndoState(); // DPS 10 Jan 2008
		editRedoAction.setEnabled(false);//updateRedoState(); // DPS 10 Jan 2008
	}

	/* Use this upon completion of execution */
	void setMenuStateTerminated() {
		/* Note: undo and redo are handled separately by the undo manager */
		fileNewAction.setEnabled(true);
		fileOpenAction.setEnabled(true);
		fileCloseAction.setEnabled(true);
		fileCloseAllAction.setEnabled(true);
		fileSaveAction.setEnabled(true);
		fileSaveAsAction.setEnabled(true);
		fileSaveAllAction.setEnabled(true);
		fileDumpMemoryAction.setEnabled(true);
		filePrintAction.setEnabled(true);
		fileExitAction.setEnabled(true);
		editCutAction.setEnabled(true);
		editCopyAction.setEnabled(true);
		editPasteAction.setEnabled(true);
		editFindReplaceAction.setEnabled(true);
		editSelectAllAction.setEnabled(true);
		settingsDelayedBranchingAction.setEnabled(true); // added 25 June 2007
		settingsMemoryConfigurationAction.setEnabled(true); // added 21 July 2009
		runAssembleAction.setEnabled(true);
		runGoAction.setEnabled(true);
		runStepAction.setEnabled(false);
		runBackstepAction.setEnabled( Globals.getSettings().getBackSteppingEnabled() && !Globals.program.getBackStepper().empty() );
		runResetAction.setEnabled(true);
		runStopAction.setEnabled(false);
		runPauseAction.setEnabled(false);
		runToggleBreakpointsAction.setEnabled(true);
		helpHelpAction.setEnabled(true);
		helpAboutAction.setEnabled(true);
		editUndoAction.updateUndoState();
		editRedoAction.updateRedoState();
	}

	/**
	 * Get current menu state. State values are constants in FileStatus class. DPS
	 * 23 July 2008
	 *
	 * @return current menu state.
	 **/

	public static int getMenuState() {
		return menuState;
	}

	/**
	 * To set whether the register values are reset.
	 *
	 * @param b Boolean true if the register values have been reset.
	 **/

	public static void setReset(final boolean b) {
		reset = b;
	}

	/**
	 * To set whether MIPS program execution has started.
	 *
	 * @param b true if the MIPS program execution has started.
	 **/

	public static void setStarted(final boolean b) {
		started = b;
	}

	/**
	 * To find out whether the register values are reset.
	 *
	 * @return Boolean true if the register values have been reset.
	 **/

	public static boolean getReset() {
		return reset;
	}

	/**
	 * To find out whether MIPS program is currently executing.
	 *
	 * @return true if MIPS program is currently executing.
	 **/
	public static boolean getStarted() {
		return started;
	}

	/**
	 * Get reference to Editor object associated with this GUI.
	 *
	 * @return Editor for the GUI.
	 **/
	public Editor getEditor() {
		return editor;
	}

	/**
	 * Get reference to main pane associated with this GUI.
	 *
	 * @return MainPane object associated with the GUI.
	 **/
	public MainPane getMainPane() {
		return mainPane;
	}

	/**
	 * Get reference to messages pane associated with this GUI.
	 *
	 * @return MessagesPane object associated with the GUI.
	 **/
	public MessagesPane getMessagesPane() {
		return messagesPane;
	}

	/**
	 * Get reference to registers pane associated with this GUI.
	 *
	 * @return RegistersPane object associated with the GUI.
	 **/
	public RegistersPane getRegistersPane() {
		return registersPane;
	}

	/**
	 * Get reference to settings menu item for display base of memory/register
	 * values.
	 *
	 * @return the menu item
	 **/
	public JCheckBoxMenuItem getValueDisplayBaseMenuItem() {
		return settingsValueDisplayBase;
	}

	/**
	 * Get reference to settings menu item for display base of memory/register
	 * values.
	 *
	 * @return the menu item
	 **/
	public JCheckBoxMenuItem getAddressDisplayBaseMenuItem() {
		return settingsAddressDisplayBase;
	}

	/**
	 * Return reference to the Run->Assemble item's action. Needed by File->Open in
	 * case assemble-upon-open flag is set.
	 *
	 * @return the Action object for the Run->Assemble operation.
	 */
	public RunAssembleAction getRunAssembleAction() {
		return runAssembleAction;
	}

	/**
	 * Have the menu request keyboard focus. DPS 5-4-10
	 */
	public void haveMenuRequestFocus() {
		menu.requestFocus();
	}

	/**
	 * Send keyboard event to menu for possible processing. DPS 5-4-10
	 *
	 * @param evt KeyEvent for menu component to consider for processing.
	 */
	public void dispatchEventToMenu(final KeyEvent evt) {
		menu.dispatchEvent(evt);
	}

	// pop up menu experiment 3 Aug 2006.  Keep for possible later revival.
	private void setupPopupMenu() {
		JPopupMenu popup;
		popup = new JPopupMenu();
		// cannot put the same menu item object on two different menus.
		// If you want to duplicate functionality, need a different item.
		// Should be able to share listeners, but if both menu items are
		// JCheckBoxMenuItem, how to keep their checked status in synch?
		// If you popup this menu and check the box, the right action occurs
		// but its counterpart on the regular menu is not checked.
		popup.add(new JCheckBoxMenuItem(settingsLabelAction));
		//Add listener to components that can bring up popup menus.
		final MouseListener popupListener = new PopupListener(popup);
		addMouseListener(popupListener);
	}
}
