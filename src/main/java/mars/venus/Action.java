package mars.venus;

import com.enderzombi102.mars.res.Resources;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.function.Consumer;

import static com.enderzombi102.mars.res.Localization.loc;

/*
 * Copyright (c) 2003-2006, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * parent class for Action subclasses to be defined for every menu/toolbar
 * option.
 */
public class Action extends AbstractAction {
	private final Consumer<ActionEvent> onPerformed;
	protected VenusUI mainUI;

	protected Action( @NotNull String key, @Nullable String iconPath, Integer mnemonic, KeyStroke accel, VenusUI gui ) {
		this( key, iconPath, mnemonic, accel, gui, null );
	}

	protected Action( @NotNull String key, @Nullable String iconPath, Integer mnemonic, KeyStroke accel, VenusUI gui, Consumer<ActionEvent> onPerformed ) {
		super( loc( "mars.action." + key + ".name" ), iconPath == null ? null : Resources.getIcon( "/images/" + iconPath ) );
		putValue( SHORT_DESCRIPTION, loc( "mars.action." + key + ".description" ) );
		putValue( MNEMONIC_KEY, mnemonic );
		putValue( ACCELERATOR_KEY, accel );
		mainUI = gui;
		this.onPerformed = onPerformed;
	}

	/**
	 * does nothing by default. Should be over-ridden by subclass
	 */
	@Override
	public void actionPerformed( ActionEvent evt ) {
		if ( onPerformed != null )
			onPerformed.accept( evt );
	}
}
