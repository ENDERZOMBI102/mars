package mars.venus;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.WindowConstants;

import mars.Globals;
import themeengine.DemoPrefs;
import themeengine.IJThemesPanel;

import static com.enderzombi102.mars.res.Localization.loc;


public class SettingsThemesAction extends Action {

	JDialog themesDialog;

	protected SettingsThemesAction( final VenusUI gui ) {
		super( "theme_settings", null, null, null, gui );
	}

	/**
	 * When this action is triggered, launch a dialog to view and modify editor
	 * settings.
	 */
	@Override
	public void actionPerformed(final ActionEvent evt) {
		themesDialog = new JDialog(Globals.getGui(), loc( "mars.popup.themes.title" ), true);
		themesDialog.setContentPane(buildDialogPanel());
		themesDialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		themesDialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(final WindowEvent we) {
				closeDialog();
			}
		});
		themesDialog.pack();
		themesDialog.setLocationRelativeTo(Globals.getGui());
		themesDialog.setVisible(true);
	}

	private Container buildDialogPanel() {
		return new IJThemesPanel(DemoPrefs.getLafState());
	}

	// We're finished with this modal dialog.
	private void closeDialog() {
		themesDialog.setVisible(false);
		themesDialog.dispose();
	}
}
