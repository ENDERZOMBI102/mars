package mars.venus.editors.jeditsyntax;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JComponent;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.CompoundEdit;
import javax.swing.undo.UndoManager;

import mars.Globals;
import mars.venus.EditPane;
import mars.venus.editors.MARSTextEditingArea;
import mars.venus.editors.jeditsyntax.tokenmarker.MIPSTokenMarker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Adaptor subclass for JEditTextArea Provides those methods required by the
 * MARSTextEditingArea interface that are not defined by JEditTextArea. This
 * permits JEditTextArea to be used within MARS largely without modification.
 * DPS 4-20-2010
 *
 * @author Pete Sanderson
 * @since 4.0
 */
public class JEditBasedTextArea extends JEditTextArea implements MARSTextEditingArea, CaretListener {
	private static final Logger LOGGER = LoggerFactory.getLogger( JEditBasedTextArea.class );
	private final UndoManager undoManager = new UndoManager();
	private final EditPane editPane;
	private CompoundEdit compoundEdit = new CompoundEdit();
	private boolean isCompoundEdit = false;

	public JEditBasedTextArea( final EditPane editPain, final JComponent lineNumbers ) {
		super( lineNumbers );
		this.editPane = editPain;

		// Needed to support unlimited undo/redo capability
		this.getDocument().addUndoableEditListener( e -> {
			//Remember the edit and update the menus.
			if ( this.isCompoundEdit ) {
				this.compoundEdit.addEdit( e.getEdit() );
			} else {
				this.undoManager.addEdit( e.getEdit() );
				this.editPane.updateUndoState();
				this.editPane.updateRedoState();
			}
		});
		this.setFont( Globals.getSettings().getEditorFont() );
		this.setTokenMarker( new MIPSTokenMarker() );

		this.addCaretListener( this );
	}

	@Override
	public void setFont( final Font f ) {
		this.getPainter().setFont( f );
	}

	@Override
	public Font getFont() {
		return this.getPainter().getFont();
	}

	/**
	 * Use for highlighting the line currently being edited.
	 *
	 * @param highlight true to enable line highlighting, false to disable.
	 */
	@Override
	public void setLineHighlightEnabled( final boolean highlight ) {
		this.getPainter().setLineHighlightEnabled( highlight );
	}

	/**
	 * Set the caret blinking rate in milliseconds. If rate is 0 will disable
	 * blinking. If negative, do nothing.
	 *
	 * @param rate blinking rate in milliseconds
	 */
	@Override
	public void setCaretBlinkRate( final int rate ) {
		if ( rate == 0 ) {
			this.caretBlinks = false;
		}
		if ( rate > 0 ) {
			this.caretBlinks = true;
			this.caretBlinkRate = rate;
			caretTimer.setDelay( rate );
			caretTimer.setInitialDelay( rate );
			caretTimer.restart();
		}
	}

	/**
	 * Set the number of characters a tab will expand to.
	 *
	 * @param chars number of characters
	 */
	@Override
	public void setTabSize( final int chars ) {
		this.painter.setTabSize( chars );
	}

	/**
	 * Update the syntax style table, which is obtained from SyntaxUtilities.
	 */
	@Override
	public void updateSyntaxStyles() {
		this.painter.setStyles( SyntaxUtilities.getCurrentSyntaxStyles() );
	}

	@Override
	public Component getOuterComponent() {
		return this;
	}

	/**
	 * Get rid of any accumulated undoable edits. It is useful to call this method
	 * after opening a file into the text area. The act of setting its text content
	 * upon reading the file will generate an undoable edit. Normally you don't want
	 * a freshly-opened file to appear with its Undo action enabled. But it will
	 * unless you call this after setting the text.
	 */
	@Override
	public void discardAllUndoableEdits() {
		this.undoManager.discardAllEdits();
	}

	/**
	 * Display caret position on the edit pane.
	 *
	 * @param e A CaretEvent
	 */

	@Override
	public void caretUpdate( final CaretEvent e ) {
		this.editPane.displayCaretPosition( e.getDot() );
	}

	/**
	 * Same as setSelectedText but named for compatibility with JTextComponent
	 * method replaceSelection. DPS, 14 Apr 2010
	 *
	 * @param replacementText The replacement text for the selection
	 */
	@Override
	public void replaceSelection( final String replacementText ) {
		this.setSelectedText( replacementText );
	}

	//
	//
	@Override
	public void setSelectionVisible( final boolean vis ) {
	}

	//
	//
	@Override
	public void setSourceCode( final String s, final boolean editable ) {
		this.setText( s );
		this.setBackground( editable ? Color.WHITE : Color.GRAY );
		this.setEditable( editable );
		this.setEnabled( editable );
		this.setCaretPosition( 0 );
		if ( editable ) {
			this.requestFocusInWindow();
		}
	}

	/**
	 * Returns the undo manager for this editing area
	 *
	 * @return the undo manager
	 */
	@Override
	public UndoManager getUndoManager() {
		return this.undoManager;
	}

	/**
	 * Undo previous edit
	 */
	@Override
	public void undo() {
		// "unredoing" is mode used by DocumentHandler's insertUpdate() and removeUpdate()
		// to pleasingly mark the text and location of the undo.
		this.unredoing = true;
		try {
			this.undoManager.undo();
		} catch ( final CannotUndoException ex ) {
			LOGGER.warn( "Unable to undo:", ex );
		}
		this.unredoing = false;
		this.setCaretVisible( true );
	}

	/**
	 * Redo previous edit
	 */
	@Override
	public void redo() {
		// "unredoing" is mode used by DocumentHandler's insertUpdate() and removeUpdate()
		// to pleasingly mark the text and location of the redo.
		this.unredoing = true;
		try {
			this.undoManager.redo();
		} catch ( final CannotRedoException ex ) {
			LOGGER.error( "Unable to redo:", ex );
		}
		this.unredoing = false;
		this.setCaretVisible( true );
	}

	//////////////////////////////////////////////////////////////////////////
	//  Methods to support Find/Replace feature
	//
	// Basis for this Find/Replace solution is:
	// http://java.ittoolbox.com/groups/technical-functional/java-l/search-and-replace-using-jtextpane-630964
	// as written by Chris Dickenson in 2005
	//

	/**
	 * Finds next occurrence of text in a forward search of a string. Search begins
	 * at the current cursor location, and wraps around when the end of the string
	 * is reached.
	 *
	 * @param find          the text to locate in the string
	 * @param caseSensitive true if search is to be case-sensitive, false otherwise
	 * @return TEXT_FOUND or TEXT_NOT_FOUND, depending on the result.
	 */
	@Override
	public int doFindText( final String find, final boolean caseSensitive ) {
		final int findPosn = this.getCaretPosition();
		int nextPosn = nextIndex( getText(), find, findPosn, caseSensitive );
		if ( nextPosn >= 0 ) {
			this.requestFocus(); // guarantees visibility of the blue highlight
			this.setSelectionStart( nextPosn ); // position cursor at word start
			this.setSelectionEnd( nextPosn + find.length() );
			// Need to RepeatActionListener start due to quirk in JEditTextArea implementation of setSelectionStart.
			this.setSelectionStart( nextPosn );
			return TEXT_FOUND;
		}
		return TEXT_NOT_FOUND;
	}

	/**
	 * Returns next posn of word in text - forward search. If end of string is
	 * reached during the search, will wrap around to the beginning one time.
	 *
	 * @param input         the string to search
	 * @param find          the string to find
	 * @param start         the character position to start the search
	 * @param caseSensitive true for case sensitive. false to ignore case
	 * @return next indexed position of found text or -1 if not found
	 */
	public int nextIndex( final String input, final String find, final int start, final boolean caseSensitive ) {
		int textPosn = -1;
		if ( input != null && find != null && start < input.length() ) {
			if ( caseSensitive ) { // indexOf() returns -1 if not found
				textPosn = input.indexOf( find, start );
				// If not found from non-starting cursor position, wrap around
				if ( start > 0 && textPosn < 0 ) {
					textPosn = input.indexOf( find );
				}
			} else {
				final String lowerCaseText = input.toLowerCase();
				textPosn = lowerCaseText.indexOf( find.toLowerCase(), start );
				// If not found from non-starting cursor position, wrap around
				if ( start > 0 && textPosn < 0 ) {
					textPosn = lowerCaseText.indexOf( find.toLowerCase() );
				}
			}
		}
		return textPosn;
	}

	/**
	 * Finds and replaces next occurrence of text in a string in a forward search.
	 * If cursor is initially at end of matching selection, will immediately replace
	 * then find and select the next occurrence if any. Otherwise it performs a find
	 * operation. The replace can be undone with one undo operation.
	 *
	 * @param find          the text to locate in the string
	 * @param replace       the text to replace the find text with - if the find text
	 *                      exists
	 * @param caseSensitive true for case sensitive. false to ignore case
	 * @return Returns TEXT_FOUND if not initially at end of selected match and
	 * matching occurrence is found. Returns TEXT_NOT_FOUND if the text is not
	 * matched. Returns TEXT_REPLACED_NOT_FOUND_NEXT if replacement is successful
	 * but there are no additional matches. Returns TEXT_REPLACED_FOUND_NEXT if
	 * reaplacement is successful and there is at least one additional match.
	 */
	@Override
	public int doReplace( final String find, final String replace, final boolean caseSensitive ) {
		int nextPosn;
		// Will perform a "find" and return, unless positioned at the end of
		// a selected "find" result.
		if ( find == null || !find.equals( this.getSelectedText() ) || this.getSelectionEnd() != this.getCaretPosition() ) {
			return doFindText( find, caseSensitive );
		}
		// We are positioned at end of selected "find".  Rreplace and find next.
		nextPosn = this.getSelectionStart();
		this.grabFocus();
		this.setSelectionStart( nextPosn ); // posn cursor at word start
		this.setSelectionEnd( nextPosn + find.length() ); //select found text
		// Need to RepeatActionListener start due to quirk in JEditTextArea implementation of setSelectionStart.
		this.setSelectionStart( nextPosn );
		this.isCompoundEdit = true;
		this.compoundEdit = new CompoundEdit();
		this.replaceSelection( replace );
		this.compoundEdit.end();
		this.undoManager.addEdit( this.compoundEdit );
		this.editPane.updateUndoState();
		this.editPane.updateRedoState();
		this.isCompoundEdit = false;
		this.setCaretPosition( nextPosn + replace.length() );
		if ( doFindText( find, caseSensitive ) == TEXT_NOT_FOUND ) {
			return TEXT_REPLACED_NOT_FOUND_NEXT;
		}
		return TEXT_REPLACED_FOUND_NEXT;
	}

	/**
	 * Finds and replaces <B>ALL</B> occurrences of text in a string in a forward
	 * search. All replacements are bundled into one CompoundEdit, so one Undo
	 * operation will undo all of them.
	 *
	 * @param find          the text to locate in the string
	 * @param replace       the text to replace the find text with - if the find text
	 *                      exists
	 * @param caseSensitive true for case sensitive. false to ignore case
	 * @return the number of occurrences that were matched and replaced.
	 */
	@Override
	public int doReplaceAll( final String find, final String replace, final boolean caseSensitive ) {
		int nextPosn = 0;
		int findPosn = 0; // *** begin at start of text
		int replaceCount = 0;
		this.compoundEdit = null; // new one will be created upon first replacement
		this.isCompoundEdit = true; // undo manager's action listener needs this
		while ( nextPosn >= 0 ) {
			nextPosn = nextIndex( this.getText(), find, findPosn, caseSensitive );
			if ( nextPosn >= 0 ) {
				// nextIndex() will wrap around, which causes infinite loop if
				// find string is a substring of replacement string.  This
				// statement will prevent that.
				if ( nextPosn < findPosn ) {
					break;
				}
				this.grabFocus();
				this.setSelectionStart( nextPosn ); // posn cursor at word start
				this.setSelectionEnd( nextPosn + find.length() ); //select found text
				// Need to RepeatActionListener start due to quirk in JEditTextArea implementation of setSelectionStart.
				this.setSelectionStart( nextPosn );
				if ( this.compoundEdit == null ) {
					this.compoundEdit = new CompoundEdit();
				}
				this.replaceSelection( replace );
				findPosn = nextPosn + replace.length(); // set for next search
				replaceCount++;
			}
		}
		this.isCompoundEdit = false;
		// Will be true if any replacements were performed
		if ( this.compoundEdit != null ) {
			this.compoundEdit.end();
			this.undoManager.addEdit( this.compoundEdit );
			this.editPane.updateUndoState();
			this.editPane.updateRedoState();
		}
		return replaceCount;
	}
	//

	/// //////////////////////////  End Find/Replace methods //////////////////////////

	@Override
	public void updateColors() {
		this.painter.updateColors();
	}

	//
	//////////////////////////////////////////////////////////////////

}
