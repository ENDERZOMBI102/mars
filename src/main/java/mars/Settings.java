package mars;

import com.enderzombi102.mars.event.Event;
import com.enderzombi102.mars.event.EventDispatcher;
import com.enderzombi102.mars.util.Observable;
import com.enderzombi102.mars.res.SalvableProperties;
import mars.util.Binary;
import mars.util.EditorFont;
import mars.util.Utility;
import mars.venus.editors.jeditsyntax.SyntaxStyle;
import mars.venus.editors.jeditsyntax.SyntaxUtilities;
import mars.venus.editors.jeditsyntax.tokenmarker.Token;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/*
 * Copyright (c) 2003-2013, Pete Sanderson and Kenneth Vollmar
 *
 * Developed by Pete Sanderson (psanderson@otterbein.edu) and Kenneth Vollmar
 * (kenvollmar@missouristate.edu)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * (MIT license, http://www.opensource.org/licenses/mit-license.html)
 */

/**
 * Contains various IDE settings. Persistent settings are maintained for the
 * current user and on the current machine using Java's Preference objects.
 * Failing that, default setting values come from Settings.properties file. If
 * both of those fail, default values come from static arrays defined in this
 * class. The latter can can be modified prior to instantiating Settings object.
 * NOTE: If the Preference objects fail due to security exceptions, changes to
 * settings will not carry over from one MARS session to the next.
 * <br/>
 * FIXME: De-clusterfuck this
 *
 * @author Pete Sanderson
 **/
public class Settings extends Observable<Settings> {
	public static final EventDispatcher<BooleanChangedEvent> BOOLEAN_CHANGED = EventDispatcher.create( BooleanChangedEvent.class );
	public static final EventDispatcher<FontChangedEvent> FONT_CHANGED = EventDispatcher.create( FontChangedEvent.class );

	// BOOLEAN SETTINGS
	/**
	 * Flag to determine whether the program being assembled is limited to basic
	 * MIPS instructions and formats.
	 */
	public static final String EXTENDED_ASSEMBLER_ENABLED = "ExtendedAssembler";
	/**
	 * Flag to determine whether the program being assembled is limited to using
	 * register numbers instead of names. NOTE: Its default value is false and the
	 * IDE provides no means to change it!
	 */
	public static final String BARE_MACHINE_ENABLED = "BareMachine";
	/**
	 * Flag to determine whether a file is immediately and automatically
	 * assembled upon opening. Handy when using external editor like mipster.
	 */
	public static final String ASSEMBLE_ON_OPEN_ENABLED = "AssembleOnOpen";
	/**
	 * Flag to determine whether only the current editor source file (enabled false)
	 * or all files in its directory (enabled true) will be assembled when assembly
	 * is selected.
	 */
	public static final String ASSEMBLE_ALL_ENABLED = "AssembleAll";
	/**
	 * Default visibilty of label window (symbol table). Default only, dynamic
	 * status maintained by ExecutePane
	 */
	public static final String LABEL_WINDOW_VISIBILITY = "LabelWindowVisibility";
	/**
	 * Default setting for displaying addresses and values in hexidecimal in the
	 * Execute pane.
	 */
	public static final String DISPLAY_ADDRESSES_IN_HEX = "DisplayAddressesInHex";
	public static final String DISPLAY_VALUES_IN_HEX = "DisplayValuesInHex";
	/**
	 * Flag to determine whether the currently selected exception handler source
	 * file will be included in each assembly operation.
	 */
	public static final String EXCEPTION_HANDLER_ENABLED = "ExceptionHandler";
	/**
	 * Flag to determine whether or not delayed branching is in effect at MIPS
	 * execution. This means we simulate the pipeline and statement FOLLOWING a
	 * successful branch is executed before branch is taken. DPS 14 June 2007.
	 */
	public static final String DELAYED_BRANCHING_ENABLED = "DelayedBranching";
	/**
	 * Flag to determine whether or not the editor will display line numbers.
	 */
	public static final String EDITOR_LINE_NUMBERS_DISPLAYED = "EditorLineNumbersDisplayed";
	/**
	 * Flag to determine whether or not assembler warnings are considered errors.
	 */
	public static final String WARNINGS_ARE_ERRORS = "WarningsAreErrors";
	/**
	 * Flag to determine whether or not to display and use program arguments
	 */
	public static final String PROGRAM_ARGUMENTS = "ProgramArguments";
	/**
	 * Flag to control whether or not highlighting is applied to data segment window
	 */
	public static final String DATA_SEGMENT_HIGHLIGHTING = "DataSegmentHighlighting";
	/**
	 * Flag to control whether or not highlighting is applied to register windows
	 */
	public static final String REGISTERS_HIGHLIGHTING = "RegistersHighlighting";
	/**
	 * Flag to control whether or not assembler automatically initializes program
	 * counter to 'main's address
	 */
	public static final String START_AT_MAIN = "StartAtMain";
	/**
	 * Flag to control whether or not editor will highlight the line currently being
	 * edited
	 */
	public static final String EDITOR_CURRENT_LINE_HIGHLIGHTING = "EditorCurrentLineHighlighting";
	/**
	 * Flag to control whether or not editor will provide popup instruction guidance
	 * while typing
	 */
	public static final String POPUP_INSTRUCTION_GUIDANCE = "PopupInstructionGuidance";
	/**
	 * Flag to control whether or not simulator will use popup dialog for input
	 * syscalls
	 */
	public static final String POPUP_SYSCALL_INPUT = "PopupSyscallInput";
	/**
	 * Flag to control whether or not to use generic text editor instead of
	 * language-aware styled editor
	 */
	public static final String GENERIC_TEXT_EDITOR = "GenericTextEditor";
	/**
	 * Flag to control whether or not language-aware editor will use auto-indent
	 * feature
	 */
	public static final String AUTO_INDENT = "AutoIndent";
	/**
	 * Flag to determine whether a program can write binary code to the text or data
	 * segment and execute that code.
	 */
	public static final String SELF_MODIFYING_CODE_ENABLED = "SelfModifyingCode";

	private static final Map<String, Boolean> DEFAULT_BOOLEAN_SETTINGS = Map.ofEntries(
		Map.entry( "ExtendedAssembler", true ),
		Map.entry( "BareMachine", false ),
		Map.entry( "AssembleOnOpen", false ),
		Map.entry( "AssembleAll", false ),
		Map.entry( "LabelWindowVisibility", false ),
		Map.entry( "DisplayAddressesInHex", true ),
		Map.entry( "DisplayValuesInHex", true ),
		Map.entry( "LoadExceptionHandler", false ),
		Map.entry( "DelayedBranching", false ),
		Map.entry( "EditorLineNumbersDisplayed", true ),
		Map.entry( "WarningsAreErrors", false ),
		Map.entry( "ProgramArguments", false ),
		Map.entry( "DataSegmentHighlighting", true ),
		Map.entry( "RegistersHighlighting", true ),
		Map.entry( "StartAtMain", true ),
		Map.entry( "EditorCurrentLineHighlighting", true ),
		Map.entry( "PopupInstructionGuidance", true ),
		Map.entry( "PopupSyscallInput", false ),
		Map.entry( "GenericTextEditor", false ),
		Map.entry( "AutoIndent", true ),
		Map.entry( "SelfModifyingCode", false ),
		Map.entry( "ExceptionHandler", false )
	);

	// STRING SETTINGS
	/**
	 * Current specified exception handler file (a MIPS assembly source file)
	 */
	public static final String EXCEPTION_HANDLER = "ExceptionHandler";
	/**
	 * Order of text segment table columns
	 */
	public static final String TEXT_COLUMN_ORDER = "TextColumnOrder";
	/**
	 * Identifier of current memory configuration
	 */
	public static final String MEMORY_CONFIGURATION = "MemoryConfiguration";
	/**
	 * Caret blink rate in milliseconds, 0 means don't blink.
	 */
	public static final String CARET_BLINK_RATE = "CaretBlinkRate";
	/**
	 * Editor tab size in characters.
	 */
	public static final String EDITOR_TAB_SIZE = "EditorTabSize";
	/**
	 * Number of letters to be matched by editor's instruction guide before popup
	 * generated (if popup enabled)
	 */
	public static final String EDITOR_POPUP_PREFIX_LENGTH = "EditorPopupPrefixLength";

	private static final Map<String, String> DEFAULT_STRING_SETTINGS = Map.ofEntries(
		Map.entry( "ExceptionHandler", "" ),
		Map.entry( "TextColumnOrder", "0 1 2 3 4" ),
		Map.entry( "LabelSortState", "BY_NAME" ),
		Map.entry( "MemoryConfiguration", "" ),
		Map.entry( "CaretBlinkRate", "500" ),
		Map.entry( "EditorTabSize", "4" ),
		Map.entry( "EditorPopupPrefixLength", "2" ),
		Map.entry( "LastOpenedFileDirectory", "." ),
		Map.entry( "Language", "en_us" )
	);

	// FONT SETTINGS
	/**
	 * Font for the text editor
	 */
	public static final int EDITOR_FONT = 0;
	/**
	 * Font for table even row background (text, data, register displays)
	 */
	public static final int EVEN_ROW_FONT = 1;
	/**
	 * Font for table odd row background (text, data, register displays)
	 */
	public static final int ODD_ROW_FONT = 2;
	/**
	 * Font for table odd row foreground (text, data, register displays)
	 */
	public static final int TEXTSEGMENT_HIGHLIGHT_FONT = 3;
	/**
	 * Font for text segment delay slot highlighted background
	 */
	public static final int TEXTSEGMENT_DELAYSLOT_HIGHLIGHT_FONT = 4;
	/**
	 * Font for text segment highlighted background
	 */
	public static final int DATASEGMENT_HIGHLIGHT_FONT = 5;
	/**
	 * Font for register highlighted background
	 */
	public static final int REGISTER_HIGHLIGHT_FONT = 6;

	private static final String[] fontFamilySettingsKeys = { "EditorFontFamily", "EvenRowFontFamily", "OddRowFontFamily", " TextSegmentHighlightFontFamily", "TextSegmentDelayslotHighightFontFamily", "DataSegmentHighlightFontFamily", "RegisterHighlightFontFamily" };
	private static final String[] fontStyleSettingsKeys = { "EditorFontStyle", "EvenRowFontStyle", "OddRowFontStyle", " TextSegmentHighlightFontStyle", "TextSegmentDelayslotHighightFontStyle", "DataSegmentHighlightFontStyle", "RegisterHighlightFontStyle" };
	private static final String[] fontSizeSettingsKeys = { "EditorFontSize", "EvenRowFontSize", "OddRowFontSize", " TextSegmentHighlightFontSize", "TextSegmentDelayslotHighightFontSize", "DataSegmentHighlightFontSize", "RegisterHighlightFontSize" };

	/**
	 * Last resort default values for Font settings; will use only if neither the
	 * Preferences nor the properties file work. If you wish to change, do so before
	 * instantiating the Settings object. Must match key by list position shown
	 * above.
	 */

	// DPS 3-Oct-2012
	// Changed default font family from "Courier New" to "Monospaced" after receiving reports that Mac were not
	// correctly rendering the left parenthesis character in the editor or text segment display.
	// See http://www.mirthcorp.com/community/issues/browse/MIRTH-1921?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel
	private static final String[] defaultFontFamilySettingsValues = { "Monospaced", "Monospaced", "Monospaced", "Monospaced", "Monospaced", "Monospaced", "Monospaced" };
	private static final String[] defaultFontStyleSettingsValues = { "Plain", "Plain", "Plain", "Plain", "Plain", "Plain", "Plain" };
	private static final String[] defaultFontSizeSettingsValues = { "12", "12", "12", "12", "12", "12", "12", };

	// COLOR SETTINGS.  Each array position has associated name.
	/**
	 * RGB color for table even row background (text, data, register displays)
	 */
	public static final int EVEN_ROW_BACKGROUND = 0;
	/**
	 * RGB color for table even row foreground (text, data, register displays)
	 */
	public static final int EVEN_ROW_FOREGROUND = 1;
	/**
	 * RGB color for table odd row background (text, data, register displays)
	 */
	public static final int ODD_ROW_BACKGROUND = 2;
	/**
	 * RGB color for table odd row foreground (text, data, register displays)
	 */
	public static final int ODD_ROW_FOREGROUND = 3;
	/**
	 * RGB color for text segment highlighted background
	 */
	public static final int TEXTSEGMENT_HIGHLIGHT_BACKGROUND = 4;
	/**
	 * RGB color for text segment highlighted foreground
	 */
	public static final int TEXTSEGMENT_HIGHLIGHT_FOREGROUND = 5;
	/**
	 * RGB color for text segment delay slot highlighted background
	 */
	public static final int TEXTSEGMENT_DELAYSLOT_HIGHLIGHT_BACKGROUND = 6;
	/**
	 * RGB color for text segment delay slot highlighted foreground
	 */
	public static final int TEXTSEGMENT_DELAYSLOT_HIGHLIGHT_FOREGROUND = 7;
	/**
	 * RGB color for text segment highlighted background
	 */
	public static final int DATASEGMENT_HIGHLIGHT_BACKGROUND = 8;
	/**
	 * RGB color for text segment highlighted foreground
	 */
	public static final int DATASEGMENT_HIGHLIGHT_FOREGROUND = 9;
	/**
	 * RGB color for register highlighted background
	 */
	public static final int REGISTER_HIGHLIGHT_BACKGROUND = 10;
	/**
	 * RGB color for register highlighted foreground
	 */
	public static final int REGISTER_HIGHLIGHT_FOREGROUND = 11;
	// Match the above by position.
	private static final String[] colorSettingsKeys = { "EvenRowBackground", "EvenRowForeground", "OddRowBackground", "OddRowForeground", "TextSegmentHighlightBackground", "TextSegmentHighlightForeground", "TextSegmentDelaySlotHighlightBackground", "TextSegmentDelaySlotHighlightForeground", "DataSegmentHighlightBackground", "DataSegmentHighlightForeground", "RegisterHighlightBackground", "RegisterHighlightForeground" };
	/**
	 * Last resort default values for color settings; will use only if neither the
	 * Preferences nor the properties file work. If you wish to change, do so before
	 * instantiating the Settings object. Must match key by list position.
	 */
	private static final String[] defaultColorSettingsValues = { "0x00e0e0e0", "0", "0x00ffffff", "0", "0x00ffff99", "0", "0x0033ff00", "0", "0x0099ccff", "0", "0x0099cc55", "0" };
	private static final String DEFAULT_CONFIG_FILE = "Settings";
	private static final Logger LOGGER = LoggerFactory.getLogger( "Settings" );

	private final Map<String, Boolean> booleanSettingsValues;
	private final Map<String, String> stringSettingsValues;
	private final String[] fontFamilySettingsValues;
	private final String[] fontStyleSettingsValues;
	private final String[] fontSizeSettingsValues;
	private final String[] colorSettingsValues;

	private final SalvableProperties preferences;


	/**
	 * Create Settings object and set to saved values. If saved values not found,
	 * will set based on defaults stored in Settings.properties file. If file
	 * problems, will set based on defaults stored in this class.
	 */
	public Settings() {
		booleanSettingsValues = new HashMap<>();
		stringSettingsValues = new HashMap<>();
		fontFamilySettingsValues = new String[fontFamilySettingsKeys.length];
		fontStyleSettingsValues = new String[fontStyleSettingsKeys.length];
		fontSizeSettingsValues = new String[fontSizeSettingsKeys.length];
		colorSettingsValues = new String[colorSettingsKeys.length];
		preferences = new SalvableProperties( System.getProperty( "user.home" ) + "/mars.cfg" );
		initialize();
	}

	/**
	 * Return whether backstepping is permitted at this time. Backstepping is
	 * ability to undo execution steps one at a time. Available only in the IDE.
	 * This is not a persistent setting and is not under MARS user control.
	 *
	 * @return true if backstepping is permitted, false otherwise.
	 */
	public boolean getBackSteppingEnabled() {
		return Globals.program != null && Globals.program.getBackStepper() != null && Globals.program.getBackStepper().enabled();
	}

	/**
	 * Reset settings to default values, as described in the constructor comments.
	 * <p>
	 * mode. Ignored as of release 3.6 but retained for compatibility.
	 */
	public void reset() {
		initialize();
	}

	/* **************************************************************************
	This section contains all code related to syntax highlighting styles settings.
	A style includes 3 components: color, bold (t/f), italic (t/f)
	
	The fallback defaults will come not from an array here, but from the
	existing static method SyntaxUtilities.getDefaultSyntaxStyles()
	in the mars.venus.editors.jeditsyntax package.  It returns an array
	of SyntaxStyle objects.
	
	*/
	private String[] syntaxStyleColorSettingsValues;
	private boolean[] syntaxStyleBoldSettingsValues;
	private boolean[] syntaxStyleItalicSettingsValues;

	private static final String SYNTAX_STYLE_COLOR_PREFIX = "SyntaxStyleColor_";
	private static final String SYNTAX_STYLE_BOLD_PREFIX = "SyntaxStyleBold_";
	private static final String SYNTAX_STYLE_ITALIC_PREFIX = "SyntaxStyleItalic_";

	private static String[] syntaxStyleColorSettingsKeys, syntaxStyleBoldSettingsKeys, syntaxStyleItalicSettingsKeys;
	private static String[] defaultSyntaxStyleColorSettingsValues;
	private static boolean[] defaultSyntaxStyleBoldSettingsValues;
	private static boolean[] defaultSyntaxStyleItalicSettingsValues;

	public void setEditorSyntaxStyleByPosition( final int index, final SyntaxStyle syntaxStyle ) {
		syntaxStyleColorSettingsValues[index] = syntaxStyle.getColorAsHexString();
		syntaxStyleItalicSettingsValues[index] = syntaxStyle.isItalic();
		syntaxStyleBoldSettingsValues[index] = syntaxStyle.isBold();
		saveEditorSyntaxStyle( index );
	}

	public SyntaxStyle getEditorSyntaxStyleByPosition( final int index ) {
		return new SyntaxStyle(
			getSyntaxColorValueByPosition( index, syntaxStyleColorSettingsValues ),
			syntaxStyleItalicSettingsValues[index],
			syntaxStyleBoldSettingsValues[index] );
	}

	public SyntaxStyle getDefaultEditorSyntaxStyleByPosition( final int index ) {
		return new SyntaxStyle(
			getSyntaxColorValueByPosition( index, defaultSyntaxStyleColorSettingsValues ),
			defaultSyntaxStyleItalicSettingsValues[index],
			defaultSyntaxStyleBoldSettingsValues[index] );
	}

	private void saveEditorSyntaxStyle( final int index ) {
		try {
			preferences.put( syntaxStyleColorSettingsKeys[index], syntaxStyleColorSettingsValues[index] );
			preferences.putBoolean( syntaxStyleBoldSettingsKeys[index], syntaxStyleBoldSettingsValues[index] );
			preferences.putBoolean( syntaxStyleItalicSettingsKeys[index], syntaxStyleItalicSettingsValues[index] );
			preferences.save();
		} catch ( IOException e ) {
			e.printStackTrace();
		}
	}

	// For syntax styles, need to initialize from SyntaxUtilities defaults.
	// Taking care not to explicitly create a Color object, since it may trigger
	// Swing initialization (that caused problems for UC Berkeley when we
	// created Font objects here).  It shouldn't, but then again Font shouldn't
	// either but they said it did.  (see HeadlessException)
	// On othe other hand, the first statement of this method causes Color objects
	// to be created!  It is possible but a real pain in the rear to avoid using
	// Color objects totally.  Requires new methods for the SyntaxUtilities class.
	private void initializeEditorSyntaxStyles() {
		final SyntaxStyle[] syntaxStyle = SyntaxUtilities.getDefaultSyntaxStyles();
		final int tokens = syntaxStyle.length;
		syntaxStyleColorSettingsKeys = new String[tokens];
		syntaxStyleBoldSettingsKeys = new String[tokens];
		syntaxStyleItalicSettingsKeys = new String[tokens];
		defaultSyntaxStyleColorSettingsValues = new String[tokens];
		defaultSyntaxStyleBoldSettingsValues = new boolean[tokens];
		defaultSyntaxStyleItalicSettingsValues = new boolean[tokens];
		syntaxStyleColorSettingsValues = new String[tokens];
		syntaxStyleBoldSettingsValues = new boolean[tokens];
		syntaxStyleItalicSettingsValues = new boolean[tokens];
		for ( int i = 0; i < tokens; i++ ) {
			syntaxStyleColorSettingsKeys[i] = SYNTAX_STYLE_COLOR_PREFIX + i;
			syntaxStyleBoldSettingsKeys[i] = SYNTAX_STYLE_BOLD_PREFIX + i;
			syntaxStyleItalicSettingsKeys[i] = SYNTAX_STYLE_ITALIC_PREFIX + i;
			syntaxStyleColorSettingsValues[i] = defaultSyntaxStyleColorSettingsValues[i] = syntaxStyle[i].getColorAsHexString();
			syntaxStyleBoldSettingsValues[i] = defaultSyntaxStyleBoldSettingsValues[i] = syntaxStyle[i].isBold();
			syntaxStyleItalicSettingsValues[i] = defaultSyntaxStyleItalicSettingsValues[i] = syntaxStyle[i].isItalic();
		}
	}

	private void getEditorSyntaxStyleSettingsFromPreferences() {
		for ( int i = 0; i < syntaxStyleColorSettingsKeys.length; i++ ) {
			syntaxStyleColorSettingsValues[i] = preferences.get(
				syntaxStyleColorSettingsKeys[i],
				syntaxStyleColorSettingsValues[i]
			);
			syntaxStyleBoldSettingsValues[i] = preferences.getBoolean(
				syntaxStyleBoldSettingsKeys[i],
				syntaxStyleBoldSettingsValues[i]
			);
			syntaxStyleItalicSettingsValues[i] = preferences.getBoolean(
				syntaxStyleItalicSettingsKeys[i],
				syntaxStyleItalicSettingsValues[i]
			);
		}
	}
	// *********************************************************************************

	////////////////////////////////////////////////////////////////////////
	//  Setting Getters
	////////////////////////////////////////////////////////////////////////

	/**
	 * Fetch value of a boolean setting given its identifier.
	 *
	 * @param key containing the setting's identifier (constants listed above)
	 * @return corresponding boolean setting.
	 * @throws IllegalArgumentException if identifier is invalid.
	 */
	public boolean getBooleanSetting( final String key ) {
		if ( DEFAULT_BOOLEAN_SETTINGS.containsKey( key ) ) {
			return booleanSettingsValues.computeIfAbsent( key, _key -> DEFAULT_BOOLEAN_SETTINGS.get( key ) );
		} else {
			throw new IllegalArgumentException( "Invalid boolean setting ID: " + key );
		}
	}

	/**
	 * Name of currently selected exception handler file.
	 *
	 * @return String pathname of current exception handler file, empty if none.
	 */
	public String getExceptionHandler() {
		return stringSettingsValues.get( EXCEPTION_HANDLER );
	}

	/**
	 * Returns identifier of current built-in memory configuration.
	 *
	 * @return String identifier of current built-in memory configuration, empty if
	 * none.
	 */
	public String getMemoryConfiguration() {
		return stringSettingsValues.get( MEMORY_CONFIGURATION );
	}

	/**
	 * Current editor font. Retained for compatibility but replaced by:
	 * getFontByPosition(Settings.EDITOR_FONT)
	 *
	 * @return Font object for current editor font.
	 */
	public Font getEditorFont() {
		return getFontByPosition( EDITOR_FONT );
	}

	/**
	 * Retrieve a Font setting
	 *
	 * @param fontSettingPosition constant that identifies which item
	 * @return Font object for given item
	 */
	public Font getFontByPosition( final int fontSettingPosition ) {
		if ( fontSettingPosition >= 0 && fontSettingPosition < fontFamilySettingsValues.length ) {
			return EditorFont.createFontFromStringValues(
				fontFamilySettingsValues[fontSettingPosition],
				fontStyleSettingsValues[fontSettingPosition],
				fontSizeSettingsValues[fontSettingPosition] );
		} else {
			return null;
		}
	}

	/**
	 * Retrieve a default Font setting
	 *
	 * @param fontSettingPosition constant that identifies which item
	 * @return Font object for given item
	 */
	public Font getDefaultFontByPosition( final int fontSettingPosition ) {
		if ( fontSettingPosition >= 0 && fontSettingPosition < defaultFontFamilySettingsValues.length ) {
			return EditorFont.createFontFromStringValues(
				defaultFontFamilySettingsValues[fontSettingPosition],
				defaultFontStyleSettingsValues[fontSettingPosition],
				defaultFontSizeSettingsValues[fontSettingPosition]
			);
		} else {
			return null;
		}
	}

	/**
	 * Order of text segment display columns (there are 5, numbered 0 to 4).
	 *
	 * @return Array of int indicating the order. Original order is 0 1 2 3 4.
	 */
	public int[] getTextColumnOrder() {
		return getTextSegmentColumnOrder( stringSettingsValues.get( TEXT_COLUMN_ORDER ) );
	}

	/**
	 * Retrieve the caret blink rate in milliseconds. Blink rate of 0 means do not
	 * blink.
	 *
	 * @return int blink rate in milliseconds
	 */

	public int getCaretBlinkRate() {
		try {
			return Integer.parseInt( stringSettingsValues.get( CARET_BLINK_RATE ) );
		} catch ( final NumberFormatException nfe ) {
			return Integer.parseInt( DEFAULT_STRING_SETTINGS.get( CARET_BLINK_RATE ) );
		}
	}

	/**
	 * Get the tab size in characters.
	 *
	 * @return tab size in characters.
	 */
	public int getEditorTabSize() {
		try {
			return Integer.parseInt( stringSettingsValues.get( EDITOR_TAB_SIZE ) );
		} catch ( final NumberFormatException nfe ) {
			return getDefaultEditorTabSize();
		}
	}

	/**
	 * Get number of letters to be matched by editor's instruction guide before
	 * popup generated (if popup enabled). Should be 1 or 2. If 1, the popup will be
	 * generated after first letter typed, based on all matches; if 2, the popup
	 * will be generated after second letter typed.
	 *
	 * @return number of letters (should be 1 or 2).
	 */
	public int getEditorPopupPrefixLength() {
		int length = 2;
		try {
			length = Integer.parseInt( stringSettingsValues.get( EDITOR_POPUP_PREFIX_LENGTH ) );
		} catch ( final NumberFormatException ignored ) {
		}
		return length;
	}

	/**
	 * Get the text editor default tab size in characters
	 *
	 * @return tab size in characters
	 */
	public int getDefaultEditorTabSize() {
		return Integer.parseInt( DEFAULT_STRING_SETTINGS.get( EDITOR_TAB_SIZE ) );
	}

	/**
	 * Get Color object for specified settings key. Returns null if key is not found
	 * or its value is not a valid color encoding.
	 *
	 * @param key the Setting key
	 * @return corresponding Color, or null if key not found or value not valid
	 * color
	 */
	public Color getColorSettingByKey( final String key ) {
		return getColorValueByKey( key, colorSettingsValues );
	}

	/**
	 * Get default Color value for specified settings key. Returns null if key is
	 * not found or its value is not a valid color encoding.
	 *
	 * @param key the Setting key
	 * @return corresponding default Color, or null if key not found or value not
	 * valid color
	 */
	public Color getDefaultColorSettingByKey( final String key ) {
		return getColorValueByKey( key, defaultColorSettingsValues );
	}

	/**
	 * Get Color object for specified settings name (a static constant). Returns
	 * null if argument invalid or its value is not a valid color encoding.
	 *
	 * @param position the Setting name (see list of static constants)
	 * @return corresponding Color, or null if argument invalid or value not valid
	 * color
	 */
	public Color getColorSettingByPosition( final int position ) {
		return getColorValueByPosition( position, colorSettingsValues );
	}

	/**
	 * Get default Color object for specified settings name (a static constant).
	 * Returns null if argument invalid or its value is not a valid color encoding.
	 *
	 * @param position the Setting name (see list of static constants)
	 * @return corresponding default Color, or null if argument invalid or value not
	 * valid color
	 */
	public Color getDefaultColorSettingByPosition( final int position ) {
		return getColorValueByPosition( position, defaultColorSettingsValues );
	}

	////////////////////////////////////////////////////////////////////////
	//  Setting Setters
	////////////////////////////////////////////////////////////////////////

	/**
	 * Set value of a boolean setting given its id and the value.
	 *
	 * @param key   int containing the setting's identifier (constants listed above)
	 * @param value boolean value to store
	 * @throws IllegalArgumentException if identifier is not valid.
	 */
	public void setBooleanSetting( final String key, final Boolean value ) {
		if ( DEFAULT_BOOLEAN_SETTINGS.containsKey( key ) ) {
			final var old = booleanSettingsValues.get( key );
			if ( old != value ) {
				booleanSettingsValues.put( key, value );
				saveBooleanSetting( key );
				BOOLEAN_CHANGED.invoke( new BooleanChangedEvent( key, old, value ) );
			}
		} else {
			throw new IllegalArgumentException( "Invalid boolean setting ID" );
		}
	}

	/**
	 * Temporarily establish boolean setting. This setting will NOT be written to
	 * persisent store! Currently this is used only when running MARS from the
	 * command line
	 *
	 * @param key   setting identifier. These are defined for this class as static final int.
	 * @param value True to enable the setting, false otherwise.
	 */
	public void setBooleanSettingNonPersistent( final String key, final Boolean value ) {
		if ( DEFAULT_BOOLEAN_SETTINGS.containsKey( key ) ) {
			booleanSettingsValues.put( key, value );
		} else {
			throw new IllegalArgumentException( "Invalid boolean setting ID" );
		}
	}

	/**
	 * Set name of exception handler file and write it to persistent storage.
	 *
	 * @param newFilename name of exception handler file
	 */
	public void setExceptionHandler( final String newFilename ) {
		setStringSetting( EXCEPTION_HANDLER, newFilename );
	}

	/**
	 * Store the identifier of the memory configuration.
	 *
	 * @param config A string that identifies the current built-in memory
	 *               configuration
	 */
	public void setMemoryConfiguration( final String config ) {
		setStringSetting( MEMORY_CONFIGURATION, config );
	}

	/**
	 * Set the caret blinking rate in milliseconds. Rate of 0 means no blinking.
	 *
	 * @param rate blink rate in milliseconds
	 */
	public void setCaretBlinkRate( final int rate ) {
		setStringSetting( CARET_BLINK_RATE, "" + rate );
	}

	/**
	 * Set the tab size in characters.
	 *
	 * @param size tab size in characters.
	 */
	public void setEditorTabSize( final int size ) {
		setStringSetting(
			EDITOR_TAB_SIZE,
			"" + size
		);
	}

	/**
	 * Set number of letters to be matched by editor's instruction guide before
	 * popup generated (if popup enabled). Should be 1 or 2. If 1, the popup will be
	 * generated after first letter typed, based on all matches; if 2, the popup
	 * will be generated after second letter typed.
	 *
	 * @param length of letters (should be 1 or 2).
	 */
	public void setEditorPopupPrefixLength( final int length ) {
		setStringSetting(
			EDITOR_POPUP_PREFIX_LENGTH,
			"" + length
		);
	}

	/**
	 * Set editor font to the specified Font object and write it to persistent
	 * storage. This method retained for compatibility but replaced by:
	 * setFontByPosition(Settings.EDITOR_FONT, font)
	 *
	 * @param font Font object to be used by text editor.
	 */
	public void setEditorFont( final Font font ) {
		setFontByPosition( EDITOR_FONT, font );
	}

	/**
	 * Store a Font setting
	 *
	 * @param fontSettingPosition Constant that identifies the item the font goes
	 *                            with
	 * @param font                The font to set that item to
	 */
	public void setFontByPosition( final int fontSettingPosition, final Font font ) {
		if ( fontSettingPosition >= 0 && fontSettingPosition < fontFamilySettingsValues.length ) {
			fontFamilySettingsValues[fontSettingPosition] = font.getFamily();
			fontStyleSettingsValues[fontSettingPosition] = EditorFont.styleIntToStyleString( font.getStyle() );
			fontSizeSettingsValues[fontSettingPosition] = EditorFont.sizeIntToSizeString( font.getSize() );
			saveFontSetting( fontSettingPosition, fontFamilySettingsKeys, fontFamilySettingsValues );
			saveFontSetting( fontSettingPosition, fontStyleSettingsKeys, fontStyleSettingsValues );
			saveFontSetting( fontSettingPosition, fontSizeSettingsKeys, fontSizeSettingsValues );
		}
		if ( fontSettingPosition == EDITOR_FONT ) {
			FONT_CHANGED.invoke( new FontChangedEvent( this, fontSettingPosition, font ) );
		}
	}

	/**
	 * Store the current order of Text Segment window table columns, so the ordering
	 * can be preserved and restored.
	 *
	 * @param columnOrder An array of int indicating column order.
	 */
	public void setTextColumnOrder( final int[] columnOrder ) {
		StringBuilder stringifiedOrder = new StringBuilder();
		for ( int j : columnOrder ) {
			stringifiedOrder.append( j ).append( " " );
		}
		setStringSetting( TEXT_COLUMN_ORDER, stringifiedOrder.toString() );
	}

	/**
	 * Set Color object for specified settings key. Has no effect if key is invalid.
	 *
	 * @param key   the Setting key
	 * @param color the Color to save
	 */
	public void setColorSettingByKey( final String key, final Color color ) {
		for ( int i = 0; i < colorSettingsKeys.length; i++ ) {
			if ( key.equals( colorSettingsKeys[i] ) ) {
				setColorSettingByPosition( i, color );
				return;
			}
		}
	}

	/**
	 * Set Color object for specified settings name (a static constant). Has no
	 * effect if invalid.
	 *
	 * @param position the Setting name (see list of static constants)
	 * @param color    the Color to save
	 */
	public void setColorSettingByPosition( final int position, final Color color ) {
		if ( position >= 0 && position < colorSettingsKeys.length ) {
			setColorSetting( position, color );
		}
	}

	/////////////////////////////////////////////////////////////////////////
	//
	//     PRIVATE HELPER METHODS TO DO THE REAL WORK
	//
	/////////////////////////////////////////////////////////////////////////

	// Initialize settings to default values.
	// Strategy: First set from properties file.
	//           If that fails, set from array.
	//           In either case, use these values as defaults in call to Preferences.

	private void initialize() {
		applyDefaultSettings();
		if ( !readSettingsFromPropertiesFile() ) {
			LOGGER.warn( "Unable to read Settings.properties defaults. Using built-in defaults." );
		}
		getSettingsFromPreferences();
	}

	// Default values.  Will be replaced if available from property file or Preferences object.
	private void applyDefaultSettings() {
		booleanSettingsValues.clear();
		booleanSettingsValues.putAll( DEFAULT_BOOLEAN_SETTINGS );
		stringSettingsValues.clear();
		stringSettingsValues.putAll( DEFAULT_STRING_SETTINGS );
		for ( int i = 0; i < fontFamilySettingsValues.length; i++ ) {
			fontFamilySettingsValues[i] = defaultFontFamilySettingsValues[i];
			fontStyleSettingsValues[i] = defaultFontStyleSettingsValues[i];
			fontSizeSettingsValues[i] = defaultFontSizeSettingsValues[i];
		}
		System.arraycopy( defaultColorSettingsValues, 0, colorSettingsValues, 0, colorSettingsValues.length );
		initializeEditorSyntaxStyles();
	}

	// Used by setter method(s) for string-based settings (initially, only exception handler name)
	public void setStringSetting( final String key, final String value ) {
		stringSettingsValues.put( key, value );
		saveStringSetting( key );
	}

	// Used by setter methods for color-based settings
	private void setColorSetting( final int settingIndex, final Color color ) {
		colorSettingsValues[settingIndex] = Binary.intToHexString(
			color.getRed() << 16 | color.getGreen() << 8 | color.getBlue()
		);
		saveColorSetting( settingIndex );
	}

	// Get Color object for this key value.  Get it from values array provided as argument (could be either
	// the current or the default settings array).
	private Color getColorValueByKey( final String key, final String[] values ) {
		for ( int i = 0; i < colorSettingsKeys.length; i++ ) {
			if ( key.equals( colorSettingsKeys[i] ) ) {
				return getColorValueByPosition( i, values );
			}
		}
		return null;
	}

	// Get Color object for this key array position.  Get it from values array provided as argument (could be either
	// the current or the default settings array).
	private Color getColorValueByPosition( final int position, final String[] values ) {
		Color color = null;
		if ( position >= 0 && position < colorSettingsKeys.length ) {
			try {
				color = Color.decode( values[position] );
			} catch ( final NumberFormatException ignored ) {
			}
		}
		return color;
	}

	// Use only for syntax style!!
	private Color getSyntaxColorValueByPosition( final int position, final String[] values ) {
		Color color = null;
		if ( position >= 0 && position < Token.ID_COUNT ) {
			try {
				color = Color.decode( values[position] );
			} catch ( final NumberFormatException ignored ) {
			}
		}
		return color;
	}

	// Establish the settings from the given properties file.  Return true if it worked,
	// false if it didn't.  Note the properties file exists only to provide default values
	// in case the Preferences fail or have not been recorded yet.
	//
	// Any settings successfully read will be stored in both the xSettingsValues and
	// defaultXSettingsValues arrays (x=boolean,string,color).  The latter will overwrite the
	// last-resort default values hardcoded into the arrays above.
	//
	// NOTE: If there is NO ENTRY for the specified property, Globals.getPropertyEntry() returns
	// null.  This is no cause for alarm.  It will occur during system development or upon the
	// first use of a new MARS release in which new settings have been defined.
	// In that case, this method will NOT make an assignment to the settings array!
	// So consider it a precondition of this method: the settings arrays must already be
	// initialized with last-resort default values.
	private boolean readSettingsFromPropertiesFile() {
		String settingValue;
		try {
			for ( var key : DEFAULT_BOOLEAN_SETTINGS.keySet() ) {
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, key );
				if ( settingValue != null )
					booleanSettingsValues.put( key, Boolean.parseBoolean( settingValue ) );
			}
			for ( var key : DEFAULT_STRING_SETTINGS.keySet() ) {
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, key );
				if ( settingValue != null )
					stringSettingsValues.put( key, settingValue );
			}
			for ( int i = 0; i < fontFamilySettingsValues.length; i++ ) {
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, fontFamilySettingsKeys[i] );
				if ( settingValue != null ) {
					fontFamilySettingsValues[i] = defaultFontFamilySettingsValues[i] = settingValue;
				}
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, fontStyleSettingsKeys[i] );
				if ( settingValue != null ) {
					fontStyleSettingsValues[i] = defaultFontStyleSettingsValues[i] = settingValue;
				}
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, fontSizeSettingsKeys[i] );
				if ( settingValue != null ) {
					fontSizeSettingsValues[i] = defaultFontSizeSettingsValues[i] = settingValue;
				}
			}
			for ( int i = 0; i < colorSettingsKeys.length; i++ ) {
				settingValue = Utility.getPropertyEntry( DEFAULT_CONFIG_FILE, colorSettingsKeys[i] );
				if ( settingValue != null ) {
					colorSettingsValues[i] = defaultColorSettingsValues[i] = settingValue;
				}
			}
		} catch ( final Exception e ) {
			return false;
		}
		return true;
	}

	// Get settings values from Preferences object.  A key-value pair will only be written
	// to Preferences if/when the value is modified.  If it has not been modified, the default value
	// will be returned here.
	//
	// PRECONDITION: Values arrays have already been initialized to default values from
	// Settings.properties file or default value arrays above!
	private void getSettingsFromPreferences() {
		preferences.put( "CONFIG_TYPE", "MARS-" + Const.VERSION );
		preferences.saveNoException();
		for ( var key : DEFAULT_BOOLEAN_SETTINGS.keySet() ) {
			booleanSettingsValues.put( key, preferences.getBoolean( key, booleanSettingsValues.get( key ) ) );
		}
		for ( var key : DEFAULT_STRING_SETTINGS.keySet() ) {
			stringSettingsValues.put( key, preferences.get( key, stringSettingsValues.get( key ) ) );
		}
		for ( int i = 0; i < fontFamilySettingsKeys.length; i++ ) {
			fontFamilySettingsValues[i] = preferences.get( fontFamilySettingsKeys[i], fontFamilySettingsValues[i] );
			fontStyleSettingsValues[i] = preferences.get( fontStyleSettingsKeys[i], fontStyleSettingsValues[i] );
			fontSizeSettingsValues[i] = preferences.get( fontSizeSettingsKeys[i], fontSizeSettingsValues[i] );
		}
		for ( int i = 0; i < colorSettingsKeys.length; i++ ) {
			colorSettingsValues[i] = preferences.get( colorSettingsKeys[i], colorSettingsValues[i] );
		}
		getEditorSyntaxStyleSettingsFromPreferences();
	}

	// Save the key-value pair in the Properties object and assure it is written to persisent storage.
	private void saveBooleanSetting( final String key ) {
		try {
			preferences.putBoolean( key, booleanSettingsValues.get( key ) );
			preferences.save();
		} catch ( IOException e ) {
			LOGGER.error( "Failed to save Boolean setting `{}`", key, e );
		}
	}

	// Save the key-value pair in the Properties object and assure it is written to persisent storage.
	private void saveStringSetting( final String key ) {
		try {
			preferences.put( key, stringSettingsValues.get( key ) );
			preferences.save();
		} catch ( IOException e ) {
			LOGGER.error( "Failed to save String setting `{}`", key, e );
		}
	}

	// Save the key-value pair in the Properties object and assure it is written to persisent storage.
	private void saveFontSetting( final int index, final String[] settingsKeys, final String[] settingsValues ) {
		try {
			preferences.put( settingsKeys[index], settingsValues[index] );
			preferences.save();
		} catch ( IOException e ) {
			LOGGER.error( "Failed to save Font setting `{}`", index, e );
		}
	}

	// Save the key-value pair in the Properties object and assure it is written to persisent storage.
	private void saveColorSetting( final int index ) {
		try {
			preferences.put( colorSettingsKeys[index], colorSettingsValues[index] );
			preferences.save();
		} catch ( IOException e ) {
			LOGGER.error( "Failed to save Color setting `{}`", index, e );
		}
	}

	/*
	 *  Private helper to do the work of converting a string containing Text
	 *  Segment window table column order into int array and returning it.
	 *  If a problem occurs with the parameter string, will fall back to the
	 *  default defined above.
	 */
	private int[] getTextSegmentColumnOrder( final String stringOfColumnIndexes ) {
		final StringTokenizer st = new StringTokenizer( stringOfColumnIndexes );
		final int[] list = new int[st.countTokens()];
		int index = 0, value;
		boolean valuesOK = true;
		while ( st.hasMoreTokens() ) {
			try {
				value = Integer.parseInt( st.nextToken() );
			} // could be either NumberFormatException or NoSuchElementException
			catch ( final Exception e ) {
				valuesOK = false;
				break;
			}
			list[index++] = value;
		}
		if ( !valuesOK && !stringOfColumnIndexes.equals( DEFAULT_STRING_SETTINGS.get( TEXT_COLUMN_ORDER ) ) ) {
			return getTextSegmentColumnOrder( DEFAULT_STRING_SETTINGS.get( TEXT_COLUMN_ORDER ) );
		}
		return list;
	}

	public SalvableProperties getPreferences() {
		return this.preferences;
	}

	public String getStringSetting( String key ) {
		if ( DEFAULT_STRING_SETTINGS.containsKey( key ) ) {
			return stringSettingsValues.computeIfAbsent( key, _key -> DEFAULT_STRING_SETTINGS.get( key ) );
		} else {
			throw new IllegalArgumentException( "Invalid string setting ID: " + key );
		}
	}

	public record BooleanChangedEvent( String key, boolean old, boolean current ) implements Event { }
	public record FontChangedEvent( Settings settings, int index, Font current ) implements Event { }
}
