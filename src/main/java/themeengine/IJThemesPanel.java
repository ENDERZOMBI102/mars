/*
 * Copyright 2019 FormDev Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package themeengine;

import com.formdev.flatlaf.*;
import com.formdev.flatlaf.extras.FlatAnimatedLafChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.List;
import java.util.function.Predicate;

import static com.enderzombi102.mars.res.Localization.loc;


/**
 * @author Karl Tauber
 */
public class IJThemesPanel extends JPanel {
	private static final Logger LOGGER = LoggerFactory.getLogger( IJThemesPanel.class );
	public static final String THEMES_PACKAGE = "/themeengine/intellijThemes/";

	private final IJThemesManager themesManager = new IJThemesManager();
	private final List<IJThemeInfo> themes = new ArrayList<>();
	private final HashMap<Integer, String> categories = new HashMap<>();
	private final PropertyChangeListener lafListener = this::lafChanged;
	private final WindowListener windowListener = new WindowAdapter() {
		@Override
		public void windowActivated( final WindowEvent e ) {
			IJThemesPanel.this.windowActivated();
		}
	};
	private Window window;

	private boolean isAdjustingThemesList;
	private boolean areThemesEnabled;

	private final JCheckBox enableThemesCheckBox;
	private final JComboBox<String> filterComboBox;
	private final JList<IJThemeInfo> themesList;

	public IJThemesPanel( boolean areThemesEnabled ) {
		super( new BorderLayout() );

		var upper = new JPanel( new BorderLayout() );

		// Theme Engine toggle
		this.enableThemesCheckBox = new JCheckBox( loc( "themeengine.enabled.name" ) );
		this.enableThemesCheckBox.setFocusable( false );
		upper.add( this.enableThemesCheckBox, BorderLayout.WEST );

		// Dark/Light theme filter
		this.filterComboBox = new JComboBox<>();
		this.filterComboBox.setModel( new DefaultComboBoxModel<>( new String[] { "all", "light", "dark" } ) );
		this.filterComboBox.putClientProperty( "JComponent.minimumWidth", 0 );
		this.filterComboBox.setFocusable( false );
		this.filterComboBox.addActionListener( e -> updateThemesList() );
		upper.add( this.filterComboBox, BorderLayout.EAST );
		upper.setBorder( BorderFactory.createEmptyBorder( 0, 0, 5, 0 ) );

		this.add( upper, BorderLayout.NORTH );
		this.setBorder( BorderFactory.createEmptyBorder( 2, 10, 5, 10 ) );

		// List of themes
		this.themesList = new JList<>();
		this.themesList.setSelectionMode( ListSelectionModel.SINGLE_SELECTION );
		this.themesList.addListSelectionListener( this::themesListValueChanged );
		this.themesList.setCellRenderer( new ThemeListCellRenderer() );

		// Scroll pane for the list
		this.add( new JScrollPane( this.themesList ), BorderLayout.CENTER );

		// load themes
		this.setThemesEnabled( areThemesEnabled );
		this.enableThemesCheckBox.addItemListener( this::checkBoxChanged ); // special treatment for this guy.

		this.updateThemesList();
	}

	private void updateThemesList() {
		final int filterLightDark = this.filterComboBox.getSelectedIndex();
		final boolean showLight = filterLightDark != 2;
		final boolean showDark = filterLightDark != 1;

		// load theme infos
		this.themesManager.loadBundledThemes();
		this.themesManager.loadThemesFromDirectory();

		// sort themes by name
		final Comparator<? super IJThemeInfo> comparator = ( t1, t2 ) -> t1.name().compareToIgnoreCase( t2.name() );
		this.themesManager.bundledThemes.sort( comparator );
		this.themesManager.moreThemes.sort( comparator );

		// remember selection (must be invoked before clearing themes field)
		IJThemeInfo oldSel = this.themesList.getSelectedValue();

		this.themes.clear();
		this.categories.clear();

		// add core themes at beginning
		this.categories.put( this.themes.size(), loc( "themeengine.category.core.name" ) );
		if ( showLight )
			this.themes.add(
				new IJThemeInfo(
					"Flat Light",
					null,
					false,
					null,
					null,
					null,
					null,
					null,
					FlatLightLaf.class.getName()
				)
			);
		if ( showDark )
			this.themes.add(
				new IJThemeInfo(
					"Flat Dark",
					null,
					true,
					null,
					null,
					null,
					null,
					null,
					FlatDarkLaf.class.getName()
				)
			);
		if ( showLight )
			this.themes.add(
				new IJThemeInfo(
					"Flat IntelliJ",
					null,
					false,
					null,
					null,
					null,
					null,
					null,
					FlatIntelliJLaf.class.getName()
				)
			);
		if ( showDark )
			this.themes.add(
				new IJThemeInfo(
					"Flat Darcula",
					null,
					true,
					null,
					null,
					null,
					null,
					null,
					FlatDarculaLaf.class.getName()
				)
			);

		// add themes from directory
		this.categories.put( this.themes.size(), loc( "themeengine.category.current_directory.name" ) );
		this.themes.addAll( this.themesManager.moreThemes );

		// add uncategorized bundled themes
		this.categories.put( themes.size(), loc( "themeengine.category.intellij.name" ) );
		for ( final IJThemeInfo ti : this.themesManager.bundledThemes ) {
			final boolean show = showLight && !ti.dark() || showDark && ti.dark();
			if ( show && !ti.name().contains( "/" ) )
				this.themes.add( ti );
		}

		// add categorized bundled themes
		String lastCategory = null;
		for ( final IJThemeInfo ti : this.themesManager.bundledThemes ) {
			final boolean show = showLight && !ti.dark() || showDark && ti.dark();
			final int sep = ti.name().indexOf( '/' );
			if ( !show || sep < 0 ) {
				continue;
			}

			final String category = ti.name().substring( 0, sep ).trim();
			if ( !Objects.equals( lastCategory, category ) ) {
				lastCategory = category;
				this.categories.put( this.themes.size(), category );
			}

			this.themes.add( ti );
		}

		// fill the theme list
		this.themesList.setModel( new AbstractListModel<>() {
			@Override
			public int getSize() {
				return themes.size();
			}

			@Override
			public IJThemeInfo getElementAt( final int index ) {
				return themes.get( index );
			}
		});

		// restore selection
		if ( oldSel != null ) {
			for ( int i = 0; i < this.themes.size(); i++ ) {
				final IJThemeInfo theme = this.themes.get( i );
				if ( oldSel.name().equals( theme.name() ) && Objects.equals( oldSel.resourceName(), theme.resourceName() ) && Objects
					.equals( oldSel.themeFile(), theme.themeFile() ) && Objects.equals( oldSel.lafClassName(),
					theme.lafClassName() ) ) {
					this.themesList.setSelectedIndex( i );
					break;
				}
			}
		} else {
			if ( UIManager.getLookAndFeel() instanceof FlatLaf ) {
				this.themesList.setSelectedIndex( DemoPrefs.getSelectedLafIndex() );
			}
		}

		// select first theme if none selected
		if ( this.themesList.getSelectedIndex() < 0 ) {
			this.themesList.setSelectedIndex( 0 );
		}

		// scroll selection into visible area
		final int sel = this.themesList.getSelectedIndex();
		if ( sel >= 0 ) {
			final Rectangle bounds = this.themesList.getCellBounds( sel, sel );
			if ( bounds != null ) {
				this.themesList.scrollRectToVisible( bounds );
			}
		}
	}

	private void themesListValueChanged( final ListSelectionEvent e ) {
		final IJThemeInfo themeInfo = this.themesList.getSelectedValue();
		DemoPrefs.setSelectedLafIndex( this.themesList.getSelectedIndex() );
		if ( e.getValueIsAdjusting() || this.isAdjustingThemesList || !this.areThemesEnabled ) {
			return;
		}

		EventQueue.invokeLater( () -> setTheme( themeInfo ) );
	}

	private void setTheme( final IJThemeInfo themeInfo ) {
		if ( themeInfo == null ) {
			return;
		}

		// change look and feel
		if ( themeInfo.lafClassName() != null ) {
			if ( themeInfo.lafClassName().equals( UIManager.getLookAndFeel().getClass().getName() ) ) {
				return;
			}

			FlatAnimatedLafChange.showSnapshot();

			try {
				UIManager.setLookAndFeel( themeInfo.lafClassName() );
			} catch ( final Exception ex ) {
				LOGGER.error( "Failed to create theme `{}`", themeInfo.themeFile(), ex );
				showInformationDialog( "Failed to create '" + themeInfo.lafClassName() + "'.", ex );
			}
		} else if ( themeInfo.themeFile() != null ) {
			FlatAnimatedLafChange.showSnapshot();
			//cannot test, if errs, can't do anything :(
			try {
				if ( themeInfo.themeFile().getName().endsWith( ".properties" ) )
					FlatLaf.setup( new FlatPropertiesLaf( themeInfo.name(), themeInfo.themeFile() ) );
				else FlatLaf.setup( IntelliJTheme.createLaf( new FileInputStream( themeInfo.themeFile() ) ) );

				DemoPrefs.getState().put( DemoPrefs.KEY_LAF_THEME, DemoPrefs.FILE_PREFIX + themeInfo.themeFile() );
			} catch ( final Exception ex ) {
				LOGGER.error( "Failed to create theme `{}`", themeInfo.themeFile(), ex );
				showInformationDialog( "Failed to load '" + themeInfo.themeFile() + "'.", ex );
			}
		} else {
			FlatAnimatedLafChange.showSnapshot();

			var themeStream = getClass().getResourceAsStream( THEMES_PACKAGE + themeInfo.resourceName() );
			if ( themeStream == null ) {
				// failed to load theme from disk
				throw new RuntimeException( "Failed to load theme \"" + THEMES_PACKAGE + themeInfo.resourceName() + "\"!" );
			}

			IntelliJTheme.setup( themeStream );
			DemoPrefs.getState().put( DemoPrefs.KEY_LAF_THEME, DemoPrefs.RESOURCE_PREFIX + themeInfo.resourceName() );
		}

		// update all components
		FlatLaf.updateUI();
		FlatAnimatedLafChange.hideSnapshotWithAnimation();
	}

	private void showInformationDialog( final String message, final Exception ex ) {
		JOptionPane.showMessageDialog(
			SwingUtilities.windowForComponent( this ),
			message + "\n\n" + ex.getMessage(),
			"FlatLaf",
			JOptionPane.INFORMATION_MESSAGE
		);
	}

	@Override
	public void addNotify() {
		super.addNotify();

		selectedCurrentLookAndFeel();
		UIManager.addPropertyChangeListener( this.lafListener );

		this.window = SwingUtilities.windowForComponent( this );
		if ( this.window != null ) {
			this.window.addWindowListener( this.windowListener );
		}
	}

	@Override
	public void removeNotify() {
		super.removeNotify();

		UIManager.removePropertyChangeListener( this.lafListener );

		if ( this.window != null ) {
			this.window.removeWindowListener( this.windowListener );
			this.window = null;
		}
	}

	private void lafChanged( final PropertyChangeEvent e ) {
		if ( "lookAndFeel".equals( e.getPropertyName() ) ) {
			selectedCurrentLookAndFeel();
		}
	}

	private void windowActivated() {
		// refresh themes list on window activation
		if ( themesManager.hasThemesFromDirectoryChanged() ) {
			updateThemesList();
		}
	}

	private void selectedCurrentLookAndFeel() {
		final LookAndFeel lookAndFeel = UIManager.getLookAndFeel();
		final String theme = UIManager.getLookAndFeelDefaults().getString( DemoPrefs.THEME_UI_KEY );

		if ( theme == null && ( lookAndFeel instanceof IntelliJTheme.ThemeLaf || lookAndFeel instanceof FlatPropertiesLaf ) ) {
			return;
		}

		Predicate<IJThemeInfo> test;
		if ( theme != null && theme.startsWith( DemoPrefs.RESOURCE_PREFIX ) ) {
			final String resourceName = theme.substring( DemoPrefs.RESOURCE_PREFIX.length() );
			test = ti -> Objects.equals( ti.resourceName(), resourceName );
		} else if ( theme != null && theme.startsWith( DemoPrefs.FILE_PREFIX ) ) {
			final File themeFile = new File( theme.substring( DemoPrefs.FILE_PREFIX.length() ) );
			test = ti -> Objects.equals( ti.themeFile(), themeFile );
		} else {
			final String lafClassName = lookAndFeel.getClass().getName();
			test = ti -> Objects.equals( ti.lafClassName(), lafClassName );
		}

		int newSel = -1;
		for ( int i = 0; i < this.themes.size(); i++ ) {
			if ( test.test( this.themes.get( i ) ) ) {
				newSel = i;
				break;
			}
		}

		this.isAdjustingThemesList = true;
		if ( newSel >= 0 ) {
			if ( newSel != this.themesList.getSelectedIndex() ) {
				this.themesList.setSelectedIndex( newSel );
			}
		} else {
			this.themesList.clearSelection();
		}
		this.isAdjustingThemesList = false;
	}

	private void checkBoxChanged( ItemEvent e ) {
		boolean newState = e.getStateChange() == ItemEvent.SELECTED;
		setThemesEnabled( newState );
		DemoPrefs.setLafState( newState );
		if ( newState ) {
			Object[] options = { loc( "themeengine.warning.button" ) };
			//noinspection MagicConstant
			JOptionPane.showOptionDialog(
				this,
				loc( "themeengine.warning.message" ),
				loc( "themeengine.warning.title" ),
				JOptionPane.OK_OPTION,
				JOptionPane.INFORMATION_MESSAGE,
				null,
				options,
				options[ 0 ]
			);
		}
	}

	private void setThemesEnabled( final boolean value ) {
		this.areThemesEnabled = value;
		this.enableThemesCheckBox.setSelected( value );
		this.filterComboBox.setEnabled( value );
		this.themesList.setEnabled( value );

		if ( value ) {  // apply theme
			updateThemesList(); // i'm a simple man, i'm lazy
			return;
		}

		// apply java default laf
		try {
			UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName() );
		} catch ( ReflectiveOperationException | UnsupportedLookAndFeelException ex ) {
			LOGGER.error( "Failed to apply look-and-feel!", ex );
		}

		// update all components
		FlatLaf.updateUI();
		FlatAnimatedLafChange.hideSnapshotWithAnimation();
	}

	private class ThemeListCellRenderer extends DefaultListCellRenderer {
		@Override
		public Component getListCellRendererComponent( final JList<?> list, final Object value, final int index, final boolean isSelected, final boolean cellHasFocus ) {
			final String title = categories.get( index );
			final IJThemeInfo info = (IJThemeInfo) value;
			String name = info.name();
			final int sep = name.indexOf( '/' );
			if ( sep >= 0 ) {
				name = name.substring( sep + 1 ).trim();
			}

			final JComponent c = (JComponent) super.getListCellRendererComponent( list, name, index, isSelected, cellHasFocus );
			c.setToolTipText( buildToolTip( info ) );
			if ( title != null ) {
				c.setBorder( new CompoundBorder( new ListCellTitledBorder( themesList, title ), c.getBorder() ) );
			}
			return c;
		}

		private String buildToolTip( final IJThemeInfo ti ) {
			if ( ti.themeFile() != null ) {
				return ti.themeFile().getPath();
			}
			if ( ti.resourceName() == null ) {
				return ti.name();
			}

			return loc( "themeengine.list.entry.tooltip", ti.name(), ti.license(), ti.sourceCodeUrl() );
		}
	}
}
