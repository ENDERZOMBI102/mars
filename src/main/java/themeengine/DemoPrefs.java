/*
 * Copyright 2019 FormDev Software GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package themeengine;

import com.enderzombi102.mars.res.SalvableProperties;
import com.formdev.flatlaf.FlatLaf;
import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.FlatPropertiesLaf;
import com.formdev.flatlaf.IntelliJTheme;
import com.formdev.flatlaf.util.StringUtils;
import mars.Globals;

import javax.swing.*;
import java.io.File;
import java.io.FileInputStream;

/**
 * @author Karl Tauber
 */
public class DemoPrefs {
	public static final String KEY_LAF = "laf";
	public static final String KEY_LAF_THEME = "lafTheme";
	public static final String RESOURCE_PREFIX = "res:";
	public static final String FILE_PREFIX = "file:";
	public static final String THEME_UI_KEY = "__FlatLaf.demo.theme";
	public static final String LAF_STATE_KEY = "areThemesActive";
	private static final String LAF_INDEX_KEY = "selectedLafIndex";

	public static SalvableProperties getState() {
		return Globals.getSettings().getPreferences();
	}

	public static void initLaf(final String[] args) {
		// remember active look and feel
		UIManager.addPropertyChangeListener( e -> {
			if ( "lookAndFeel".equals( e.getPropertyName() ) )
				getState().put( KEY_LAF, UIManager.getLookAndFeel().getClass().getName() );
		});

		if (! getLafState() )
			return;
		// set look and feel

		JFrame.setDefaultLookAndFeelDecorated(true);
		JDialog.setDefaultLookAndFeelDecorated(true);
		try {
			if ( args.length > 0 ) {
				UIManager.setLookAndFeel(args[0]);
			} else {
				final String lafClassName = getState().get( KEY_LAF, IntelliJTheme.ThemeLaf.class.getName() );
				if ( IntelliJTheme.ThemeLaf.class.getName().equals(lafClassName) ) {
					final String theme = getState().get(KEY_LAF_THEME, "res:DarkPurple.theme.json");
					if ( theme.startsWith(RESOURCE_PREFIX) ) {
						IntelliJTheme.setup(
							IJThemesPanel.class.getResourceAsStream(
								IJThemesPanel.THEMES_PACKAGE + theme.substring( RESOURCE_PREFIX.length() )
							)
						);
					} else if ( theme.startsWith(FILE_PREFIX) ) {
						FlatLaf.setup(
							IntelliJTheme.createLaf(
								new FileInputStream( theme.substring( FILE_PREFIX.length() ) )
							)
						);
					} else {
						FlatLightLaf.setup();
					}

					if (! theme.isEmpty() )
						UIManager.getLookAndFeelDefaults().put(THEME_UI_KEY, theme);
				} else if ( FlatPropertiesLaf.class.getName().equals(lafClassName) ) {
					final String theme = getState().get( KEY_LAF_THEME, "" );
					if ( theme.startsWith(FILE_PREFIX) ) {
						final File themeFile = new File( theme.substring( FILE_PREFIX.length() ) );
						final String themeName = StringUtils.removeTrailing( themeFile.getName(), ".properties" );
						FlatLaf.setup( new FlatPropertiesLaf( themeName, themeFile ) );
					} else {
						FlatLightLaf.setup();
					}

					if (! theme.isEmpty() )
						UIManager.getLookAndFeelDefaults().put(THEME_UI_KEY, theme);
				} else {
					UIManager.setLookAndFeel(lafClassName);
				}
			}
		} catch (final Throwable ex) {
			ex.printStackTrace();

			// fallback
			FlatLightLaf.setup();
		}
	}

	public static void setLafState(boolean lafState) {
		getState().putBoolean(LAF_STATE_KEY, lafState);
		getState().saveNoException();
	}

	public static boolean getLafState() {
		return getState().getBoolean(LAF_STATE_KEY, true);
	}

	public static void setSelectedLafIndex(int selectedIndex) {
		getState().putInt(LAF_INDEX_KEY, selectedIndex);
		getState().saveNoException();
	}

	public static int getSelectedLafIndex() {
		return getState().getInt(LAF_INDEX_KEY, 12);
	}

}
