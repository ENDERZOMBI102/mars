package com.enderzombi102.mars.ui

import com.enderzombi102.enderlib.SafeUtils.doSafely
import com.enderzombi102.mars.res.Localization.loc
import com.enderzombi102.mars.res.Resources.readImage
import mars.Globals
import java.awt.BorderLayout
import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import java.awt.event.KeyAdapter
import java.awt.event.KeyEvent
import java.awt.font.TextAttribute
import java.awt.image.BufferedImage
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.SwingConstants.CENTER


class SplashScreen( escCloses: Boolean = false ) : JFrame() {
	init {
		if ( escCloses ) {
			addKeyListener(object : KeyAdapter() {
				override fun keyPressed(e: KeyEvent) {
					if (e.keyCode == KeyEvent.VK_ESCAPE) {
						this@SplashScreen.dispose()
					}
				}
			})
		}

		// Set the window's bounds, centering the window
		setSize( IMAGE.width, IMAGE.height )
		setLocation( ( toolkit.screenSize.width - width ) / 2, ( toolkit.screenSize.height - height ) / 2 )

		fun builder( value: String, font: Font, foreground: Color ): JLabel {
			val label = JLabel( value, CENTER )
			label.setForeground( foreground )
			label.setFont( font )
			return label
		}

		fun row( index: Int, font: Font ): JLabel =
			builder( "<html><br><br>${loc("mars.splash.row$index")}</html>", font, Color.WHITE )


		// Build the splash screen
		// texts
		contentPane = object : JPanel() {
			override fun paintComponent( g: Graphics ) {
				super.paintComponent( g )
				g.drawImage( IMAGE, 0, 0, width, height, null )
			}
		}

		val font = Font( "Sans-Serif", Font.BOLD, 14 )
		add( builder( loc( "mars.splash.title" ), font.deriveFont( 16f ), Color.BLACK ), BorderLayout.NORTH )
		add( row( 0, font ), BorderLayout.CENTER )
		add( row( 1, font ), BorderLayout.SOUTH )
		add( row( 2, font ), BorderLayout.SOUTH )

		isAlwaysOnTop = true
		isUndecorated = true
		isVisible = true
	}

	companion object {
		private val IMAGE = doSafely<BufferedImage> { readImage( Globals.imagesPath + "MarsSurfacePathfinder.jpg" ) }
	}
}
